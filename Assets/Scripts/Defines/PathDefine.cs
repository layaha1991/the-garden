﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathDefine {

    public static string Path_Pond = "Pond/";

    public static string GetPondPath (EnumPondType type) {
        string typeStr = type.ToString ();
        return Path_Pond + typeStr+ "/" + typeStr; 
    }

}
