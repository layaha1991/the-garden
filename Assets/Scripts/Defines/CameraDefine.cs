﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CameraDefine {

    // main
    public static Vector3 POS_MAIN = new Vector3 (-11f, 329f , 1693f);
    public static Quaternion ROT_MAIN = Quaternion.Euler (6f,-195f,0f);

    //Garden
    public static Vector3 POS_GARDEN = new Vector3(-699f, 766f, 1362f);
    public static Quaternion ROT_GARDEN = Quaternion.Euler(27f, -219f, 0f);


    // water wheel
    public static Vector3 POS_WATERWHEEL = new Vector3 (-433f, 273f, 517f);
    public static Quaternion ROT_WATERWHEEL = Quaternion.Euler (17.33f, -228f, 0f);

    // pond
    public static Vector3 POS_POND = new Vector3 (353f, 771f, 700f);
    public static Quaternion ROT_POND = Quaternion.Euler (84f, 182f, 0f);

    // insdie temple
    public static Vector3 POS_INSIDETEMPLE = new Vector3(4000f, 0f, 0f);
    public static Quaternion ROT_INSIDETEMPLE = Quaternion.identity;


    //temple building
    public static Vector3 POS_TEMPLE = new Vector3(310, 300f, -290f);
    public static Quaternion ROT_TEMPLE = Quaternion.Euler(12f, 180f, 0f);

    // bamboo
    public static Vector3 POS_BAMBOO = new Vector3 (560f, 404f, 774f);
    public static Quaternion ROT_BAMBOO = Quaternion.Euler (34f, 143f, 0f);

    // Flower
    public static Vector3[] POS_FLOWER = new Vector3 [3] {  new Vector3 (405f, 157f, 1244f),
                                                            new Vector3 (-255f, 379f, 132f),
                                                            new Vector3 (305f, 541f, 156f)};
    public static Quaternion[] ROT_FLOWER = new Quaternion [3] {Quaternion.Euler (39f, 179f, 0f),
                                                                Quaternion.Euler (58f, 106f, 0f),
                                                                Quaternion.Euler (43f, 180f, 0f)};

    // Mushroom
    public static Vector3[] POS_MUSHROOM = new Vector3 [3] {  new Vector3 (560f, 114f, 1036f),
                                                            new Vector3 (356f, 251f, 328f),
                                                            new Vector3 (865f, 94f, 1127f)};
    public static Quaternion[] ROT_MUSHROOM = new Quaternion [3] {Quaternion.Euler (49f, 33f, 0f),
                                                                Quaternion.Euler (47f, 143f, 0f),
                                                                Quaternion.Euler (24.2f, 227f, 0f)};

    // Tree
    public static Vector3[] POS_TREE = new Vector3 [3] {  new Vector3 (-476f, 501f, -589f),
                                                            new Vector3 (515f, 370f, 1339f),
                                                            new Vector3 (405f, 477f, 320f)};
    public static Quaternion[] ROT_TREE = new Quaternion [3] {Quaternion.Euler (45f, 295f, 0f),
                                                                Quaternion.Euler (49f, -176f, 0f),
                                                                Quaternion.Euler (30f, 140f, 0f)};

    // Reed
    public static Vector3[] POS_REED = new Vector3 [3] {  new Vector3 (257.7f, 232.8f, 700f),
                                                            new Vector3 (-571f, 395f, 205f),
                                                            new Vector3 (-57f, 317.5f, 1076f)};
    public static Quaternion[] ROT_REED = new Quaternion [3] {Quaternion.Euler (37f, 170f, 0f),
                                                                Quaternion.Euler (47f, 125f, 0f),
                                                                Quaternion.Euler (48f, 157f, 0f)};

    // Sakura
    public static Vector3[] POS_SAKURA = new Vector3 [3] {  new Vector3 (-427f, 754f, 218f),
                                                            new Vector3 (535, 466f, 386f),
                                                            new Vector3 (485f, 540f, 757f)};
    public static Quaternion[] ROT_SAKURA = new Quaternion [3] {Quaternion.Euler (49f, 165f, 0f),
                                                                Quaternion.Euler (34f, 12f, 0f),
                                                                Quaternion.Euler (20f, 150f, 0f)};

}
