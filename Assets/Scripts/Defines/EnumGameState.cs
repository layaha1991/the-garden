﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumGameState {
    Main,
    Garden,         // 花園
    Pond,           // 水塘
    WaterWheel,     // 水車
    Temple,         // 廟
    InsideTemple,      // 廟裏
    StartZenPractice, // 廟Game中
    Watch           // 用於觀看
}
