﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public delegate void AdFinishedEventHandler (AdResult result);

public enum AdResult {
    Success,
    Failure,
    Unknown
}

public class AdsSystem : Singleton<AdsSystem> 
{

    private static string _unityAdID = "";

    private static event AdFinishedEventHandler _callback;

    public static bool HasAd 
    {

        get 
        {
            
            if (HasUnityAd ())
                return true;

            return false;
        }

    }

    #region Init

        public static void Setup () 
    {
            // get unity ad id...
            #if UNITY_IOS
                _unityAdID = "ios id";
            #elif UNITY_ANDROID
                _unityAdID = "android id";
            #endif

            // Initial unity's ad
            Advertisement.Initialize (_unityAdID);
        }

    #endregion

    #region Show ad

        public static void ShowAd (AdFinishedEventHandler callback) {

            if (HasAd) 
        {
                _callback = callback;
                
                // logic...
            } else 
        {
                return;
            }

            if (HasUnityAd ()) 
        {
                ShowOptions option = new ShowOptions ();
                option.resultCallback += OnUnityAdFinishedEvent;
                Advertisement.Show ("rewardedVideo", option);
            }

        }

    #endregion

    #region UnityAd

        private static bool HasUnityAd () 
    {
            return Advertisement.IsReady ("rewardedVideo");
        }

        private static void OnUnityAdFinishedEvent (ShowResult result) 
    {
            AdResult adResult = AdResult.Unknown;

            switch (result) {
                case ShowResult.Finished:
                    adResult = AdResult.Success;
                    break;
                default:
                    adResult = AdResult.Failure;
                    break;
            }

            OnAdFinishedEvent (adResult);
        }

    #endregion

    #region Finished handler

        private static void OnAdFinishedEvent (AdResult result) {
            
            if (_callback != null) 
        {
                _callback (result);
                _callback = null;
            }

        }

    #endregion

}
