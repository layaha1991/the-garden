﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan);

public class OfflineSystem : Singleton<OfflineSystem> {

    [SerializeField] private float _waterWheelOfflineMinutesLimit = 240f;
    [SerializeField] private float _plantOfflineMinutesLimit = 240f;
    [SerializeField] private float _pondOfflineMinutesLimit = 240f;

    public float WaterWheelOfflineSecondsLimit {
        get {
            return _waterWheelOfflineMinutesLimit * 60f;
        }
    }

    public float PlantOfflineSecondsLimit {
        get {
            return _plantOfflineMinutesLimit * 60f;
        }
    }

    public float PondOfflineSecondsLimit {
        get {
            return _pondOfflineMinutesLimit * 60f;
        }
    }

    public static event OnOfflineSystemTimeSpanChanged TimeSpanChanged;

    private void OnApplicationFocus(bool focusStatus) {
        // Debug.Log("Application Focus: " + focusStatus);

        if (DataManager.IsNewPlayer) {
            // Debug.Log("IsNewPlayer.");
            DataManager.IsNewPlayer = false;
            return;
        }

        if (focusStatus) {
            // 計算離開時間
            TimeSpan timeSpan = DateTime.Now - DataManager.ExitTime;
            
            if (TimeSpanChanged != null) {
                TimeSpanChanged (timeSpan);
            }

        } else {
            // 保存離開時間
            DataManager.ExitTime = DateTime.Now;
        }

    }

}
