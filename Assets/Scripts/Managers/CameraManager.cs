﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraManager : Singleton<CameraManager> {

    [SerializeField] private float _moveSpeed = 4f;
    [SerializeField] private float _rotateSpeed = 4f;

    private Vector3 _watchPos;
    private Quaternion _watchRot;

    public event Action ArrivedEvents;

    public bool Smooth { get; set; }

    private void Awake () {
        Smooth = true;
    }

    private void Update () {
        Vector3 destination = Vector3.zero;
        Quaternion rotation = Quaternion.identity;

        switch (GameManager.Instance.State) {

            case EnumGameState.Main:
                destination = CameraDefine.POS_MAIN;
                rotation = CameraDefine.ROT_MAIN;
                break;
            case EnumGameState.Garden:
                destination = CameraDefine.POS_GARDEN;
                rotation = CameraDefine.ROT_GARDEN;
                break;
            case EnumGameState.WaterWheel:
                destination = CameraDefine.POS_WATERWHEEL;
                rotation = CameraDefine.ROT_WATERWHEEL;
                break;
            case EnumGameState.Pond:
                destination = CameraDefine.POS_POND;
                rotation = CameraDefine.ROT_POND;
                break; 
            case EnumGameState.Temple:
                destination = CameraDefine.POS_TEMPLE;
                rotation = CameraDefine.ROT_TEMPLE;
                break;
            case EnumGameState.InsideTemple:
                destination = CameraDefine.POS_INSIDETEMPLE;
                rotation = CameraDefine.ROT_INSIDETEMPLE;
                break;
            case EnumGameState.StartZenPractice:
                destination = CameraDefine.POS_INSIDETEMPLE;
                rotation = CameraDefine.ROT_INSIDETEMPLE;
                break;
            case EnumGameState.Watch:
                destination = _watchPos;
                rotation = _watchRot;
                break;

        }

        if (Smooth) {
            CacheTF.position = Vector3.Lerp (CacheTF.position, destination, Time.deltaTime * _moveSpeed);
            CacheTF.rotation = Quaternion.Slerp (CacheTF.rotation, rotation, Time.deltaTime * _rotateSpeed);
        } else {
            CacheTF.position = destination;
            CacheTF.rotation = rotation;
        }

        // 到達目的地
        if (Vector3.Distance (CacheTF.position, destination) <= 1f) {
            
            if (ArrivedEvents != null) {
                ArrivedEvents();
                ArrivedEvents = null;
            }

        }

    }

    public void SetWatch (Vector3 pos, Quaternion rot) {
        _watchPos = pos;
        _watchRot = rot;
    }

    public void SetWatch (Vector3 pos) {
        _watchPos = pos;
        _watchRot = transform.rotation;
    }

    public void SetWatch (Quaternion rot) {
        _watchPos = transform.position;
        _watchRot = rot;
    }

}
