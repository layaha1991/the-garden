﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataManager {

    #region Save_Key

        // Is New Player
        public const string KEY_NEW_PLAYER = "k_NewPlayer";

        // Level
        public const string KEY_STAGE = "k_Stage";

        //Focus Level
        public const string KEY_FOCUS = "k_Focus";
        //Savvy Level
        public const string KEY_SAVVY = "k_Savvy";
        // Energy
        public const string KEY_WATER_ENERGY = "k_WaterEnergy";
        public const string KEY_PLANT_ENERGY = "k_PlantEnergy";
        public const string KEY_FISH_ENERGY = "k_FishEnergy";

        // Water wheel
        public const string KEY_WATER_WHEEL_POWER = "k_WaterWheelPower";
        public const string KEY_WATER_WHEEL_CAPACITY_LEVEL = "k_WaterWheelCapacityLevel";
        public const string KEY_WATER_WHEEL_ENERGY_LEVEL = "k_WaterWheelEnergyLevel";

        // Plant
        // Bamboo
        public const string KEY_PLANT_BAMBOO_UNLOCK = "k_PlantBambooUnlock";
        public const string KEY_PLANT_BAMBOO_ENERGY = "k_PlantBambooEnergy";

        // Reed
        public const string KEY_PLANT_REED_UNLOCK = "k_PlantReedUnlock";
        public static readonly string[] KEY_PLANT_REED_ENERGY = { "k_PlantReedEnergy_1", "k_PlantReedEnergy_2", "k_PlantReedEnergy_3" };

        // Mushroom
        public const string KEY_PLANT_MUSHROOM_UNLOCK = "k_PlantMushroomUnlock";
        public static readonly string [] KEY_PLANT_MUSHROOM_ENERGY = { "k_PlantMushroomEnergy_1", "k_PlantMushroomEnergy_2", "k_PlantMushroomEnergy_3" };

        // Tree
        public const string KEY_PLANT_TREE_UNLOCK = "k_PlantTreeUnlock";
        public static readonly string [] KEY_PLANT_TREE_ENERGY = { "k_PlantTreeEnergy_1", "k_PlantTreeEnergy_2", "k_PlantTreeEnergy_3" };

        // Sakura
        public const string KEY_PLANT_SAKURA_UNLOCK = "k_PlantSakuraUnlock";
        public static readonly string [] KEY_PLANT_SAKURA_ENERGY = { "k_PlantSakuraEnergy_1", "k_PlantSakuraEnergy_2", "k_PlantSakuraEnergy_3" };

        // Pond
        // Koi
        public const string KEY_POND_KOI_UNLOCK = "k_PondKoiUnlock";
        // Turtle
        public const string KEY_POND_TURTLE_UNLOCK = "k_PondTurtleUnlock";
        // Snails
        public const string KEY_POND_SNAILS_UNLOCK = "k_PondSnailsUnlock";
        // Frog
        public const string KEY_POND_FROG_UNLOCK = "k_PondFrogUnlock";
        // Dragonfly
        public const string KEY_POND_DRAGONFLY_UNLOCK = "k_PondDragonflyUnlock";

        // Offline
        public const string KEY_EXIT_TIME = "k_ExitTime";

    #endregion

    #region Level

        // 等級
        private static int _stage;
        public static int Stage {
            get { return _stage; }
            set {
                _stage = value;
                Save ();
            }
        }

    #endregion

    #region focus and savvy level
    private static int _focusLevel = 0;
    public static int FocusLevel
    {
        get { return _focusLevel; }
        set
        {
            _focusLevel = value;
            Save();
        }
    }

    private static int _savvyLevel = 0;
    public static int SavvyLevel
    {
        get { return _savvyLevel; }
        set
        {
            _savvyLevel = value;
            Save();
        }
    }
    #endregion

    #region Energy

    // 水能量
    private static float _waterEnergy = 0;
        public static float WaterEnergy {
            get { return _waterEnergy; }
            set {
                _waterEnergy = value;
                Save ();
            }

        }

        // 植物能量
        private static int _plantEnergy = 0;
        public static int PlantEnergy {
            get { return _plantEnergy; }
            set {
                _plantEnergy = value;
                Save ();
            }

        }

        // 魚能量
        private static int _fishEnergy = 0;
        public static int FishEnergy {
            get { return _fishEnergy; }
            set {
                _fishEnergy = value;
                Save ();
            }

        }

    #endregion

    #region Water_Wheel

        // 水車容量等級
        private static int _waterWheelCapacityLevel = 0;
        public static int WaterWheelCapacityLevel {
            get { return _waterWheelCapacityLevel; }
            set {
                _waterWheelCapacityLevel = value;
                Save ();
            } 
        }

        // 水車能量等級
        private static int _waterWheelEnergyLevel = 0;
        public static int WaterWheelEnergyLevel {
            get { return _waterWheelEnergyLevel; }
            set {
                _waterWheelEnergyLevel = value;
                Save ();
            } 
        }

        // 水車Power
        private static float _waterWheelPower = 0;
        public static float WaterWheelPower {
            get { return _waterWheelPower; }
            set {
                _waterWheelPower = value;
                Save ();
            } 
        }

    #endregion

    #region Plant

        #region Bamboo

            // 竹的等級
            private static int _bambooLevel = 0;
            public static int BambooLevel {
                get { return _bambooLevel; }
                set{
                    _bambooLevel = value;
                    Save ();
                }
            }

            // 判斷竹是否已解鎖的
            public static bool BambooIsUnlock (int id) {
                return id <= _bambooLevel;
            }

            // 竹的植物能量存庫
            private static int _bambooPlantEnergy = 0;
            public static int BambooPlantEnergy {
                get { return _bambooPlantEnergy; }
                set {
                    _bambooPlantEnergy = value;
                    Save ();
                }
            }

        #endregion

        #region Reed

            // 蘆葦的等級
            private static int _reedLevel = 0;
            public static int ReedLevel {
                get { return _reedLevel; }
                set {
                    _reedLevel = value;
                    Save ();
                }
            }

            public static bool ReedIsUnlock (int id) {
                return id <= _reedLevel;
            }

            // 蘆葦的能量
            private static int[] _reedPlantEnergy = new int [3];
            public static int GetReedPlantEnergy (int index) {
                return _reedPlantEnergy [index];
            }
            public static void SetReedPlantEnergy (int index, int value) {
                // Debug.Log("Index: " + index + ", Value: " + value);
                _reedPlantEnergy [index] = value;
                Save ();
            }

        #endregion

        #region Mushroom

            // 蘑菇的等級
            private static int _mushroomLevel = 0;
            public static int MushroomLevel {
                get { return _mushroomLevel; }
                set {
                    _mushroomLevel = value;
                    Save ();
                }
            }

            public static bool MushroomIsUnlock (int id) {
                return id <= _mushroomLevel;
            }

            // 蘑菇的能量
            private static int[] _mushroomPlantEnergy = new int [3];
            public static int GetMushroomPlantEnergy (int index) {
                return _mushroomPlantEnergy [index];
            }
            public static void SetMushroomPlantEnergy (int index, int value) {
                // Debug.Log("Index: " + index + ", Value: " + value);
                _mushroomPlantEnergy [index] = value;
                Save ();
            }

        #endregion

        #region Tree

            // 樹的等級
            private static int _treeLevel = 0;
            public static int TreeLevel {
                get { return _treeLevel; }
                set {
                    _treeLevel = value;
                    Save ();
                }
            }

            public static bool TreeIsUnlock (int id) {
                return id <= _treeLevel;
            }

            // 樹的能量
            private static int[] _treePlantEnergy = new int [3];
            public static int GetTreePlantEnergy (int index) {
                return _treePlantEnergy [index];
            }
            public static void SetTreePlantEnergy (int index, int value) {
                // Debug.Log("Index: " + index + ", Value: " + value);
                _treePlantEnergy [index] = value;
                Save ();
            }

        #endregion

        #region Sakura

            // 櫻花的等級
            private static int _sakuraLevel = 0;
            public static int SakuraLevel {
                get { return _sakuraLevel; }
                set {
                    _sakuraLevel = value;
                    Save ();
                }
            }

            public static bool SakuraIsUnlock (int id) {
                return id <= _sakuraLevel;
            }

            // 櫻花的能量
            private static int[] _sakuraPlantEnergy = new int [3];
            public static int GetSakuraPlantEnergy (int index) {
                return _sakuraPlantEnergy [index];
            }
            public static void SetSakuraPlantEnergy (int index, int value) {
                // Debug.Log("Index: " + index + ", Value: " + value);
                _sakuraPlantEnergy [index] = value;
                Save ();
            }

        #endregion

    #endregion

    #region Pond

        #region Koi

            // 錦鯉的等級
            private static int _koiLevel = 0;
            public static int KoiLevel {
                get { return _koiLevel; }
                set{
                    _koiLevel = value;
                    Save ();
                }
            }

        #endregion

        #region Turtle

            // 龜的等級
            private static int _turtleLevel = 0;
            public static int TurtleLevel {
                get { return _turtleLevel; }
                set{
                    _turtleLevel = value;
                    Save ();
                }
            }

        #endregion

        #region Snails

            // 蝸牛的等級
            private static int _snailsLevel = 0;
            public static int SnailsLevel {
                get { return _snailsLevel; }
                set{
                    _snailsLevel = value;
                    Save ();
                }
            }

        #endregion

        #region Frog

            // 錦鯉的等級
            private static int _frogLevel = 0;
            public static int FrogLevel {
                get { return _frogLevel; }
                set{
                    _frogLevel = value;
                    Save ();
                }
            }

        #endregion

        #region DragonFly

            // 蜻蜓的等級
            private static int _dragonflyLevel = 0;
            public static int DragonFlyLevel {
                get { return _dragonflyLevel; }
                set{
                    _dragonflyLevel = value;
                    Save ();
                }
            }

        #endregion

    #endregion

    #region Offline

        private static DateTime _exitTime;
        public static DateTime ExitTime {
            get { return _exitTime; }
            set {
                _exitTime = value;
                Save ();
            }

        }

    #endregion

    // 是否新玩家
    private static bool _isNewPlayer;
    public static bool IsNewPlayer {
        get { return _isNewPlayer; }
        set {
            _isNewPlayer = value;
            Save ();
        }

    }

    public static void Load () {
        // 關卡等級
        _stage = PlayerPrefs.GetInt (KEY_STAGE, 1);

        //Focus
        _focusLevel = PlayerPrefs.GetInt(KEY_FOCUS, 0);
        //Savvy
        _savvyLevel = PlayerPrefs.GetInt(KEY_SAVVY, 0);

        // 新玩家
        _isNewPlayer = PlayerPrefs.GetInt (KEY_NEW_PLAYER, 1) == 1;

        // 能量
        _waterEnergy = PlayerPrefs.GetFloat (KEY_WATER_ENERGY, 0);
        _plantEnergy = PlayerPrefs.GetInt (KEY_PLANT_ENERGY, 0);
        _fishEnergy = PlayerPrefs.GetInt (KEY_FISH_ENERGY, 0);

        // 水車
        _waterWheelPower = PlayerPrefs.GetFloat (KEY_WATER_WHEEL_POWER, 0);
        _waterWheelCapacityLevel = PlayerPrefs.GetInt (KEY_WATER_WHEEL_CAPACITY_LEVEL, 0);
        _waterWheelEnergyLevel = PlayerPrefs.GetInt (KEY_WATER_WHEEL_ENERGY_LEVEL, 0);

        // 植物
        // 竹
        _bambooLevel = PlayerPrefs.GetInt (KEY_PLANT_BAMBOO_UNLOCK, 0);
        _bambooPlantEnergy = PlayerPrefs.GetInt (KEY_PLANT_BAMBOO_ENERGY, 0);

        // 蘑菇
        _mushroomLevel = PlayerPrefs.GetInt (KEY_PLANT_MUSHROOM_UNLOCK, 0);
        int length = _mushroomPlantEnergy.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_MUSHROOM_ENERGY [i];
            _mushroomPlantEnergy [i] = PlayerPrefs.GetInt (key, 0);   
        }

        // 樹
        _treeLevel = PlayerPrefs.GetInt (KEY_PLANT_TREE_UNLOCK, 0);
        length = _treePlantEnergy.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_TREE_ENERGY [i];
            _treePlantEnergy [i] = PlayerPrefs.GetInt (key, 0);   
        }

        // 蘆葦
        _reedLevel = PlayerPrefs.GetInt (KEY_PLANT_REED_UNLOCK, 0);
        length = KEY_PLANT_REED_ENERGY.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_REED_ENERGY [i];
            _reedPlantEnergy [i] = PlayerPrefs.GetInt (key, 0);
        }

        // 櫻花
        _sakuraLevel = PlayerPrefs.GetInt (KEY_PLANT_SAKURA_UNLOCK, 0);
        length = _sakuraPlantEnergy.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_SAKURA_ENERGY [i];
            _sakuraPlantEnergy [i] = PlayerPrefs.GetInt (key, 0);   
        }

        // 水塘
        // 錦鯉
        _koiLevel = PlayerPrefs.GetInt (KEY_POND_KOI_UNLOCK, 0);

        // 龜
        _turtleLevel = PlayerPrefs.GetInt (KEY_POND_TURTLE_UNLOCK, 0);

        // 蝸牛
        _snailsLevel = PlayerPrefs.GetInt (KEY_POND_SNAILS_UNLOCK, 0);

        // 青蛙
        _frogLevel = PlayerPrefs.GetInt (KEY_POND_FROG_UNLOCK, 0);

        // 蜻蜓
        _dragonflyLevel = PlayerPrefs.GetInt (KEY_POND_DRAGONFLY_UNLOCK, 0);

        // 離線系統
        _exitTime = Convert.ToDateTime (PlayerPrefs.GetString (KEY_EXIT_TIME, DateTime.Now.ToString ()));
    }

    public static void Save () 
    {
        
        // 關卡等級
        PlayerPrefs.SetInt (KEY_STAGE, _stage);

        //Focus
        PlayerPrefs.SetInt(KEY_FOCUS, _focusLevel);

        //Savvy
        PlayerPrefs.SetInt(KEY_SAVVY, _savvyLevel);

        // 新玩家
        PlayerPrefs.SetInt (KEY_NEW_PLAYER, _isNewPlayer ? 1 : 0);

        // 能量
        PlayerPrefs.SetFloat (KEY_WATER_ENERGY, _waterEnergy);
        PlayerPrefs.SetInt (KEY_PLANT_ENERGY, _plantEnergy);
        PlayerPrefs.SetInt (KEY_FISH_ENERGY, _fishEnergy);

        // 水車
        PlayerPrefs.SetFloat (KEY_WATER_WHEEL_POWER, _waterWheelPower);
        PlayerPrefs.SetInt (KEY_WATER_WHEEL_CAPACITY_LEVEL, _waterWheelCapacityLevel);
        PlayerPrefs.SetInt (KEY_WATER_WHEEL_ENERGY_LEVEL, _waterWheelEnergyLevel);

        // 植物
        // 竹
        PlayerPrefs.SetInt (KEY_PLANT_BAMBOO_UNLOCK, _bambooLevel);
        PlayerPrefs.SetInt (KEY_PLANT_BAMBOO_ENERGY, _bambooPlantEnergy);

        // 蘑菇
        PlayerPrefs.SetInt (KEY_PLANT_MUSHROOM_UNLOCK, _mushroomLevel);
        int length = KEY_PLANT_MUSHROOM_ENERGY.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_MUSHROOM_ENERGY [i];
            PlayerPrefs.SetInt (key, _mushroomPlantEnergy [i]);
        }

        // 樹
        PlayerPrefs.SetInt (KEY_PLANT_TREE_UNLOCK, _treeLevel);
        length = KEY_PLANT_TREE_ENERGY.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_TREE_ENERGY [i];
            PlayerPrefs.SetInt (key, _treePlantEnergy [i]);
        }

        // 蘆葦
        PlayerPrefs.SetInt (KEY_PLANT_REED_UNLOCK, _reedLevel);
        length = KEY_PLANT_REED_ENERGY.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_REED_ENERGY [i];
            PlayerPrefs.SetInt (key, _reedPlantEnergy [i]);
        }
        
        // 櫻花
        PlayerPrefs.SetInt (KEY_PLANT_SAKURA_UNLOCK, _sakuraLevel);
        length = KEY_PLANT_SAKURA_ENERGY.Length;
        for (int i = 0; i < length; i++) {
            string key = KEY_PLANT_SAKURA_ENERGY [i];
            PlayerPrefs.SetInt (key, _sakuraPlantEnergy [i]);
        }

        // 水塘
        // 錦鯉
        PlayerPrefs.SetInt (KEY_POND_KOI_UNLOCK, _koiLevel);

        // 龜
        PlayerPrefs.SetInt (KEY_POND_TURTLE_UNLOCK, _turtleLevel);

        // 蝸牛
        PlayerPrefs.SetInt (KEY_POND_SNAILS_UNLOCK, _snailsLevel);

        // 青蛙
        PlayerPrefs.SetInt (KEY_POND_FROG_UNLOCK, _frogLevel);

        // 蜻蜓
        PlayerPrefs.SetInt (KEY_POND_DRAGONFLY_UNLOCK, _dragonflyLevel);

        // 離線系統
        PlayerPrefs.SetString (KEY_EXIT_TIME, _exitTime.ToString ());
    }

}
