﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void GameStateChangedEventHandler (EnumGameState oldState, EnumGameState newState);

public class GameManager : Singleton<GameManager> {

    // 游戲狀態
    private EnumGameState _state = EnumGameState.Garden;
    public EnumGameState State {

        set {

            if (value != _state) {
                EnumGameState oldState = _state;
                _state = value;

                // switch (_state) {
                //     case EnumGameState.Garden:
                //         InMain ();
                //         break;
                //     case EnumGameState.WaterWheel:
                //         InWaterWheel ();
                //         break;
                //     case EnumGameState.Watch:

                //         break;
                // }

                if (null != GameStateChanged) {
                    GameStateChanged (oldState, _state);
                }

            }

        }

        get { return _state; }
    }

    // 游戲狀態改變事件
    public event GameStateChangedEventHandler GameStateChanged;

    // protected override void OnInit() {
    //     InMain ();
    // }

    // private void InMain () {

    // }

    // private void InWaterWheel () {

    // }

}
