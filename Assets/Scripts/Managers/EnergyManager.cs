﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void EnergyDataChangedEventHandler ();

public class EnergyManager : Singleton<EnergyManager> {

    public float[] plantEnergyIncreased;
    public int[] fishEnergyIncreased;

    public bool[] plantEnergyCrop ;  // 0= bamboo  1 = mushroom 2 = reed  3 = tree 4 = sakura
    public bool[] fishEnergyCrop  ;  //  0 = koi    1 = turtle   2 = snail 3 = frog 4 = dragonfly

    //public float oriPlantEnergy;



    private float _waterEnergy;
    public float WaterEnergy {
        get { return _waterEnergy; }
        set {
            _waterEnergy = value;

            if (EnergyDataChanged != null) {
                EnergyDataChanged ();
            }
            
            DataManager.WaterEnergy = _waterEnergy;
        }

    }

    private int _plantEnergy;
    public int PlantEnergy {
        get { return _plantEnergy; }
        set {
            _plantEnergy = value;

            if (EnergyDataChanged != null) {
                EnergyDataChanged ();
            }

            DataManager.PlantEnergy = _plantEnergy;
        }

    }

    private int _fishEnergy;
    public int FishEnergy {
        get { return _fishEnergy; }
        set {
            _fishEnergy = value;

            if (EnergyDataChanged != null) {
                EnergyDataChanged ();
            }

            DataManager.FishEnergy = _fishEnergy;
        }

    }

    private float _zenPower;
    public float ZenPower
    {
        get { return _zenPower; }
        set
        {
            _zenPower = value;

            if (EnergyDataChanged != null)
            {
                EnergyDataChanged();
            }
        }

    }



    public event EnergyDataChangedEventHandler EnergyDataChanged;

    protected override void OnInit() {
        WaterEnergy = DataManager.WaterEnergy;
        PlantEnergy = DataManager.PlantEnergy;
        FishEnergy = DataManager.FishEnergy;
    }

}
