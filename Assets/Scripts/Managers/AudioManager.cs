﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Sound
{
    public string name;

    public AudioClip clip;
    [Range(0f, 1f)]
    public float volume;
    [Range(0.1f, 3f)]
    public float pitch;
    public bool loop;
    [HideInInspector]
    public AudioSource source;
    public bool mute;
}


public class AudioManager : MonoBehaviour {

    public static AudioManager Instance;

    public Sound[] sounds;

    public bool _isBGM1On, _isBGM2On, _isEmptyOn;
    public bool _isMute;

    private void Awake()
    {
            if (Instance == null)
        {
            Instance = this;
        }
            else if (Instance != this)
        {
            Destroy(gameObject); 
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.mute = s.mute;

        }
    }

    private void Start()
    {
        Play("BGM1");
        _isBGM1On = true;
    }


    public void Play(string name)

    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            return;
        }
        s.source.Play();
    }

    public void Mute(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }

        s.source.Stop();
    }

    public void RandomPlay()
    {
        if(_isMute == false)
        {
            int r = UnityEngine.Random.Range(1, 14);
            Play("Final_ingame_" + r);
        }
    }

    public void muteCheck()
    {
        if (_isEmptyOn == true)
        {
            _isMute = true;
        }
        else
        {
            _isMute = false;
        }
    }

}
