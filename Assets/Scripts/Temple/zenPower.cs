﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zenPower : MonoBehaviour {

    private GameObject player;
    private float speed;
    private float t;
    private void Start()
    {
        t = 0f;
        speed = 6f;
    }
    void Update () 
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);

            t += Time.deltaTime;
            if (t>=0.5)
            {
                speed += 0.5f;
                t = 0f;
            }
            if (transform.position == player.transform.position)
            {
                Destroy(this.gameObject);
                EnergyManager.Instance.ZenPower++;
            }
        }
	}
}
