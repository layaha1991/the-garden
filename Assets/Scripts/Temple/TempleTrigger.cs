﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TempleTrigger : MonoBehaviour {

    public GameObject player;
    public GameObject playerSpawnPoint;
    public Animator templeBackgroundAni;
    public Animator templeTriggerAni;



    private bool _playerSpawning;

    private void Start()
    {
       
    }
    void OnMouseDown ()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (_playerSpawning == false)
        {
            Instantiate(player, playerSpawnPoint.transform.position, Quaternion.identity);
            templeBackgroundAni.Play("temple_backgroundAni");
            templeTriggerAni.Play("temple_triggerAni");
            _playerSpawning = true;
            GameManager.Instance.State = EnumGameState.StartZenPractice;
            StartCoroutine(whenTriggerAniFinish());

        }
     
    }

    private IEnumerator whenTriggerAniFinish()
    {
        // wait until the animation of the triggerAni finish
        yield return new WaitForSeconds(3f);
        StopCoroutine("whenTriggerAniFinish");
        this.gameObject.SetActive(false);
        _playerSpawning = false;


    }


}
