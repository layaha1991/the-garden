﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stubborn : MonoBehaviour {

    public float speed;
    private Vector3 playerPos;

    private Vector3 direction;
	// Use this for initialization

	void Start () 
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        Destroy(gameObject, 20f);
        direction = playerPos - transform.position;
	
    }
	
	// Update is called once per frame
	void Update () 
    {
        transform.Translate(direction * speed * Time.deltaTime);

    
	}
}
