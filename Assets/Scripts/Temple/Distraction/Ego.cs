﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ego : MonoBehaviour {
    
    [SerializeField]
    private GameObject player;
    private float speed;
    private float t;

    private void Start()
    {
        
        speed = 6f;
        t = 0f;
        Destroy(gameObject, 10f);
    }
    // Update is called once per frame
    void Update () 
    {
        t += Time.deltaTime;
        if(t>0.1)
        {
            speed += 0.1f;
            t = 0;
        }
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        }
        else
        {
            return;
        }

	}


}
