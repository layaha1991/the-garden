﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pain : MonoBehaviour {

    private Rigidbody rb;
    public float forceX;
    public float forceY;

    private void Start()
    {

        rb = GetComponent<Rigidbody>();
        Vector3 direction = new Vector3(forceX, forceY, 0);
        rb.AddForce(direction);
        Destroy(gameObject, 10f);

    }
    private void Update()
    {
      
    }


}
