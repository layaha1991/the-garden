﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HateFireAdvance : MonoBehaviour {
    
	public GameObject player;
    public Vector3 playerPos;
    public float speed;
    private Vector3 direction;
    [SerializeField]
    private GameObject _hateTempPrefab;


	void Start () 
    {
        speed = 0.4f;
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
            direction = playerPos - transform.position;
            Destroy(gameObject, 10f);
        }
        StartCoroutine(HateFireAdvanceRoutine());
   
	}

    private void Update()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }

    private IEnumerator HateFireAdvanceRoutine()
    {
        var i = 0;
        while (i <= 120)
        {
            i++;
            yield return new WaitForSeconds(0.01f);
           
        }
        while (i>120)
        {
            Instantiate(_hateTempPrefab, transform.localPosition, Quaternion.identity);
            StopCoroutine(HateFireAdvanceRoutine());
            Destroy(gameObject);
            yield return null;
        }


    }
		
}
