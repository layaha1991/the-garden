﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HateAdvance : MonoBehaviour
{

    private float radian = 0;
    [SerializeField]
    private float perRadian = 0.05f;
    [SerializeField]
    private float radius = 1f;
    public float CD;
    Vector3 oldPos;

    public GameObject HateFireAdvance;
    public GameObject player;


    public float t;

    private bool _increaseRadius;


    private void Start()
    {
        oldPos = transform.position;
        CD = 3f;
    }
    void Update()
    {
        movement();
        hateFire();
    }

    private void movement()
    {
        radian += perRadian;
        float dy = Mathf.Cos(radian) * radius;
        float dx = Mathf.Sin(radian) * radius;
        transform.position = oldPos + new Vector3(dx, dy, 0);
    }

    void hateFire()

    {
        t += Time.deltaTime;
        if (t > CD)
        {
            Instantiate(HateFireAdvance, transform.position, transform.rotation);
            t = 0f;
            CD -= 0.3f;
            if (CD <= 1)
            {
                CD = 1f;
            }
            if (radius >= 8)
            {
                _increaseRadius = false;
            } else if (radius <=5)
            {
                _increaseRadius = true;
            }

            if (_increaseRadius == false)
            {
                radius -= 0.5f;
            }else if (_increaseRadius ==true)
            {
                radius += 0.5f;
            }
           
           
        }



    }

}
