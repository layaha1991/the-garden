﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hate : MonoBehaviour {

    private float radian = 0;
    [SerializeField]
    private float perRadian = 0.01f;
    [SerializeField]
    private float radius = 3f;
    public float CD;
    Vector3 oldPos;

    public GameObject HateFire;
    public GameObject player;
    public float t;

    private void Start()
    {
        oldPos = transform.position;
        CD = 2f;
    }
    void Update ()
    {
        movement();
        hateFire();
    }

    private void movement()
    {
        radian += perRadian;
        float dy = Mathf.Cos(radian) * radius;
        float dx = Mathf.Sin(radian) * radius;
        transform.position = oldPos + new Vector3(dx, dy, 0);
    }

    void hateFire()

    {
            t += Time.deltaTime;
            if (t > CD)
            {
                Instantiate(HateFire, transform.position, transform.rotation);
                t = 0f;
                CD -= 0.01f;
            if (CD <=0.3)
            {
                CD = 0.3f;
            }
            }



    }
        
}
