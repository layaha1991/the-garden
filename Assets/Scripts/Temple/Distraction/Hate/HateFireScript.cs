﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HateFireScript : MonoBehaviour {
    
	public GameObject player;
    public Vector3 playerPos;
	public float speed;
    private Vector3 direction;

	void Start () {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
            direction = playerPos - transform.position;
            Destroy(gameObject, 10f);
        }
   
	}

	void Update () {
        transform.Translate(direction * speed * Time.deltaTime);
	}

		
}
