﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Envy_2 : MonoBehaviour 
{
    private float t;
    private float timeBetweenDash = 3f;
    public float speed;



    private Vector3 destination;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Envy")
        {
            Destroy(this.gameObject);
        }
    }


    private void Start()
    {
       
        destination = ((DistractionManager.Instance.Envy_2Pos + DistractionManager.Instance.Envy_1Pos) / 2);
        timeBetweenDash = 1f;
    }
    private void Update()
    {
        t += Time.deltaTime;
        if (t > timeBetweenDash)
        {
            dash();
        }
        if (transform.position == destination)
        {
            Destroy(this.gameObject);
        }
    }

    private void dash()
    {
        //Debug.Log("dash_2");
        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
     
    }

}
