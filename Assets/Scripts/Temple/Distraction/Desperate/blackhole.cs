﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackhole : MonoBehaviour
{

    public float strengthOfPull;
    public float pullMultiplier;
    public Collider[] paincollider;

    public GameObject pain;


    private float t;

    void Start()
    {
        Destroy(gameObject, 7f);
    }

    void Update()
    {
        blackholeFunction();
        spawnPainer();
    }

    private void blackholeFunction()
    {
        paincollider = Physics.OverlapSphere(transform.position, 10000f);

        foreach (var col in paincollider)
        {
            Rigidbody rb = col.GetComponent<Rigidbody>();

            if (rb == null) continue;

            Vector3 direction = transform.position - col.transform.position;

            float distance = direction.sqrMagnitude * pullMultiplier + 1; // The distance formula

            // Object mass also affects the gravitational pull
            rb.AddForce(direction.normalized * (strengthOfPull / distance) * rb.mass * Time.fixedDeltaTime);


        }
    }

    private void spawnPainer()
    {
        t += Time.deltaTime; 
        if (t>2.3f) // 3 seconds delay
        {
            Vector3 pos = RandomCircle(transform.position, 4.0f);
            Instantiate(pain, pos, Quaternion.identity);
            t = 2f;
        }
    }

    Vector3 RandomCircle(Vector3 blackhole, float radius)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = blackhole.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = blackhole.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = blackhole.z;
        return pos;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "pain")
        {
            Destroy(col.gameObject);
        }
    }
}
