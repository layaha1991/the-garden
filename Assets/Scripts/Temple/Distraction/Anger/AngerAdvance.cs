﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngerAdvance : MonoBehaviour {


    /**[SerializeField]

    private float _radian = 0;
    [SerializeField]
    private float _perRadian = 0.1f;
    [SerializeField]
    private float _radius = 0.1f;**/
    [SerializeField]
    private float destroyTimer = 12f;
    private Vector3 oldPos;
    private int numberOfShot;
    private float t;
    private float cd;

    public GameObject Shot;


    void Start () {
        oldPos = transform.position;
        numberOfShot = Random.Range(18, 24);
        cd = 3f;
        Destroy(gameObject, destroyTimer);
    }

    void Update(){
        
  
       
        //floating ();
        rotating ();
        shooting();
    }

    /**private void floating () {
        _radian += _perRadian;
        float dy = Mathf.Cos (_radian) * _radius;
        float dx = Mathf.Sin (_radian) * _radius;
        transform.position = oldPos + new Vector3 (dx, dy, 0);
    }**/

    void rotating(){
        transform.Rotate (new Vector3 (0,0,5) * Time.deltaTime); 
    }

    void shooting()
    {
        t += Time.deltaTime;
        if (t> cd)
        {
            for (int i = 0; i < numberOfShot; i++)
            {
                Instantiate(Shot, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
            }
            t = 0;
            cd -= 0.3f;
        }
    }


}
