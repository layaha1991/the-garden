﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngerFireAdvance : MonoBehaviour
{
    [SerializeField]
    private float Speed = 3f;
    private void Start()
    {
        StartCoroutine(Fire());
        Destroy(gameObject, 8f);

    }

    private IEnumerator Fire()
    {
        var i = 0;
        while(i<200)
        {
            transform.Translate(Vector3.up * Speed * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
            i++;
        }
        while (i>=200)
        {
            transform.Translate(Vector3.up * Speed * Time.deltaTime * 30);
            yield return new WaitForSeconds(0.01f);
        }

            
    }
}