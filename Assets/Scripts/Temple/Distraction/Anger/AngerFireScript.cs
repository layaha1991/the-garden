﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngerFireScript : MonoBehaviour {

	public float Speed;
	void Update()
    {

        transform.Translate(Vector3.up * Speed* Time.deltaTime);
        Destroy(gameObject, 10f);
	}

}
