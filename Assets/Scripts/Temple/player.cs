﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    public GameObject zenPowerPrefab; 
    public GameObject hit;
    public float distance;
    public float zenPowerCounter;
    [SerializeField]
    private bool _isDraging;
    private bool _runOnceBool;
  

    public Animator Core;
    public Animator tail;
    // Use this for initialization


    //private ParticleSystem ps;
    //private ParticleSystem.MainModule main;

    //private SphereCollider sphereCollider;

    void Start()
    {
        Core.Play("Player_Spawn");
        tail.Play("Tail_Spawn");
        PlayerManager.Instance.PlayerHP = 5f + DataManager.FocusLevel;
        PlayerManager.Instance.ZenPowerSpawnCD = 2f - (DataManager.SavvyLevel * 0.15f);
        _runOnceBool = false;
        PlayerManager.Instance._isPlayerDead = false;
        //ps = GetComponent<ParticleSystem>();
        //main = ps.main;
        //sphereCollider = GetComponent<SphereCollider>();

        

    }

    // Update is called once per frame
    void Update()
    {
        playerDie();
        if (_isDraging == true)
        {
            spawnZenEnergy();
        }
      
    }

    private void OnMouseDrag()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPos = Camera.main.ScreenToWorldPoint(mousePos);
        transform.position = objPos;
        _isDraging = true;
    }
    private void OnMouseUp()
    {
        _isDraging = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Distraction" || other.gameObject.tag == "Envy")
        {
            var newHit = Instantiate(hit, transform.position, Quaternion.identity);
            newHit.transform.parent = gameObject.transform;
            PlayerManager.Instance.PlayerHP --;
        }
        if (other.gameObject.tag == "Bullet") 
        {
            var newHit = Instantiate(hit, transform.position, Quaternion.identity);
            newHit.transform.parent = gameObject.transform;
            PlayerManager.Instance.PlayerHP--;
            Destroy(other.gameObject);
        }
        if(other.gameObject.tag == "Ego")
        {
            var newHit = Instantiate(hit, transform.position, Quaternion.identity);
            newHit.transform.parent = gameObject.transform;
            //main.startSizeMultiplier = 10f;
            //Debug.Log("hitEgo");
            //sphereCollider.radius += 0.1f;
            PlayerManager.Instance.PlayerHP--;
            Destroy(other.gameObject);
        }
    }

    #region Animation
    void playerDie()
    {
        
        if (PlayerManager.Instance.PlayerHP <0)
            {
            PlayerManager.Instance.PlayerHP = 0;
            }
            
        
        if (PlayerManager.Instance.PlayerHP <= 0 && _runOnceBool == false)
        {
            PlayerManager.Instance._isPlayerDead = true;
            playerDieAni();
            Destroy(GameObject.FindGameObjectWithTag("Distraction"));
            Destroy(GameObject.FindGameObjectWithTag("Bullet"));
            Destroy(GameObject.FindGameObjectWithTag("zenPower"));
            _runOnceBool = true;
            Destroy(this.gameObject, 6f);
           
        }
    }

    void playerDieAni()
    {
        Core.Play("Player_Die");
        tail.Play("Tail_Die");
    }

    #endregion

    #region spawn Zen Power

    public void spawnZenEnergy()
    {
        zenPowerCounter += Time.deltaTime;
        if (zenPowerCounter > PlayerManager.Instance.ZenPowerSpawnCD && PlayerManager.Instance._isPlayerDead == false)
        {
            Vector3 pos = RandomCircle(transform.position, Random.Range (10f,15f));
            Instantiate(zenPowerPrefab, pos, Quaternion.identity);
            zenPowerCounter = 0;
        }
    }
    Vector3 RandomCircle(Vector3 player, float radius)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = player.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = player.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = player.z;
        return pos;
    }
#endregion
}
