﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void StageChangedHandler (int stage);

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    private Animator _levelUpAni;
    [SerializeField]
    private GameObject _templeTrigger;
    [SerializeField]
    private Animator _templeBackground;



    private Vector3 _oriScale;
    private SpriteRenderer _oriColor;

    public bool _shouldReset;
    private bool _spawned;
    public bool _allMenuOn;

    private float levelCounter;
    private float t_pass;
    public float passScore;

    private int _stage;
    public int Stage
    {
        get { return _stage; }
        private set
        {
            _stage = value;
            DataManager.Stage = _stage;

            if (StageChanged != null)
            {
                StageChanged(_stage);
            }

        }

    }

    public event StageChangedHandler StageChanged;

    protected override void OnInit()
    {
        _stage = DataManager.Stage;
        //_stage = 100;
    }


    private void Start()
    {
        _oriScale = _templeTrigger.transform.localScale;
        _oriColor = _templeTrigger.GetComponent<SpriteRenderer>();

    }



    void Update()
    {
        /**if (Input.GetKeyDown (KeyCode.Space))
        {
            Stage++;
        }**/

        pass();
        resetTemple();
        // need UI to indicate the time
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            levelCounter += Time.deltaTime;
            if (levelCounter > 5f)
            {
                spawnFunction();

            }
        }

    
    }


    private void pass()
    {
        passScore = (0 + (10 * _stage));
        if (EnergyManager.Instance.ZenPower >= passScore)
        {
            StopAllCoroutines();
            Destroy(GameObject.FindGameObjectWithTag("Distraction"));
            Destroy(GameObject.FindGameObjectWithTag("Bullet"));
            Destroy(GameObject.FindGameObjectWithTag("Player"));
            Destroy(GameObject.FindGameObjectWithTag("zenPower"));
            _levelUpAni.Play("Level");
            EnergyManager.Instance.ZenPower = 0f; // no longer looping (or this routine will be stopped by stop all coroutine
            StartCoroutine(passMenuOnRoutine());

        }
    }

    private IEnumerator passMenuOnRoutine()
    {
        yield return new WaitForSeconds(5f);
        Stage++;
        yield return new WaitForSeconds(3f);
        GameManager.Instance.State = EnumGameState.Main;
        _shouldReset = true;
        CameraManager.Instance.Smooth = true;
        StopCoroutine(passMenuOnRoutine());

    }
    #region Reset Temple
    private void resetTemple()
    {
        /**if (GameManager.Instance.State == EnumGameState.InsideTemple || GameManager.Instance.State == EnumGameState.StartZenPractice)
        {
            _shouldReset = true;
        }**/
        if (_shouldReset == true)
        {

            EnergyManager.Instance.ZenPower = 0;
            StopAllCoroutines();
            DestroyGameObject("Distraction");
            DestroyGameObject("Bullet");
            DestroyGameObject("Player");
            DestroyGameObject("zenPower");
            _templeTrigger.transform.localScale = _oriScale;
            _oriColor.color = new Color(255, 255, 255, 255);

            // templeBackground back to default
            _templeBackground.Play("background_Reset");
            _templeTrigger.SetActive(true);
            _shouldReset = false; // for reset
            _spawned = false; // for level design
            levelCounter = 0; // prevent from spawning enemy before the player
        }
    }

    private void DestroyGameObject(string a)
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(a);
        foreach (GameObject target in gameObjects)
        {
            GameObject.Destroy(target);
        }
    }
    #endregion


    private void spawnFunction()
    {
        if (_stage == 1 && _spawned == false)
        {
            StartCoroutine(level_1());
            _spawned = true;
        }

        if (_stage == 2 && _spawned == false)
        {
            StartCoroutine(level_2());
            _spawned = true;
        }
        if (_stage == 3 && _spawned == false)
        {
            StartCoroutine(level_3());
            _spawned = true;
        }
        if (_stage == 4 && _spawned == false)
        {
            StartCoroutine(level_4());
            _spawned = true;
        }

        if (_stage == 5 && _spawned == false)
        {
            StartCoroutine(level_5());
            _spawned = true;
        }

        if (_stage == 6 && _spawned == false)
        {
            StartCoroutine(level_6());
            _spawned = true;
        }
        if (_stage == 7 && _spawned == false)
        {
            StartCoroutine(level_7());
            _spawned = true;
        }

        if (_stage == 8 && _spawned == false)
        {
            StartCoroutine(level_8());
            _spawned = true;
        }

        if (_stage == 9 && _spawned == false)
        {
            StartCoroutine(level_9());
            _spawned = true;
        }
        if (_stage == 10 && _spawned == false)
        {
            StartCoroutine(level_10());
            _spawned = true;
        }

        if (_stage == 11 && _spawned == false)
        {
            StartCoroutine(level_11());
            _spawned = true;
        }

        if (_stage == 12 && _spawned == false)
        {
            StartCoroutine(level_12());
            _spawned = true;
        }
        if (_stage == 13 && _spawned == false)
        {
            StartCoroutine(level_13());
            _spawned = true;
        }

        if (_stage == 14 && _spawned == false)
        {
            StartCoroutine(level_14());
            _spawned = true;
        }

        if (_stage == 15 && _spawned == false)
        {
            StartCoroutine(level_15());
            _spawned = true;
        }
    }

    #region levelDesign  
    private IEnumerator level_1()
    {
        DistractionManager.Instance.spawnHate();
        yield return null;

    }
    private IEnumerator level_2()
    {
        while (true)
        {
            DistractionManager.Instance.spawnEnvy();
            yield return new WaitForSeconds(1f);
        }
    }



    private IEnumerator level_3()
    {
        float t = 0;
        while (true)
        {
            DistractionManager.Instance.spawnStubborn();
          
            if (t >= 1.5f)
            {
                t = 1.5f;
            }

            Debug.Log("t_stubborn" + t);
            Debug.Log("CD:" + (Random.Range(4, 6) - t));

            yield return new WaitForSeconds(Random.Range(4, 6) - t);

        }

    }
    private IEnumerator level_4()
    {
        float t = 10f;
        while (true)
        {
            DistractionManager.Instance.spawnDesperate();
            yield return new WaitForSeconds(t);
            t -= 0.5f;
            if (t <= 7)
            {
                t = 7;
            }
        }
    }

    private IEnumerator level_5()
    {
        
        while (true)
        { 
            DistractionManager.Instance.spawnEgo();
            yield return new WaitForSeconds(1f);
            DistractionManager.Instance.spawnEgo();
            yield return new WaitForSeconds(1f);
            DistractionManager.Instance.spawnEgo();
            yield return new WaitForSeconds(1f);
            DistractionManager.Instance.spawnEgo();
            yield return new WaitForSeconds(10f);
        }
    }

    private IEnumerator level_6()
    {
        float t = 8;
        while (true)
        {
            DistractionManager.Instance.spawnAnger();
            yield return new WaitForSeconds(t);

        }
    }

    private IEnumerator level_7()
    {

        while (true)
        {
            DistractionManager.Instance.spawnStubbornLight();
            yield return new WaitForSeconds(1f);

        }
    }
    private IEnumerator level_8()
    {
        while (true)
        {
            var t = 10;
            while (true)
            {
                DistractionManager.Instance.spawnDesperate();
                yield return new WaitForSeconds(t);
                t--;
                if (t < 4)
                {
                    t = 4;
                }
            }
        }
    }

    private IEnumerator level_9()
    {
        float t = 9;
        while (true)
        {
            DistractionManager.Instance.spawnAngerAdvance();
            yield return new WaitForSeconds(t);
            t -= 0.4f;
            if (t <= 7)
            {
                t = 7;
            }
        }
    }

    private IEnumerator level_10()
    {
        float t = 3f;
        DistractionManager.Instance.spawnHate();
        yield return new WaitForSeconds(5f);
        DistractionManager.Instance.spawnHate();
        yield return new WaitForSeconds(5f);
        while (true)
        {
            DistractionManager.Instance.spawnStubbornLight();
            yield return new WaitForSeconds(t);
            t -= 0.1f;
            if (t <= 1.5)
            {
                t = 1.5f;
            }
        }

    }
    private IEnumerator level_11()
    {
        float t = 12;
        while (true)
        {
            DistractionManager.Instance.spawnAngerAdvance();
            yield return new WaitForSeconds(t);
            DistractionManager.Instance.spawnEnvy();
            yield return new WaitForSeconds(0.5f);
            DistractionManager.Instance.spawnEnvy();
            yield return new WaitForSeconds(0.5f);
            DistractionManager.Instance.spawnEnvy();
            yield return new WaitForSeconds(0.5f);
            DistractionManager.Instance.spawnEnvy();
            DistractionManager.Instance.spawnEnvy();
            DistractionManager.Instance.spawnEnvy();
            DistractionManager.Instance.spawnEnvy();
            yield return new WaitForSeconds(1f);
            t -= 0.6f;
            if (t <= 10f)
            {
                t = 10f;
            }
        }
    }
    private IEnumerator level_12()
    {
        DistractionManager.Instance.spawnHate();
        while(true)
        {
            DistractionManager.Instance.spawnEgo();
            yield return new WaitForSeconds(5f);
         
        }

    }

    private IEnumerator level_13()
    {

            DistractionManager.Instance.spawnHateAdvance();
            yield return null;
    }
    private IEnumerator level_14()
    {

        float t = 5f;
        while (true)
        {
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            DistractionManager.Instance.spawnStubbornLight();
            yield return new WaitForSeconds(t);
            t -= 0.1f;
            if (t < 4f)
            {
                t =4f;
            }
        }



    }
    private IEnumerator level_15()
    {
        float t = 5;
        while (true)
        {
            DistractionManager.Instance.spawnAnger();
            yield return new WaitForSeconds(t);
            DistractionManager.Instance.spawnAnger();
            yield return new WaitForSeconds(t);
            DistractionManager.Instance.spawnAngerAdvance();
            yield return new WaitForSeconds(t);
            t -= 0.4f;
        }
    }
}
    #endregion

  

