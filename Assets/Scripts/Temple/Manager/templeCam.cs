﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class templeCam: MonoBehaviour 
{

    public GameObject transitAniCanvas;
    public Animator transitAni;
    private bool _shouldPlayAni;
   // private float animationTime =3f ;
    private float t = 0;

    public void menuTempleFunction() 
    {
     

        if (EventSystem.current.IsPointerOverGameObject()) 
        {
            return;
        }

        switch (GameManager.Instance.State) 
        {
            case EnumGameState.Garden:
                CameraManager.Instance.Smooth = true;
                GameManager.Instance.State = EnumGameState.Temple;
                break;
        }


        transitAniCanvas.SetActive(true);
        _shouldPlayAni = true;


    }

    private void Update()
    {
        if (_shouldPlayAni ==true)
        {
            t += Time.deltaTime;
            // at 2 seoncds, the state will switch (Temple -> InsideTemple) . It will chagne the cam pos
            if (t > 2)
            {
                switch (GameManager.Instance.State)
                {
                    case EnumGameState.Temple:
                        CameraManager.Instance.Smooth = false;
                        GameManager.Instance.State = EnumGameState.InsideTemple;
                        break;
                }
                // 3 seconds to finish up the animation;
                if (t > 3)
                {
                    transitAniCanvas.SetActive(false);
                    t = 0;
                    _shouldPlayAni = false;
                }
            }
        }
    }

}
