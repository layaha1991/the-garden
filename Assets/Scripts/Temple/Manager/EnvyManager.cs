﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvyManager : MonoBehaviour
{

    public GameObject Envy_1;
    public GameObject Envy_2;
    [HideInInspector]
    public Vector3 Envy_1Pos;
    [HideInInspector]
    public Vector3 Envy_2Pos;
    [HideInInspector]
    public Vector3 Envy1_dir;
    [HideInInspector]
    public Vector3 Envy2_dir;

    public GameObject player;
    public float spawnCD;

    private float t_envy;


    void Start()
    {
        t_envy = 6f;
        spawnCD = 1f;

    }

    void Update()
    {
        t_envy += Time.deltaTime;
        if (t_envy > spawnCD)
        {
            spawnEnvy();
            t_envy = 0;
        }
    
   
    }
    void spawnEnvy()
    {
        Envy_1Pos = RandomCircle(player.transform.position, 10.0f);
        Instantiate(Envy_1, Envy_1Pos, Quaternion.identity);
        Envy_2Pos = RandomCircle(player.transform.position, 10.0f);
        Instantiate(Envy_2, Envy_2Pos, Quaternion.identity);


           /** Envy_1Pos = new Vector3(Random.Range(-20, 20), Random.Range(-10, 10), 0);
            Envy_2Pos = new Vector3(Random.Range(-20, 20), Random.Range(-10, 10), 0);
            Instantiate(Envy_1, Envy_1Pos, Quaternion.identity);
            Instantiate(Envy_2, Envy_2Pos, Quaternion.identity);
            Envy1_dir = Envy_1Pos - Envy_2Pos;
            Envy2_dir = Envy_2Pos - Envy_1Pos;**/
    }

    Vector3 RandomCircle(Vector3 player, float radius)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = player.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = player.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = player.z;
        return pos;
    }
}
