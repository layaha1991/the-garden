﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistractionManager : Singleton<DistractionManager> {

    [SerializeField]
    private GameObject _stubbornPrefab;
    [SerializeField]
    private GameObject _desperate_blackhole;

    public GameObject envy_1;
    public GameObject envy_2;

    [SerializeField]
    private GameObject _hatePrefab;
    [SerializeField]
    private GameObject _angerPrefab;
    [SerializeField]
    private GameObject _angerAdvancePrefab;
    [SerializeField]
    private GameObject _hateAdvancePrefab;
    [SerializeField]
    private GameObject _distractionSpawnPos;
    [SerializeField]
    private GameObject _stubbornLightPrefab;
    [SerializeField]
    private GameObject _egoPrefab;


    private Vector3 _stubbornPos;
    private Vector3 _angerPos;
    private Vector3 _desperatePos;
    private Vector3 _hatePos;
    private Vector3 _angerAdvancePos;
    private Vector3 _hateAdvancePos;
    private Vector3 _stubbornLightPos;
    private Vector3 _egoPos;





    private int painSpawn;
    public ParticleSystemRenderer particleSystemRenderer;


    [HideInInspector]
    public Vector3 Envy_1Pos;
    [HideInInspector]
    public Vector3 Envy_2Pos;
    [HideInInspector]
    public Vector3 Envy1_dir;
    [HideInInspector]
    public Vector3 Envy2_dir;

    private player _player;
  

    private void Update()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            _player = GameObject.FindGameObjectWithTag("Player").GetComponent<player>();
        }
        else
        {
            return;
        }
    }

    public void spawnStubborn()
    {
        _stubbornPos = RandomCircle(_player.transform.position, 40.0f);
        Instantiate(_stubbornPrefab, _stubbornPos, Quaternion.identity);
        painSpawn++;
        particleSystemRenderer.sortingOrder = 10 + painSpawn;
    }
    public void spawnEgo()
    {
        _egoPos = RandomCircle(_player.transform.position, 40.0f);
        Instantiate(_egoPrefab, _egoPos, Quaternion.identity);
    }
   

    public void spawnDesperate()
    {

        _desperatePos = RandomCircle (_player.transform.position, 8.0f);
        Instantiate(_desperate_blackhole, _desperatePos, Quaternion.identity);
    }

    public void spawnEnvy()
    {
        Envy_1Pos = RandomCircle(_player.transform.position, 8.0f);
        Instantiate(envy_1, Envy_1Pos, Quaternion.identity);
        Envy_2Pos = RandomCircle(_player.transform.position, 8.0f);
        Instantiate(envy_2, Envy_2Pos, Quaternion.identity);
    }

    public void spawnAnger()
    {
        _angerPos = RandomCircle(_player.transform.position, 7.0f);
        Instantiate(_angerPrefab, _angerPos, Quaternion.identity);
    }

    public void spawnHate()
    {
        _hatePos = RandomCircle(_player.transform.position, 10.0f);
        Instantiate(_hatePrefab, _hatePos, Quaternion.identity);
    }

    public void spawnAngerAdvance()
    {
        _angerAdvancePos = RandomCircle(_player.transform.position, 7.0f);
        Instantiate(_angerAdvancePrefab, _angerAdvancePos, Quaternion.identity);
    }

    public void spawnHateAdvance()
    {
        Instantiate(_hateAdvancePrefab, _distractionSpawnPos.transform.position, Quaternion.identity);
    }

    public void spawnStubbornLight()
    {
        _stubbornLightPos = RandomCircle(_player.transform.position, 30.0f);
        Instantiate(_stubbornLightPrefab, _stubbornLightPos, Quaternion.identity);
    }
    Vector3 RandomCircle(Vector3 player, float radius)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = player.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = player.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = player.z;
        return pos;
    }


}
