﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void TempleEnergyReqChangedHandler(float req);
public delegate void HPChangedHandler(float _playerHP);
public class PlayerManager : Singleton<PlayerManager>

{

    public bool _isPlayerDead;

    private float _playerHP;
    public float PlayerHP
    {
        get
        {
            return _playerHP;
        }
        set
        {
            _playerHP = value;
            if (HPChanged != null)
            {
                HPChanged(_playerHP);
            }

        }
    }


    private float _zenPowerSpawnCD;
    public float ZenPowerSpawnCD
    {
        get
        {
            return _zenPowerSpawnCD;
        }
        set
        {

            _zenPowerSpawnCD = value;
            if (_zenPowerSpawnCD < 0.5f)
            {
                _zenPowerSpawnCD = 0.5f;
            }
        }
    }

    public event HPChangedHandler HPChanged;
    public event TempleEnergyReqChangedHandler ReqChanged;


    public float baseFishReq;
    public float baseEnergyReq;


    private float _fishEnergyReq;
    public float FishEnergyReq
    {
        get
        {
            return _fishEnergyReq;
        }
        set
        {
            _fishEnergyReq = value;

            if (ReqChanged != null)
            {
                ReqChanged(_fishEnergyReq);
            }
        }
    }

    private float _plantEnergyReq;
    public float PlantEnergyReq
    {
        get
        {
            return _plantEnergyReq;
        }
        set
        {
            _plantEnergyReq = value;

            if (ReqChanged != null)
            {
                ReqChanged(_plantEnergyReq);
            }
        }
    }


    void Start()
    {
        _isPlayerDead = false;
    }

    public void fishLevelUp()
    {
        if (EnergyManager.Instance.FishEnergy >= FishEnergyReq)
        {
            EnergyManager.Instance.FishEnergy -= (int)FishEnergyReq;
            DataManager.FocusLevel++;
            // Debug.Log(DataManager.FocusLevel);

        }
    }

    public void plantLevelUp()
    {
        if (EnergyManager.Instance.PlantEnergy >= PlantEnergyReq)
        {
            EnergyManager.Instance.PlantEnergy -= (int)PlantEnergyReq;
            DataManager.SavvyLevel++;
            // Debug.Log(DataManager.SavvyLevel);
        }
    }

    public void FocusReq()
    {
        FishEnergyReq = baseFishReq * Mathf.Pow(1.3f, DataManager.FocusLevel);
    }

    public void SavvyReq()
    {
        PlantEnergyReq = baseEnergyReq * Mathf.Pow(1.7f, DataManager.SavvyLevel);
    }
}
