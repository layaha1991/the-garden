﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    
    [Header("Common")]
    [SerializeField] private GameObject _backHomeButtonGO;      // 返回按鈕
    [SerializeField] private GameObject _buyListButtonGO;       // 購買列表按鈕
    [SerializeField] private GameObject _settingsButtonGO;      // 設置按鈕
    //[SerializeField] private GameObject _screenShotButtonGO;   // 拍照按鈕
    [SerializeField] private GameObject _skill1ButtonGO;        // skill 1
    [SerializeField] private GameObject _skill2ButtonGO;        // skill 2
    [SerializeField] private GameObject _skill3ButtonGO;        // skill 3
    [SerializeField] private GameObject _amitabhaGO;            // amitabha gameobject
    [SerializeField] private GameObject _levelGO;               // level gameobject
    [SerializeField] private Text _zenEnergyText;               // zen energy text
    [SerializeField] private Text _waterEnergyText;             // water text
    [SerializeField] private Text _plantEnergyText;             // plant text
    [SerializeField] private Text _fishEnergyText;              // fish text
    [SerializeField] private Text _stageText;                   // level text
    [SerializeField] private Image _gardenInfo;
    [SerializeField] private Image _fishPondInfo;
    [SerializeField] private Image _waterWheelInfo;
    [SerializeField] private Image _templeInfo;
    [SerializeField] private GameObject _infoButton;
    [SerializeField] private bool _isInfoOn;
    [SerializeField] private Button _menuButton;// Main Menu
    [SerializeField] private Button _menuTempleButton;
    [SerializeField] private Button _menuWheelButton;// Main Menu
    [SerializeField] private Button _menuPondButton;// Main Menu
    [SerializeField] private Button _menuGardenButton;// Main Menu
    [SerializeField] private Animator _menuTempleAni; // Main Menu
    [SerializeField] private Animator _menuWheelAni;// Main Menu
    [SerializeField] private Animator _menuPondAni;// Main Menu
    [SerializeField] private Animator _menuGardenAni;// Main Menu
    [SerializeField] private bool _isMenuOn;
    [SerializeField] private float t_menuAni;
    [SerializeField] private GameObject transitAniCanvas;
    [SerializeField] private Animator transitAni; // the image under the Canvas
    [SerializeField] private Image _itemWheel;  // The small icon beside the item panel
    [SerializeField] private Image _itemPond;// The small icon beside the item panel
    [SerializeField] private Image _itemGarden;// The small icon beside the item panel
    [SerializeField] private Image _infoWheelImage;
    [SerializeField] private Image _infoPondImage;
    [SerializeField] private Image _infoGardenImage;
    [SerializeField] private Image _infoTempleImage;
    [SerializeField] private GameObject _optionPanel;
    [SerializeField] private Animator _optionPanelAni;
    [SerializeField] private bool _isOptionOn;
    [SerializeField] private Text _musicText;
    [SerializeField] private Text _bambooEnergyIncreaseUI;
    [SerializeField] private Text _mushroomEnergyIncreaseUI;
    [SerializeField] private Text _reedEnergyIncreaseUI;
    [SerializeField] private Text _treeEnergyIncreaseUI;
    [SerializeField] private Text _sakuraEnergyIncreaseUI;
    [SerializeField] private Text _koiEnergyIncreaseUI;
    [SerializeField] private Text _turtleEnergyIncreaseUI;
    [SerializeField] private Text _snailEnergyIncreaseUI;
    [SerializeField] private Text _frogEnergyIncreaseUI;
    [SerializeField] private Text _dragonflyEnergyIncreaseUI;

    [Header("Temple")]
    [SerializeField] private GameObject _fishLevelUpButton;
    [SerializeField] private GameObject _plantLevelUpButton;
    [SerializeField] private GameObject _fishExp;
    [SerializeField] private GameObject _plantExp;
    [SerializeField] private Image _fishExpImage;
    [SerializeField] private Image _plantExpImage;
    [SerializeField] private Text _fishExpText;
    [SerializeField] private Text _plantExpText;
    [SerializeField] private Image _zenProgressBarImage;
    [SerializeField] private GameObject _zenProgressBar;
    [SerializeField] private GameObject _hp;
    [SerializeField] private Text _hpText;
    [SerializeField] private Image _hpImage;
    [SerializeField] private Text _zenCDText;
    [SerializeField] private Text _plantReqText; // those text on the exp bar
    [SerializeField] private Text _fishReqText;// those text on the exp bar
    [SerializeField] private GameObject _restartButton;
    [SerializeField] private bool _runOnceBool;
    [SerializeField] private Text _templeIntroText1;
    [SerializeField] private Text _templeIntroText2;
    [SerializeField] private Text _templeIntroText3;


    [Header("GardenUI")]
    [SerializeField] private Button _toThePond;
    [SerializeField] private Button _toTheGarden;
    //[SerializeField] private Button _toTheTemple;
    [SerializeField] private GameObject _pondItemPanel;
    [SerializeField] private Button _fromGardenToWheel;
    [SerializeField] private Button _fromGardenToPond;
    [SerializeField] private Button _fromGardenToTemple;

    [Header("PondUI")]
    [SerializeField] private Button _fromPondToWheel;
    [SerializeField] private Button _fromPondToGarden;

    [Header("Tutorial")]
    [SerializeField] private GameObject _tut1ReqGO;
    [SerializeField] private GameObject _tut2ReqGO;
    [SerializeField] private GameObject _tut3ReqGO;
    [SerializeField] private GameObject _tut1ReqGO_toThePond;
    [SerializeField] private GameObject _tut2ReqGO_toTheGarden;
    [SerializeField] private GameObject _tut3ReqGO_toTheTemple;
    [SerializeField] private GameObject _wheelTutorial;
    [SerializeField] private Text _templeTutorialText1;
    [SerializeField] private Text _templeTutorialText2;
    [SerializeField] private Text _templeTutorialText3;
    [SerializeField] private Text _gardenCollect;
    [SerializeField] private Text _afterGardenCollect;
    [SerializeField] private Text _afterFishCollect;
    [SerializeField] private Text _clickStatue;
    [Header("WaterWheel")]
    [SerializeField] private GameObject _waterWheelItemPanel;           // 水車的panel
    [SerializeField] private Button _waterWheelUpgradeEnergyButton;
    [SerializeField] private Text _waterWheelEnergyLevelText;
    [SerializeField] private Text _waterWheelEnergyCurFuncText;
    [SerializeField] private Text _waterWheelEnergyNextFuncText;
    [SerializeField] private Text _waterWheelEnergyCostText;
    [SerializeField] private Button _waterWheelUpgradeCapacityButton;
    [SerializeField] private Text _waterWheelCapacityLevelText;
    [SerializeField] private Text _waterWheelCapacityCurFuncText;
    [SerializeField] private Text _waterWheelCapacityNextFuncText;
    [SerializeField] private Text _waterWheelCapacityCostText;

    [Header("Koi")]
    [SerializeField] private GameObject _pondKoiMask;
    [SerializeField] private Button _pondKoiUpgradeButton;
    [SerializeField] private Text _pondKoiLevelText;
    [SerializeField] private Text _pondKoiCostText;
    [SerializeField] private Text _pondKoiCurFuncText;
    [SerializeField] private Text _pondKoiNextFuncText;
    [Header("Turtle")]
    [SerializeField] private GameObject _pondTurtleMask;
    [SerializeField] private Button _pondTurtleUpgradeButton;
    [SerializeField] private Text _pondTurtleLevelText;
    [SerializeField] private Text _pondTurtleCostText;
    [SerializeField] private Text _pondTurtleCurFuncText;
    [SerializeField] private Text _pondTurtleNextFuncText;
    [Header("Snails")]
    [SerializeField] private GameObject _pondSnailsMask;
    [SerializeField] private Button _pondSnailsUpgradeButton;
    [SerializeField] private Text _pondSnailsLevelText;
    [SerializeField] private Text _pondSnailsCostText;
    [SerializeField] private Text _pondSnailsCurFuncText;
    [SerializeField] private Text _pondSnailsNextFuncText;
    [Header("Frog")]
    [SerializeField] private GameObject _pondFrogMask;
    [SerializeField] private Button _pondFrogUpgradeButton;
    [SerializeField] private Text _pondFrogLevelText;
    [SerializeField] private Text _pondFrogCostText;
    [SerializeField] private Text _pondFrogCurFuncText;
    [SerializeField] private Text _pondFrogNextFuncText;
    [Header("DragonFly")]
    [SerializeField] private GameObject _pondDragonFlyMask;
    [SerializeField] private Button _pondDragonFlyUpgradeButton;
    [SerializeField] private Text _pondDragonFlyLevelText;
    [SerializeField] private Text _pondDragonFlyCostText;
    [SerializeField] private Text _pondDragonFlyCurFuncText;
    [SerializeField] private Text _pondDragonFlyNextFuncText;

    [Header("Plant")]
    [SerializeField] private GameObject _gardenItemPanel;       // 花園的panel
    [Header("Bamboo")]
    [SerializeField] private GameObject _gardenBambooMask;
    [SerializeField] private Button _gardenBambooUpgradeButton;
    [SerializeField] private Text _gardenBambooLevelText;
    [SerializeField] private Text _gardenBambooCostText;
    [SerializeField] private Text _gardenBambooCurFuncText;
    [SerializeField] private Text _gardenBambooNextFuncText;
    [Header("Reed")]
    [SerializeField] private GameObject _gardenReedMask;
    [SerializeField] private Button _gardenReedUpgradeButton;
    [SerializeField] private Text _gardenReedLevelText;
    [SerializeField] private Text _gardenReedCostText;
    [SerializeField] private Text _gardenReedCurFuncText;
    [SerializeField] private Text _gardenReedNextFuncText;
    [Header("Mushroom")]
    [SerializeField] private GameObject _gardenMushroomMask;
    [SerializeField] private Button _gardenMushroomUpgradeButton;
    [SerializeField] private Text _gardenMushroomLevelText;
    [SerializeField] private Text _gardenMushroomCostText;
    [SerializeField] private Text _gardenMushroomCurFuncText;
    [SerializeField] private Text _gardenMushroomNextFuncText;
    [Header("Tree")]
    [SerializeField] private GameObject _gardenTreeMask;
    [SerializeField] private Button _gardenTreeUpgradeButton;
    [SerializeField] private Text _gardenTreeLevelText;
    [SerializeField] private Text _gardenTreeCostText;
    [SerializeField] private Text _gardenTreeCurFuncText;
    [SerializeField] private Text _gardenTreeNextFuncText;
    [Header("Sakura")]
    [SerializeField] private GameObject _gardenSakuraMask;
    [SerializeField] private Button _gardenSakuraUpgradeButton;
    [SerializeField] private Text _gardenSakuraLevelText;
    [SerializeField] private Text _gardenSakuraCostText;
    [SerializeField] private Text _gardenSakuraCurFuncText;
    [SerializeField] private Text _gardenSakuraNextFuncText;

    [Header("TransitionAnimation")]
    // This is an object with black out animation
    [SerializeField] private GameObject transitionAni;

    // Menu item panels active status
    private bool _gardenItemPanelActive, _waterWheelItemPanelActive, _pondItemPanelActive;

    private bool _tut1, _tut2, _tut3;  // tut1 unlock pond   // tut2 unlock garden //tut3 unlock temple

    private IEnumerator _restartCo;
    private IEnumerator _gardenUICo;
    private IEnumerator _templeTextCo;
    private IEnumerator _templeTutorialTextCo;

    private void Start()
    {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
        WaterWheel.Instance.PowerChanged += OnWaterWheelPowerChanged;
        EnergyManager.Instance.EnergyDataChanged += OnEnergyDataChanged;
        LevelManager.Instance.StageChanged += OnStageLevelChanged;
        PlayerManager.Instance.HPChanged += OnHPChange;

        // 初始化UI
        SetupCommonUI();
        SetupMainUI();
        SetupWaterWheelUI();
        SetupPondUI();
        TutorialCheck();

        // 隱藏UIs
        HideItemPanels();

        // 當前場景位於Main
        InMain();




    }

    #region Common

    public void BackMenuClick()
    {
        GameManager.Instance.State = EnumGameState.Garden;
    }

    private void SetupCommonUI()
    {
        SetEnergyText();
        SetStageText(LevelManager.Instance.Stage);
    }

    private void HideItemPanels()
    {

        if (_gardenItemPanel.activeSelf)
            _gardenItemPanel.GetComponent<Animator>().SetTrigger("Hide");

        if (_waterWheelItemPanel.activeSelf)
            _waterWheelItemPanel.GetComponent<Animator>().SetTrigger("Hide");

        if (_pondItemPanel.activeSelf)
            _pondItemPanel.GetComponent<Animator>().SetTrigger("Hide");

        _gardenItemPanelActive = false;
        _waterWheelItemPanelActive = false;
        _pondItemPanelActive = false;
    }

    private void SetEnergyText()
    {
        int waterEnergy = (int)EnergyManager.Instance.WaterEnergy;
        _waterEnergyText.text = waterEnergy.ToString();
        int plantEnergy = EnergyManager.Instance.PlantEnergy;
        _plantEnergyText.text = plantEnergy.ToString();
        int fishEnergy = EnergyManager.Instance.FishEnergy;
        _fishEnergyText.text = fishEnergy.ToString();

        // 水車
        SetWaterWheelCapacityUpgradeButtonActive();
        SetWaterWheelEnergyUpgradeButtonActive();

        // 水塘
        // 錦鯉
        SetKoiUpgradeButtonActive();

        // 龜
        SetTurtleUpgradeButtonActive();

        // 蝸牛
        SetSnailsUpgradeButtonActive();

        // 青蛙
        SetFrogUpgradeButtonActive();

        // 蜻蜓
        SetDragonFlyUpgradeButtonActive();

        // 竹
        SetBambooUpgradeButtonActive();

        // 蘑菇
        SetMushroomUpgradeButtonActive();

        // 蘆葦
        SetReedUpgradeButtonActive();

        // 樹
        SetTreeUpgradeButtonActive();

        // 櫻花
        SetSakuraUpgradeButtonActive();
    }

    private void SetZenEnergyText()
    {
        _zenEnergyText.text = "Zen: " + EnergyManager.Instance.ZenPower.ToString() + "/" + LevelManager.Instance.passScore;
    }

    private void SetStageText(int stage)
    {
        _stageText.text = "Stage " + stage;

    }

    private void SetKoiEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.fishEnergyCrop[0] == true)
            
        {
            _koiEnergyIncreaseUI.text = "+" + (EnergyManager.Instance.fishEnergyIncreased[0]+EnergyManager.Instance.fishEnergyIncreased[1]+EnergyManager.Instance.fishEnergyIncreased[2]+EnergyManager.Instance.fishEnergyIncreased[3]+EnergyManager.Instance.fishEnergyIncreased[4]);
            _koiEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.fishEnergyCrop[0] = false;
            }
    }
   /** private void SetTurtleEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.fishEnergyCrop[1] == true)

        {
            _turtleEnergyIncreaseUI.text = "+" + EnergyManager.Instance.fishEnergyIncreased[1];
            _turtleEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.fishEnergyCrop[1] = false;
        }
    }
    private void SetSnailEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.fishEnergyCrop[2] == true)

        {
            _snailEnergyIncreaseUI.text = "+" + EnergyManager.Instance.fishEnergyIncreased[2];
            _snailEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.fishEnergyCrop[2] = false;
        }
    }
    private void SetFrogEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.fishEnergyCrop[3] == true)

        {
            _frogEnergyIncreaseUI.text = "+" + EnergyManager.Instance.fishEnergyIncreased[3];
            _frogEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.fishEnergyCrop[3] = false;
        }
    }
    private void SetDragonflyEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.fishEnergyCrop[4] == true)

        {
            _dragonflyEnergyIncreaseUI.text = "+" + EnergyManager.Instance.fishEnergyIncreased[4];
            _dragonflyEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.fishEnergyCrop[4] = false;
        }
    }
**/

    private void SetBambooEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.plantEnergyCrop[0] == true)

        {
            _bambooEnergyIncreaseUI.text = "+" + EnergyManager.Instance.plantEnergyIncreased[0];
            _bambooEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.plantEnergyCrop[0] = false;
        }
    }
    private void SetMushroomEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.plantEnergyCrop[1] == true)
            
        {
            _mushroomEnergyIncreaseUI.text = "+" + EnergyManager.Instance.plantEnergyIncreased[1];
            _mushroomEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.plantEnergyCrop[1] = false;
        }
    }
    private void SetReedEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.plantEnergyCrop[2] == true)

        {
            _reedEnergyIncreaseUI.text = "+" + EnergyManager.Instance.plantEnergyIncreased[2];
            _reedEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.plantEnergyCrop[2] = false;
        }
    }
    private void SetTreeEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.plantEnergyCrop[3] == true)

        {
            _treeEnergyIncreaseUI.text = "+" + EnergyManager.Instance.plantEnergyIncreased[3];
            _treeEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.plantEnergyCrop[3] = false;
        }
    }
    private void SetSakuraEnergyIncreaseUI()
    {
        if (EnergyManager.Instance.plantEnergyCrop[4] == true)

        {
            _sakuraEnergyIncreaseUI.text = "+" + EnergyManager.Instance.plantEnergyIncreased[4];
            _sakuraEnergyIncreaseUI.gameObject.SetActive(true);
            EnergyManager.Instance.plantEnergyCrop[4] = false;
        }
    }
    private void SetFishEnergyIncreaseUI()
    {
        SetKoiEnergyIncreaseUI();
        /**SetTurtleEnergyIncreaseUI();
        SetFrogEnergyIncreaseUI();
        SetSnailEnergyIncreaseUI();
        SetDragonflyEnergyIncreaseUI();**/
    }

    private void SetPlantEnergyIncreaseUI()
    {
        SetBambooEnergyIncreaseUI();
        SetMushroomEnergyIncreaseUI();
        SetReedEnergyIncreaseUI();
        SetTreeEnergyIncreaseUI();
        SetSakuraEnergyIncreaseUI();

    }

    // 設置所有通用的UI
    private void SetCommonActive(bool active)
    {
        _backHomeButtonGO.SetActive(active);
        _buyListButtonGO.SetActive(active);
        _settingsButtonGO.SetActive(active);
        // _screenShotButtonGO.SetActive (active);
        //_skill1ButtonGO.SetActive (active);
        //_skill2ButtonGO.SetActive (active);
        //_skill3ButtonGO.SetActive (active);
        _zenEnergyText.gameObject.SetActive(active);
        _waterEnergyText.gameObject.SetActive(active);
        _plantEnergyText.gameObject.SetActive(active);
        _fishEnergyText.gameObject.SetActive(active);
        //_stageText.gameObject.SetActive (active);
        _levelGO.SetActive(active);
        _infoButton.SetActive(active);

    }

    private void SetTempleActive(bool active)
    {
        _fishExp.gameObject.SetActive(active);
        _plantExp.gameObject.SetActive(active);
        _fishLevelUpButton.gameObject.SetActive(active);
        _plantLevelUpButton.gameObject.SetActive(active);
        //_zenEnergyText.gameObject.SetActive(active);
        _fishExpText.gameObject.SetActive(active);
        _plantExpText.gameObject.SetActive(active);
        _zenProgressBar.SetActive(active);
        _amitabhaGO.SetActive(active);
    }

    public void MenuButtonFunction()
    {
        if (LevelManager.Instance._allMenuOn == false)
        {
            StartCoroutine(menuOnRoutine());
            LevelManager.Instance._allMenuOn = true;

        }
        else if (LevelManager.Instance._allMenuOn == true)
        {
            StartCoroutine(menuOffRoutine());
            LevelManager.Instance._allMenuOn = false;
        }
    }

    private IEnumerator menuOnRoutine()
    {
        _menuWheelButton.gameObject.SetActive(true);
        _menuPondButton.gameObject.SetActive(true);
        _menuGardenButton.gameObject.SetActive(true);
        _menuTempleButton.gameObject.SetActive(true);
        _menuPondAni.Play("menuPond_On");
        _menuWheelAni.Play("menuWheel_On");
        _menuGardenAni.Play("menuGarden_On");
        _menuTempleAni.Play("menuTemple_On");

        StopCoroutine(menuOnRoutine());
        yield return null;
    }
    private IEnumerator menuOffRoutine()
    {
        _menuPondAni.Play("menuPond_Off");
        _menuWheelAni.Play("menuWheel_Off");
        _menuGardenAni.Play("menuGarden_Off");
        _menuTempleAni.Play("menuTemple_Off");
        yield return new WaitForSeconds(0.1f); // the time need for the menu off animations
        _menuWheelButton.gameObject.SetActive(false);
        _menuPondButton.gameObject.SetActive(false);
        _menuGardenButton.gameObject.SetActive(false);
        _menuTempleButton.gameObject.SetActive(false);
        LevelManager.Instance._allMenuOn = false;
        StopCoroutine(menuOffRoutine());

    }
    #endregion

    #region Main

    private void InMain()
    {
        SetCommonActive(true);
        _backHomeButtonGO.SetActive(false);
        _zenEnergyText.gameObject.SetActive(false);
        _hp.SetActive(false);
        _zenCDText.gameObject.SetActive(false);
        SetTempleActive(false);
        _menuButton.gameObject.SetActive(true);
        _buyListButtonGO.SetActive(false);
        _infoButton.SetActive(false);
        SetMenuButtonUnlock();
        _infoWheelImage.gameObject.SetActive(false);
        _infoPondImage.gameObject.SetActive(false);
        _infoGardenImage.gameObject.SetActive(false);
        _infoTempleImage.gameObject.SetActive(false);

    }

    private void SetupMainUI()
    {
        _gardenBambooUpgradeButton.onClick.AddListener(BambooUpgradeClick);
        _gardenReedUpgradeButton.onClick.AddListener(ReedUpgradeClick);
        _gardenMushroomUpgradeButton.onClick.AddListener(MushroomUpgradeClick);
        _gardenTreeUpgradeButton.onClick.AddListener(TreeUpgradeClick);
        _gardenSakuraUpgradeButton.onClick.AddListener(SakuraUpgradeClick);

        SetBambooUI();
        SetReedUI();
        SetMushroomUI();
        SetSakuraUI();
        SetTreeUI();
    }

    private void SetGardenMask()
    {
        SetBambooMask();
        SetReedMask();
        SetMushroomMask();
        SetTreeMask();
        SetSakuraMask();

        SetKoiMask();
        SetTurtleMask();
        SetFrogMask();
        SetDragonFlyMask();
        SetSnailsMask();
    }

    public void menuTempleButton()
    {
        if (_tut3 == true)
        {
            GameManager.Instance.State = EnumGameState.Temple;
            StartCoroutine(menuOffRoutine());
            StartCoroutine(templeToInsideTempleRoutine());
        }


    }
    public void menuPondButton()
    {
        if (_tut1 == true)
        {
            GameManager.Instance.State = EnumGameState.Pond;
            StartCoroutine(menuOffRoutine());
        }


    }
    public void menuGardenButton()
    {
        if (_tut2 == true)
        {
            GameManager.Instance.State = EnumGameState.Garden;
            StartCoroutine(menuOffRoutine());

        }
    }

    public void menuWheelButton()
    {

        GameManager.Instance.State = EnumGameState.WaterWheel;
        StartCoroutine(menuOffRoutine());
    }



    private IEnumerator templeToInsideTempleRoutine()
    {

        transitAniCanvas.SetActive(true);
        yield return new WaitForSeconds(2f);
        CameraManager.Instance.Smooth = false;
        GameManager.Instance.State = EnumGameState.InsideTemple;
        yield return new WaitForSeconds(1f);
        transitAniCanvas.SetActive(false);
        StopCoroutine(templeToInsideTempleRoutine());
    }

    private void clickBackWhenPlayerDie()
    {

        _restartButton.SetActive(false);
        if (_restartCo != null)
        {
            StopCoroutine(_restartCo);
        }
        PlayerManager.Instance._isPlayerDead = false;
        _runOnceBool = false; // reference to the restartcounter
    }

    public void OnToThePondButtonClick()
    {
        if (_tut1 == true)
        {
            GameManager.Instance.State = EnumGameState.Pond;
            _toThePond.gameObject.SetActive(false);
        }

    }


    #endregion

    #region Garden

    private void InGarden()
    {
        SetCommonActive(true);
        _backHomeButtonGO.SetActive(true);
        _menuButton.gameObject.SetActive(false);
        _itemGarden.gameObject.SetActive(true);
        _itemPond.gameObject.SetActive(false);
        _itemWheel.gameObject.SetActive(false);
        _zenEnergyText.gameObject.SetActive(false);
        _infoWheelImage.gameObject.SetActive(false);
        _infoPondImage.gameObject.SetActive(false);
        _infoGardenImage.gameObject.SetActive(true);
        _infoTempleImage.gameObject.SetActive(false);
        _gardenUICo = SetGardenUIRoutine();
        StartCoroutine(_gardenUICo);



    }

    private void GardenUI(bool active)
    {
        _fromGardenToTemple.gameObject.SetActive(active);
        _fromGardenToPond.gameObject.SetActive(active);
        _fromGardenToWheel.gameObject.SetActive(active);
    }

    private IEnumerator SetGardenUIRoutine()
    {
        yield return new WaitForSeconds(1f);
        GardenUI(true);
    }

    public void OnFromGardenToTempleButtonClick()
    {
        if (_tut3 == true)
        {
            GameManager.Instance.State = EnumGameState.Temple;
            StartCoroutine(templeToInsideTempleRoutine());
            GardenUI(false);
        }
    }
    public void OnFromGardenToPondButtonClick()
    {
        GameManager.Instance.State = EnumGameState.Pond;

        GardenUI(false);
    }

    public void OnFromGardenToWheelButtonClick()
    {
        GameManager.Instance.State = EnumGameState.WaterWheel;

        GardenUI(false);
    }


    #endregion
    #region Bamboo

    private void SetBambooUI()
    {
        SetBambooMask();
        SetBambooItem();
    }

    private void SetBambooMask()
    {
        bool unlock = GameTool.BambooIsUnlock();
        _gardenBambooMask.SetActive(!unlock);
    }

    private void SetBambooItem()
    {
        int bambooLevel = DataManager.BambooLevel;
        FacilityInfo info = GameTool.GetBambooInfo(bambooLevel);
        _gardenBambooLevelText.text = "Lv. " + info.Level.ToString();
        _gardenBambooCurFuncText.text = "Current: " + info.CurFunc + " Plant Energy/min";
        _gardenBambooNextFuncText.text = "Next Level: " + info.NextFunc + " Plant Energy/min";
        _gardenBambooCostText.text = info.NextCost.ToString();
    }

    private void BambooUpgradeClick()
    {
        // Debug.Log("Bamboo Upgrade Click.");

        int bambooLevel = DataManager.BambooLevel;
        int cost = GameTool.GetBambooCost(bambooLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        BambooManager.Instance.UnlockBamboo();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetBambooItem();

        SetBambooUpgradeButtonActive();
    }

    private void SetBambooUpgradeButtonActive()
    {
        int bambooLevel = BambooManager.Instance.BambooLevel;
        int cost = GameTool.GetBambooCost(bambooLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && bambooLevel < Config.BambooLevelLimit)
        {
            _gardenBambooUpgradeButton.interactable = true;
        }
        else
        {
            _gardenBambooUpgradeButton.interactable = false;
        }

    }

    #endregion

    #region Reed

    private void SetReedUI()
    {
        SetReedMask();
        SetReedItem();
    }
    private void SetReedMask()
    {
        bool unlock = GameTool.ReedIsUnlock();
        _gardenReedMask.SetActive(!unlock);
    }

    private void SetReedItem()
    {
        int reedLevel = DataManager.ReedLevel;
        FacilityInfo info = GameTool.GetReedInfo(reedLevel);
        _gardenReedLevelText.text = "Lv. " + info.Level.ToString();
        _gardenReedCurFuncText.text = "Current: " + info.CurFunc + " Plant Energy/min";
        _gardenReedNextFuncText.text = "Next Level: " + info.NextFunc + " Plant Energy/min";
        _gardenReedCostText.text = info.NextCost.ToString();
    }

    private void ReedUpgradeClick()
    {
        // Debug.Log("Reed Upgrade Click.");

        int reedLevel = DataManager.ReedLevel;
        int cost = GameTool.GetReedCost(reedLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        ReedManager.Instance.UnlockReed();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetReedItem();

        SetReedUpgradeButtonActive();
    }

    private void SetReedUpgradeButtonActive()
    {
        int reedLevel = ReedManager.Instance.ReedLevel;
        int cost = GameTool.GetReedCost(reedLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && reedLevel < Config.ReedLevelLimit)
        {
            _gardenReedUpgradeButton.interactable = true;
        }
        else
        {
            _gardenReedUpgradeButton.interactable = false;
        }

    }

    #endregion

    #region Tree

    private void SetTreeUI()
    {
        SetTreeMask();
        SetTreeItem();
    }
    private void SetTreeMask()
    {
        bool unlock = GameTool.TreeIsUnlock();
        _gardenTreeMask.SetActive(!unlock);
    }

    private void SetTreeItem()
    {
        int TreeLevel = DataManager.TreeLevel;
        FacilityInfo info = GameTool.GetTreeInfo(TreeLevel);
        _gardenTreeLevelText.text = "Lv. " + info.Level.ToString();
        _gardenTreeCurFuncText.text = "Current: " + info.CurFunc + " Plant Energy/min";
        _gardenTreeNextFuncText.text = "Next Level: " + info.NextFunc + " Plant Energy/min";
        _gardenTreeCostText.text = info.NextCost.ToString();
    }

    private void TreeUpgradeClick()
    {
        // Debug.Log("Tree Upgrade Click.");

        int TreeLevel = DataManager.TreeLevel;
        // Debug.Log("TreeLevel: " + TreeLevel);
        int cost = GameTool.GetTreeCost(TreeLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        TreeManager.Instance.UnlockTree();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetTreeItem();

        SetTreeUpgradeButtonActive();
    }

    private void SetTreeUpgradeButtonActive()
    {
        int TreeLevel = TreeManager.Instance.TreeLevel;

        if (TreeLevel >= Config.TreeLevelLimit)
        {
            _gardenTreeUpgradeButton.interactable = false;
            return;
        }

        int cost = GameTool.GetTreeCost(TreeLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && TreeLevel < Config.TreeLevelLimit)
        {
            _gardenTreeUpgradeButton.interactable = true;
        }
        else
        {
            _gardenTreeUpgradeButton.interactable = false;
        }

    }

    #endregion

    #region Mushroom

    private void SetMushroomUI()
    {
        SetMushroomMask();
        SetMushroomItem();
    }
    private void SetMushroomMask()
    {
        bool unlock = GameTool.MushroomIsUnlock();
        _gardenMushroomMask.SetActive(!unlock);
    }

    private void SetMushroomItem()
    {
        int MushroomLevel = DataManager.MushroomLevel;
        FacilityInfo info = GameTool.GetMushroomInfo(MushroomLevel);
        _gardenMushroomLevelText.text = "Lv. " + info.Level.ToString();
        _gardenMushroomCurFuncText.text = "Current: " + info.CurFunc + " Plant Energy/min";
        _gardenMushroomNextFuncText.text = "Next Level: " + info.NextFunc + " Plant Energy/min";
        _gardenMushroomCostText.text = info.NextCost.ToString();
    }

    private void MushroomUpgradeClick()
    {
        // Debug.Log("Mushroom Upgrade Click.");

        int MushroomLevel = DataManager.MushroomLevel;
        int cost = GameTool.GetMushroomCost(MushroomLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        MushroomManager.Instance.UnlockMushroom();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetMushroomItem();

        SetMushroomUpgradeButtonActive();
    }

    private void SetMushroomUpgradeButtonActive()
    {
        int MushroomLevel = MushroomManager.Instance.MushroomLevel;
        int cost = GameTool.GetMushroomCost(MushroomLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && MushroomLevel < Config.MushroomLevelLimit)
        {
            _gardenMushroomUpgradeButton.interactable = true;
        }
        else
        {
            _gardenMushroomUpgradeButton.interactable = false;
        }

    }

    #endregion

    #region Sakura

    private void SetSakuraUI()
    {
        SetSakuraMask();
        SetSakuraItem();
    }

    private void SetSakuraMask()
    {
        bool unlock = GameTool.SakuraIsUnlock();
        _gardenSakuraMask.SetActive(!unlock);
    }

    private void SetSakuraItem()
    {
        int SakuraLevel = DataManager.SakuraLevel;
        FacilityInfo info = GameTool.GetSakuraInfo(SakuraLevel);
        _gardenSakuraLevelText.text = "Lv. " + info.Level.ToString();
        _gardenSakuraCurFuncText.text = "Current: " + info.CurFunc + " Plant Energy/min";
        _gardenSakuraNextFuncText.text = "Next Level: " + info.NextFunc + " Plant Energy/min";
        _gardenSakuraCostText.text = info.NextCost.ToString();
    }

    private void SakuraUpgradeClick()
    {
        // Debug.Log("Sakura Upgrade Click.");

        int SakuraLevel = DataManager.SakuraLevel;
        // Debug.Log("SakuraLevel: " + SakuraLevel);
        int cost = GameTool.GetSakuraCost(SakuraLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        SakuraManager.Instance.UnlockSakura();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetSakuraItem();

        SetSakuraUpgradeButtonActive();
    }

    private void SetSakuraUpgradeButtonActive()
    {
        int SakuraLevel = SakuraManager.Instance.SakuraLevel;

        if (SakuraLevel >= Config.SakuraLevelLimit)
        {
            _gardenSakuraUpgradeButton.interactable = false;
            return;
        }

        int cost = GameTool.GetSakuraCost(SakuraLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && SakuraLevel < Config.SakuraLevelLimit)
        {
            _gardenSakuraUpgradeButton.interactable = true;
        }
        else
        {
            _gardenSakuraUpgradeButton.interactable = false;
        }

    }

    #endregion


    #region Watch

    private void InWatch()
    {
        SetCommonActive(false);
        _menuButton.gameObject.SetActive(false);
        _menuWheelButton.gameObject.SetActive(false);
        _menuPondButton.gameObject.SetActive(false);
        _menuGardenButton.gameObject.SetActive(false);
        _menuTempleButton.gameObject.SetActive(false);

        _fromGardenToPond.gameObject.SetActive(false);
        _fromGardenToTemple.gameObject.SetActive(false);
        _fromGardenToWheel.gameObject.SetActive(false);
        if (_gardenUICo != null)
        {
            StopCoroutine(_gardenUICo);
        }

        _infoWheelImage.gameObject.SetActive(false);
        _infoPondImage.gameObject.SetActive(false);
        _infoGardenImage.gameObject.SetActive(false);
        _infoTempleImage.gameObject.SetActive(false);
        SetPondUI(false);
        _gardenCollect.gameObject.SetActive(false);
        _afterFishCollect.gameObject.SetActive(false);
        _afterFishCollect.gameObject.SetActive(false);



    }

    #endregion

    #region Water Wheel

    private void InWaterWheel()
    {
        SetCommonActive(true);
        _zenEnergyText.gameObject.SetActive(false);
        _menuButton.gameObject.SetActive(false);
        _itemGarden.gameObject.SetActive(false);
        _itemPond.gameObject.SetActive(false);
        _itemWheel.gameObject.SetActive(true);
        _infoWheelImage.gameObject.SetActive(true);
        _infoPondImage.gameObject.SetActive(false);
        _infoGardenImage.gameObject.SetActive(false);
        _infoTempleImage.gameObject.SetActive(false);
        _toThePond.gameObject.SetActive(true);
        SetWheelTutorialText();


    }

 

    private void SetupWaterWheelUI()
    {
        _waterWheelUpgradeCapacityButton.onClick.AddListener(WaterWheelCapacityUpgradeClick);
        _waterWheelUpgradeEnergyButton.onClick.AddListener(WaterWheelEnergyUpgradeClick);

        SetWaterWheelCapacityUI();
        SetWaterWheelEnergyUI();
    }

    private void SetWaterWheelCapacityUI()
    {
        int capacityLevel = DataManager.WaterWheelCapacityLevel;
        FacilityInfo info = GameTool.GetWaterWheelCapacityInfo(capacityLevel);
        _waterWheelCapacityLevelText.text = "Lv. " + info.Level.ToString();
        _waterWheelCapacityCurFuncText.text = "Current: " + info.CurFunc + " max capacity";
        _waterWheelCapacityNextFuncText.text = "Next Level: " + info.NextFunc + "  max capacity";
        _waterWheelCapacityCostText.text = info.NextCost.ToString();
    }

    private void SetWaterWheelEnergyUI()
    {
        int energyLevel = DataManager.WaterWheelEnergyLevel;
        FacilityInfo info = GameTool.GetWaterWheelWaterEnergyInfo(energyLevel);
        _waterWheelEnergyLevelText.text = "Lv. " + info.Level.ToString();
        _waterWheelEnergyCurFuncText.text = "Current: " + info.CurFunc + "  water/second";
        _waterWheelEnergyNextFuncText.text = "Next Level: " + info.NextFunc + "  water/second";
        _waterWheelEnergyCostText.text = info.NextCost.ToString();
    }

    private void WaterWheelCapacityUpgradeClick()
    {
        int capacityLevel = DataManager.WaterWheelCapacityLevel;
        int cost = GameTool.GetWaterWheelCapacityCost(capacityLevel);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        WaterWheel.Instance.UpgradeCapacity();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetWaterWheelCapacityUI();
    }

    private void WaterWheelEnergyUpgradeClick()
    {
        int energyLevel = DataManager.WaterWheelEnergyLevel;
        int cost = GameTool.GetWaterWheelWaterEnergyCost(energyLevel);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        WaterWheel.Instance.UpgradeEnergy();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetWaterWheelEnergyUI();
    }

    private void OnWaterWheelPowerChanged(float power, int capacity)
    {

    }

    private void SetWaterWheelCapacityUpgradeButtonActive()
    {
        int curEnergy = (int)EnergyManager.Instance.WaterEnergy;
        int cost = GameTool.GetWaterWheelCapacityCost(WaterWheel.Instance.CapacityLevel);
        // Debug.Log("WaterWheel capacity upgrade cost: " + cost);
        _waterWheelUpgradeCapacityButton.interactable = GameTool.IsEnoughWaterEnergy(cost);
    }

    private void SetWaterWheelEnergyUpgradeButtonActive()
    {
        int curEnergy = (int)EnergyManager.Instance.WaterEnergy;
        int cost = GameTool.GetWaterWheelWaterEnergyCost(WaterWheel.Instance.EnergyLevel);
        // Debug.Log("WaterWheel water energy upgrade cost: " + cost);
        _waterWheelUpgradeEnergyButton.interactable = GameTool.IsEnoughWaterEnergy(cost);
    }

    #endregion

    #region Pond

    private void InPond()
    {
        SetCommonActive(true);
        _zenEnergyText.gameObject.SetActive(false);
        _menuButton.gameObject.SetActive(false);
        _itemGarden.gameObject.SetActive(false);
        _itemPond.gameObject.SetActive(true);
        _itemWheel.gameObject.SetActive(false);
        _infoWheelImage.gameObject.SetActive(false);
        _infoPondImage.gameObject.SetActive(true);
        _infoGardenImage.gameObject.SetActive(false);
        _infoTempleImage.gameObject.SetActive(false);
        SetPondUI(true);
       

    }

    // 初始化Pond的UI
    private void SetupPondUI()
    {
        // add button events
        _pondKoiUpgradeButton.onClick.AddListener(KoiUpgradeClick);
        _pondTurtleUpgradeButton.onClick.AddListener(TurtleUpgradeClick);
        _pondSnailsUpgradeButton.onClick.AddListener(SnailsUpgradeClick);
        _pondFrogUpgradeButton.onClick.AddListener(FrogUpgradeClick);
        _pondDragonFlyUpgradeButton.onClick.AddListener(DragonFlyUpgradeClick);

        // set all ui
        SetKoiUI();
        SetTurtleUI();
        SetSnailsUI();
        SetFrogUI();
        SetDragonFlyUI();
    }

    private void SetPondUI(bool active)
    {
        _fromPondToGarden.gameObject.SetActive(active);
        _fromPondToWheel.gameObject.SetActive(active);
    }
    public void OnFromPondToWheelClick()
    {
        GameManager.Instance.State = EnumGameState.WaterWheel;
        SetPondUI(false);
    }

    public void OnFromPondToGardenClick()
    {
        if (_tut2 == true)
        {
            GameManager.Instance.State = EnumGameState.Garden;
            _fromPondToGarden.gameObject.SetActive(false);
            SetPondUI(false);
        }
    }


    #region Koi

    private void SetKoiUI()
    {
        SetKoiMask();
        SetKoiItem();
    }

    private void SetKoiUpgradeButtonActive()
    {
        int koiLevel = Pond.Instance.GetPondLevel(EnumPondType.Koi);
        int cost = GameTool.GetKoiCost(koiLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && koiLevel < Config.KoiLevelLimit)
        {
            _pondKoiUpgradeButton.interactable = true;
        }
        else
        {
            _pondKoiUpgradeButton.interactable = false;
        }

    }

    private void SetKoiMask()
    {
        bool unlock = GameTool.KoiIsUnlock();
        _pondKoiMask.SetActive(!unlock);
    }

    private void SetKoiItem()
    {
        int koiLevel = DataManager.KoiLevel;
        FacilityInfo info = GameTool.GetKoiInfo(koiLevel);
        _pondKoiLevelText.text = "Lv. " + info.Level.ToString();
        _pondKoiCurFuncText.text = "Current: " + info.CurFunc + " Fish Power/min";
        _pondKoiNextFuncText.text = "Next Level: " + info.NextFunc + "  Fish Power/min";
        _pondKoiCostText.text = info.NextCost.ToString();
    }

    private void KoiUpgradeClick()
    {
        // Debug.Log("Koi Upgrade Click.");

        int koiLevel = DataManager.KoiLevel;
        // Debug.Log("KoiLevel: " + koiLevel);
        int cost = GameTool.GetKoiCost(koiLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        Pond.Instance.UnlockKoi();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetKoiItem();

        SetKoiUpgradeButtonActive();
    }

    #endregion

    #region Turtle

    private void SetTurtleUI()
    {
        SetTurtleMask();
        SetTurtleItem();
    }

    private void SetTurtleUpgradeButtonActive()
    {
        int TurtleLevel = Pond.Instance.GetPondLevel(EnumPondType.Turtle);
        int cost = GameTool.GetTurtleCost(TurtleLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && TurtleLevel < Config.TurtleLevelLimit)
        {
            _pondTurtleUpgradeButton.interactable = true;
        }
        else
        {
            _pondTurtleUpgradeButton.interactable = false;
        }

    }

    private void SetTurtleMask()
    {
        bool unlock = GameTool.TurtleIsUnlock();
        _pondTurtleMask.SetActive(!unlock);
    }

    private void SetTurtleItem()
    {
        int TurtleLevel = DataManager.TurtleLevel;
        FacilityInfo info = GameTool.GetTurtleInfo(TurtleLevel);
        _pondTurtleLevelText.text = "Lv. " + info.Level.ToString();
        _pondTurtleCurFuncText.text = "Current: " + info.CurFunc + " Fish Power/min";
        _pondTurtleNextFuncText.text = "Next Level: " + info.NextFunc + " Fish Power/min";
        _pondTurtleCostText.text = info.NextCost.ToString();
    }

    private void TurtleUpgradeClick()
    {
        // Debug.Log("Turtle Upgrade Click.");

        int TurtleLevel = DataManager.TurtleLevel;
        // Debug.Log("TurtleLevel: " + TurtleLevel);
        int cost = GameTool.GetTurtleCost(TurtleLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        Pond.Instance.UnlockTurtle();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetTurtleItem();

        SetTurtleUpgradeButtonActive();
    }

    #endregion

    #region Snails

    private void SetSnailsUI()
    {
        SetSnailsMask();
        SetSnailsItem();
    }

    private void SetSnailsUpgradeButtonActive()
    {
        int snailsLevel = Pond.Instance.GetPondLevel(EnumPondType.Snails);
        int cost = GameTool.GetSnailsCost(snailsLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && snailsLevel < Config.SnailsLevelLimit)
        {
            _pondSnailsUpgradeButton.interactable = true;
        }
        else
        {
            _pondSnailsUpgradeButton.interactable = false;
        }

    }

    private void SetSnailsMask()
    {
        bool unlock = GameTool.SnailsIsUnlock();
        _pondSnailsMask.SetActive(!unlock);
    }

    private void SetSnailsItem()
    {
        int snailsLevel = DataManager.SnailsLevel;
        FacilityInfo info = GameTool.GetSnailsInfo(snailsLevel);
        _pondSnailsLevelText.text = "Lv. " + info.Level.ToString();
        _pondSnailsCurFuncText.text = "Current: " + info.CurFunc + " Fish Power/min";
        _pondSnailsNextFuncText.text = "Next Level: " + info.NextFunc + " Fish Power/min";
        _pondSnailsCostText.text = info.NextCost.ToString();
    }

    private void SnailsUpgradeClick()
    {
        // Debug.Log("Snails Upgrade Click.");

        int SnailsLevel = DataManager.SnailsLevel;
        // Debug.Log("SnailsLevel: " + SnailsLevel);
        int cost = GameTool.GetSnailsCost(SnailsLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        Pond.Instance.UnlockSnails();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetSnailsItem();

        SetSnailsUpgradeButtonActive();
    }

    #endregion

    #region Frog

    private void SetFrogUI()
    {
        SetFrogMask();
        SetFrogItem();
    }

    private void SetFrogUpgradeButtonActive()
    {
        int frogLevel = Pond.Instance.GetPondLevel(EnumPondType.Frog);
        int cost = GameTool.GetFrogCost(frogLevel);

        if (GameTool.IsEnoughWaterEnergy(cost) && frogLevel < Config.FrogLevelLimit)
        {
            _pondFrogUpgradeButton.interactable = true;
        }
        else
        {
            _pondFrogUpgradeButton.interactable = false;
        }

    }

    private void SetFrogMask()
    {
        bool unlock = GameTool.FrogIsUnlock();
        _pondFrogMask.SetActive(!unlock);
    }

    private void SetFrogItem()
    {
        int frogLevel = DataManager.FrogLevel;
        FacilityInfo info = GameTool.GetFrogInfo(frogLevel);
        _pondFrogLevelText.text = "Lv. " + info.Level.ToString();
        _pondFrogCurFuncText.text = "Current: " + info.CurFunc + " Fish Power/min";
        _pondFrogNextFuncText.text = "Next Level: MAX";
        //_pondFrogNextFuncText.text = "Next Level: " + info.NextFunc + " Fish Power/min";
        _pondFrogCostText.text = info.NextCost.ToString();
    }

    private void FrogUpgradeClick()
    {
        // Debug.Log("Frog Upgrade Click.");

        int FrogLevel = DataManager.FrogLevel;
        // Debug.Log("FrogLevel: " + FrogLevel);
        int cost = GameTool.GetFrogCost(FrogLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        Pond.Instance.UnlockFrog();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetFrogItem();

        SetFrogUpgradeButtonActive();
    }

    #endregion

    #region DragonFly

    private void SetDragonFlyUI()
    {
        SetDragonFlyMask();
        SetDragonFlyItem();
    }

    private void SetDragonFlyUpgradeButtonActive()
    {
        int dragonFlyLevel = Pond.Instance.GetPondLevel(EnumPondType.DragonFly);

        if (dragonFlyLevel >= Config.DragonFlyLevelLimit)
        {
            _pondDragonFlyUpgradeButton.interactable = false;
            return;
        }

        int cost = GameTool.GetDragonFlyCost(dragonFlyLevel);
        if (GameTool.IsEnoughWaterEnergy(cost) && dragonFlyLevel < Config.DragonFlyLevelLimit)
        {
            _pondDragonFlyUpgradeButton.interactable = true;
        }
        else
        {
            _pondDragonFlyUpgradeButton.interactable = false;
        }

    }

    private void SetDragonFlyMask()
    {
        bool unlock = GameTool.DragonFlyIsUnlock();
        _pondDragonFlyMask.SetActive(!unlock);
    }

    private void SetDragonFlyItem()
    {
        int dragonFlyLevel = DataManager.DragonFlyLevel;
        FacilityInfo info = GameTool.GetDragonFlyInfo(dragonFlyLevel);
        _pondDragonFlyLevelText.text = "Lv. " + info.Level.ToString();
        _pondDragonFlyCurFuncText.text = "Current: " + info.CurFunc + " Fish Power/min";
        _pondDragonFlyNextFuncText.text = "Next Level: " + info.NextFunc + " Fish Power/min";
        _pondDragonFlyCostText.text = info.NextCost.ToString();
    }

    private void DragonFlyUpgradeClick()
    {
        // Debug.Log("DragonFly Upgrade Click.");

        int DragonFlyLevel = DataManager.DragonFlyLevel;
        // Debug.Log("DragonFlyLevel: " + DragonFlyLevel);
        int cost = GameTool.GetDragonFlyCost(DragonFlyLevel);
        // Debug.Log(cost);

        if (!GameTool.IsEnoughWaterEnergy(cost))
            return;

        Pond.Instance.UnlockDragonFly();
        EnergyManager.Instance.WaterEnergy -= cost;

        SetDragonFlyItem();

        SetDragonFlyUpgradeButtonActive();
    }

    #endregion

    #endregion

    #region Temple

    private void InsideTemple()
    {
        
        SetCommonActive(false);
        SetTempleActive(true);
        _backHomeButtonGO.SetActive(true);
        _settingsButtonGO.SetActive(true);
        _levelGO.SetActive(true);
        _waterEnergyText.gameObject.SetActive(true);
        _fishEnergyText.gameObject.SetActive(true);
        _plantEnergyText.gameObject.SetActive(true);
        _zenProgressBar.SetActive(false);
        _menuButton.gameObject.SetActive(false);
        _infoButton.SetActive(true);
        _infoWheelImage.gameObject.SetActive(false);
        _infoPondImage.gameObject.SetActive(false);
        _infoGardenImage.gameObject.SetActive(false);
        _infoTempleImage.gameObject.SetActive(true);

    }
    private void SetExpImage()
    {
        _fishExpImage.fillAmount = EnergyManager.Instance.FishEnergy / PlayerManager.Instance.FishEnergyReq;
        _plantExpImage.fillAmount = EnergyManager.Instance.PlantEnergy / PlayerManager.Instance.PlantEnergyReq;
    }
    private void SetExpText()
    {
        _fishExpText.text = "Focus Level:" + DataManager.FocusLevel;
        _plantExpText.text = "Savvy Level:" + DataManager.SavvyLevel;

    }
    private void SetPlayerStat()
    {
        _hpText.text = "" + PlayerManager.Instance.PlayerHP;

    }

    private void SetZenCD()
    {
        var zenCD = Mathf.Round((1 / PlayerManager.Instance.ZenPowerSpawnCD) * 100) / 100;
        _zenCDText.text = zenCD + " Zen/ second";
    }

    private void SetProgress()
    {
        _zenProgressBarImage.fillAmount = EnergyManager.Instance.ZenPower / LevelManager.Instance.passScore;
    }

    private void SetFocusReqText()
    {
        _fishReqText.text = DataManager.FishEnergy + " / " + (int)PlayerManager.Instance.FishEnergyReq;
    }

    private void SetSavvyReqText()
    {
        _plantReqText.text = DataManager.PlantEnergy + " / " + (int)PlayerManager.Instance.PlantEnergyReq;
    }

    public void OnRestartButtonClick()
    {
        LevelManager.Instance._shouldReset = true;
        GameManager.Instance.State = EnumGameState.InsideTemple;
        PlayerManager.Instance.PlayerHP = (5 + DataManager.FocusLevel); // Without this, the restart button will keeps come up because playerHp =0
        _runOnceBool = false;
        _restartButton.SetActive(false);

    }

    private void SetRestartButton()
    {
        if (PlayerManager.Instance.PlayerHP <= 0 && _runOnceBool == false)
        {
            Debug.Log("SetrestartButton");
            _restartCo = countRestartButton();
            StartCoroutine(_restartCo);
            _runOnceBool = true;
        }
    }
    private IEnumerator countRestartButton()
    {

        yield return new WaitForSeconds(6f); // wait for player die animation
        _restartButton.SetActive(true);
        PlayerManager.Instance._isPlayerDead = false;
        Debug.Log("routineRun");
        if (_restartCo != null)
        {
            StopCoroutine(_restartCo);
        }

    }

    private void SetHpImage()
    {
        _hpImage.fillAmount = PlayerManager.Instance.PlayerHP / (5 + DataManager.FocusLevel);
    }

    #endregion

    #region During Zen Game
    private void DuringZenGame()
    {
        SetCommonActive(false);
        SetTempleActive(false);
        _zenEnergyText.gameObject.SetActive(true);
        _amitabhaGO.SetActive(true);
        _zenProgressBar.SetActive(true);
        _hp.SetActive(true);
        _zenCDText.gameObject.SetActive(true);
        _backHomeButtonGO.gameObject.SetActive(true);
        _settingsButtonGO.gameObject.SetActive(true);
        _levelGO.SetActive(true);
        _infoWheelImage.gameObject.SetActive(false);
        _infoPondImage.gameObject.SetActive(false);
        _infoGardenImage.gameObject.SetActive(false);
        _infoTempleImage.gameObject.SetActive(false);

    }
    #endregion 
    #region Button Events

    public void OnBackHomeClick()
    {

        switch (GameManager.Instance.State)
        {
            case EnumGameState.Garden:
                CameraManager.Instance.Smooth = true;
                GameManager.Instance.State = EnumGameState.Main;
                GardenUI(false);
                SetPondUI(false);
                StartCoroutine(menuOnRoutine());
                if (_gardenUICo != null)
                {
                    StopCoroutine(_gardenUICo);
                }
                break;
            case EnumGameState.WaterWheel:
                CameraManager.Instance.Smooth = true;
                GameManager.Instance.State = EnumGameState.Main;
                _toThePond.gameObject.SetActive(false);
                StartCoroutine(menuOnRoutine());
                SetWheelTutorialText();
                break;
            case EnumGameState.Pond:
                CameraManager.Instance.Smooth = true;
                GameManager.Instance.State = EnumGameState.Main;
                _toTheGarden.gameObject.SetActive(false);
                StartCoroutine(menuOnRoutine());
                SetPondUI(false);
                break;
            case EnumGameState.InsideTemple:
                CameraManager.Instance.Smooth = false;
                StartCoroutine(countForTransAni());// GameManager.Instance.State = EnumGameState.Main; **Included in the routine
                break;
            case EnumGameState.StartZenPractice:
                CameraManager.Instance.Smooth = false;
                StartCoroutine(countForTransAni());// GameManager.Instance.State = EnumGameState.Main; **Included in the routine
                clickBackWhenPlayerDie();

                if (_templeTutorialTextCo !=null)
                {
                    StopCoroutine(_templeTutorialTextCo);
                }
                if (_templeTextCo != null)
                {
                    StopCoroutine(_templeTextCo);
                }
                clearOffTextGO();
                LevelManager.Instance._shouldReset = true;// prevent the restart button from showing when click on go back button.
                break;
        }

    }

    public void OnMenuButtonClick()
    {

        switch (GameManager.Instance.State)
        {
            case EnumGameState.Garden:

                if (_gardenItemPanelActive == true)
                {
                    _gardenItemPanel.GetComponent<Animator>().SetTrigger("Hide");
                    _gardenItemPanelActive = false;
                }
                else
                {
                    _gardenItemPanelActive = true;
                    _gardenItemPanel.SetActive(false);
                    _gardenItemPanel.SetActive(true);
                }
                break;

            case EnumGameState.WaterWheel:
                if (_waterWheelItemPanelActive)
                {
                    _waterWheelItemPanel.GetComponent<Animator>().SetTrigger("Hide");
                    _waterWheelItemPanelActive = false;
                }
                else
                {
                    _waterWheelItemPanel.SetActive(false);
                    _waterWheelItemPanel.SetActive(true);
                    _waterWheelItemPanelActive = true;
                }
                break;
            case EnumGameState.Pond:
                if (_pondItemPanelActive)
                {
                    _pondItemPanel.GetComponent<Animator>().SetTrigger("Hide");
                    _pondItemPanelActive = false;
                }
                else
                {
                    _pondItemPanel.SetActive(false);
                    _pondItemPanel.SetActive(true);
                    _pondItemPanelActive = true;
                }
                break;
        }

    }

    public void OnExitItemPanelClick()
    {

        switch (GameManager.Instance.State)
        {
            case EnumGameState.Garden:
                _gardenItemPanel.GetComponent<Animator>().SetTrigger("Hide");
                break;
            case EnumGameState.WaterWheel:
                _waterWheelItemPanel.GetComponent<Animator>().SetTrigger("Hide");
                break;
            case EnumGameState.Pond:
                _pondItemPanel.GetComponent<Animator>().SetTrigger("Hide");
                break;
        }

    }


    public void OnFishExpButtonClick()
    {
        PlayerManager.Instance.fishLevelUp();
        PlayerManager.Instance.FocusReq();
        SetFocusReqText();
        SetExpText();
        SetExpImage();

    }

    public void OnPlantExpButtonClick()
    {
        PlayerManager.Instance.plantLevelUp();
        PlayerManager.Instance.SavvyReq();
        SetSavvyReqText();
        SetExpText();
        SetExpImage();
    }

    #endregion

    #region Events

    // 當能量改變時
    private void OnEnergyDataChanged()
    {
        SetFishEnergyIncreaseUI();
        SetPlantEnergyIncreaseUI();
        SetEnergyText();
        SetZenEnergyText();
        SetExpImage();
        SetZenCD();
        SetProgress();
        TutorialCheck();
        SetMenuButtonUnlock();

    }


    // 當游戲狀態改變時
    private void OnGameStateChanged(EnumGameState oldState, EnumGameState newState)
    {
        HideItemPanels();
        SetWheelTutorialText();
        SetAfterFishCollect();
        SetAfterGardenCollectText();
        SetGardenCollectText();
        SetClickStatueText();
        switch (newState)
        {
            case EnumGameState.Main:
                InMain();
                LevelManager.Instance._allMenuOn = false; // false = turn menu on
                MenuButtonFunction();   // automatically tap once when back to main menu
                SetEnergyText();
                break;
            case EnumGameState.Garden:
                InGarden();
                OnMenuButtonClick();
                break;
            case EnumGameState.WaterWheel:
                InWaterWheel();
                OnMenuButtonClick();
                break;
            case EnumGameState.Pond:
                InPond();
                OnMenuButtonClick();
                break;
            case EnumGameState.InsideTemple:
                PlayerManager.Instance.FocusReq();
                PlayerManager.Instance.SavvyReq();
                InsideTemple();
                SetExpText();
                SetExpImage();
                SetSavvyReqText();
                SetFocusReqText();
                if (LevelManager.Instance.Stage == 1)
                {
                    infoClickFunction();
                }

                break;
            case EnumGameState.StartZenPractice:
                SetPlayerStat();
                DuringZenGame();
                SetZenEnergyText();
                SetProgress();
                SetTempleTutorialText();
                SetTempleIntroText();
                break;

            case EnumGameState.Watch:
                InWatch();
                break;
        }

    }

    private void OnHPChange(float _playerHP)
    {
        SetPlayerStat();
        SetHpImage();
        SetRestartButton();
    }


    // 當關卡等級改變時
    private void OnStageLevelChanged(int stage)
    {
        SetStageText(stage);
        SetGardenMask();
    }

    #endregion

    private IEnumerator countForTransAni()
    {
        transitionAni.SetActive(true);

        // countdown , sychronize the camera move and the tranistion animation
        yield return new WaitForSeconds(2f);
        GameManager.Instance.State = EnumGameState.Main;
        StopCoroutine("countForTransAni");
        yield return new WaitForSeconds(1f);
        transitionAni.SetActive(false);
        CameraManager.Instance.Smooth = true;
        LevelManager.Instance._shouldReset = true;
        StartCoroutine(menuOnRoutine());
    }

    #region info
    public void infoClickFunction()
    {
        if (_isInfoOn == false)
        {
            if (GameManager.Instance.State == EnumGameState.Garden)
            {
                _gardenInfo.gameObject.SetActive(true);
            }
            if (GameManager.Instance.State == EnumGameState.WaterWheel)
            {
                _waterWheelInfo.gameObject.SetActive(true);
            }
            if (GameManager.Instance.State == EnumGameState.Pond)
            {
                _fishPondInfo.gameObject.SetActive(true);
            }
            if (GameManager.Instance.State == EnumGameState.InsideTemple)
            {
                _templeInfo.gameObject.SetActive(true);
            }

            _isInfoOn = true;
        }
        else if (_isInfoOn == true)
        {
            _gardenInfo.gameObject.SetActive(false);
            _waterWheelInfo.gameObject.SetActive(false);
            _fishPondInfo.gameObject.SetActive(false);
            _templeInfo.gameObject.SetActive(false);
            _isInfoOn = false;
        }
    }

    public void OnInfoOkButtonClick()
    {
        _gardenInfo.gameObject.SetActive(false);
        _waterWheelInfo.gameObject.SetActive(false);
        _fishPondInfo.gameObject.SetActive(false);
        _templeInfo.gameObject.SetActive(false);
        _isInfoOn = false;
    }
    #endregion


    #region settings Button

    public void OnSettingButtonClick()
    {
        if (_isOptionOn == false)
        {

            StartCoroutine(OptionOnRoutine());
            // optional On Ani
            _isOptionOn = true;

        }
        else if (_isOptionOn == true)
        {
            StartCoroutine(OptionOffRoutine());
            //option Off An
            _isOptionOn = false;
        }
    }

    private IEnumerator OptionOnRoutine()
    {
        _optionPanel.SetActive(true);
        _optionPanelAni.Play("optionPanelOn");
        StopCoroutine(OptionOnRoutine());
        yield return null;
    }

    private IEnumerator OptionOffRoutine()
    {
        _optionPanelAni.Play("optionPanelOff");
        yield return new WaitForSeconds(0.5f); // for the close animation
        _optionPanel.SetActive(false);
        StopCoroutine(OptionOffRoutine());
    }

    public void ExitButton()
    {
        Application.Quit();
        Debug.Log("Quit Game!");
    }


    public void OnMusicButtonClick()
    {
        // off the current track
        if (AudioManager.Instance._isBGM1On == true) //playing BGM 1
        {
            AudioManager.Instance.Mute("BGM1");
            AudioManager.Instance.Play("BGM2");
            AudioManager.Instance.Mute("BGM_Muted");


            AudioManager.Instance._isBGM2On = true;
            AudioManager.Instance._isBGM1On = false;
            AudioManager.Instance._isEmptyOn = false;
        }
        else if (AudioManager.Instance._isBGM2On == true) //playing BGM 2
        {
            AudioManager.Instance.Mute("BGM1");
            AudioManager.Instance.Mute("BGM2");
            AudioManager.Instance.Play("BGM_Muted");

            AudioManager.Instance._isBGM1On = false;
            AudioManager.Instance._isBGM2On = false;
            AudioManager.Instance._isEmptyOn = true;
        }
        else if (AudioManager.Instance._isEmptyOn == true) //Muted
        {
            AudioManager.Instance.Play("BGM1");
            AudioManager.Instance.Mute("BGM2");
            AudioManager.Instance.Mute("BGM_Muted");
            AudioManager.Instance._isBGM1On = true;
            AudioManager.Instance._isBGM2On = false;
            AudioManager.Instance._isEmptyOn = false;
        }

        SetMusicText();
        AudioManager.Instance.muteCheck();
    }

    private void SetMusicText()
    {
        if (AudioManager.Instance._isBGM1On == true)
        {
            _musicText.text = "Zen";
        }
        else if (AudioManager.Instance._isBGM2On == true)
        {
            _musicText.text = "Meditation";
        }
        else if (AudioManager.Instance._isEmptyOn == true)
        {
            _musicText.text = "Muted";
        }
    }
    #endregion
    #region Tutorial

    private void SetWheelTutorialText()
    {
        if (_tut1 == false && GameManager.Instance.State == EnumGameState.WaterWheel)
        {
            _wheelTutorial.SetActive(true);
        }
        else if (_tut1 == true || GameManager.Instance.State != EnumGameState.WaterWheel)
        {
            _wheelTutorial.SetActive(false);
        }
    }


    private void TutorialCheck()
    {
        if (DataManager.WaterWheelEnergyLevel >= 2 && DataManager.WaterWheelCapacityLevel >= 2)
        {
            {
                _tut1 = true;
            }
        }
        if (DataManager.KoiLevel >= 1)
        {
            _tut2 = true;
        }
        if (DataManager.BambooLevel >= 4)
        {
            _tut3 = true;
        }
    }

    private void SetMenuButtonUnlock()  // set the color   
    {
        if (_tut1 == false)
        {
            _menuPondButton.image.color = new Color(0.2f, 0.2f, 0.2f, 255);
            _toThePond.image.color = new Color(0.2f, 0.2f, 0.2f, 255);
            _tut1ReqGO.SetActive(true);
            _tut1ReqGO_toThePond.SetActive(true);

        }
        else if (_tut1 == true)
        {
            _menuPondButton.image.color = new Color(255, 255, 255, 255);
            _toThePond.image.color = new Color(255, 255, 255, 255);
            _tut1ReqGO.SetActive(false);
            _tut1ReqGO_toThePond.SetActive(false);
        }

        if (_tut2 == false)
        {
            _menuGardenButton.image.color = new Color(0.2f, 0.2f, 0.2f, 255);
            _toTheGarden.image.color = new Color(0.2f, 0.2f, 0.2f, 255);
            _tut2ReqGO.SetActive(true);
            _tut2ReqGO_toTheGarden.SetActive(true);
        }
        else if (_tut2 == true)
        {
            _menuGardenButton.image.color = new Color(255, 255, 255, 255);
            _toTheGarden.image.color = new Color(255, 255, 255, 255);
            _tut2ReqGO.SetActive(false);
            _tut2ReqGO_toTheGarden.SetActive(false);
        }

        if (_tut3 == false)
        {
            _menuTempleButton.image.color = new Color(0.2f, 0.2f, 0.2f, 255);
            _fromGardenToTemple.image.color = new Color(0.2f, 0.2f, 0.2f, 255);
            _tut3ReqGO.SetActive(true);
            _tut3ReqGO_toTheTemple.SetActive(true);
        }
        else if (_tut3 == true)
        {
            _menuTempleButton.image.color = new Color(255, 255, 255, 255);
            _fromGardenToTemple.image.color = new Color(255, 255, 255, 255);
            _tut3ReqGO.SetActive(false);
            _tut3ReqGO_toTheTemple.SetActive(false);
        }
    }

        private void SetTempleTutorialText()
    {
        if (DataManager.Stage == 1)
        {
            _templeTutorialTextCo = templeTutorialTextRoutine();
            StartCoroutine(_templeTutorialTextCo);
        }
        else 
        {
            return;
        }

    }

    private IEnumerator templeTutorialTextRoutine()
    {
        _templeTutorialText1.gameObject.SetActive(false);
        _templeTutorialText2.gameObject.SetActive(false);
        _templeTutorialText3.gameObject.SetActive(false);
        _templeTutorialText1.gameObject.SetActive(true);
        yield return new WaitForSeconds(4.8f);
        _templeTutorialText1.gameObject.SetActive(false);
        _templeTutorialText2.gameObject.SetActive(true);
        yield return new WaitForSeconds(4.8f);
        _templeTutorialText2.gameObject.SetActive(false);
        _templeTutorialText3.gameObject.SetActive(true);
        yield return new WaitForSeconds(4.8f);
        _templeTutorialText3.gameObject.SetActive(false);
        StopCoroutine(templeTutorialTextRoutine());
    }

    private void SetTempleIntroText()
    {
        if (DataManager.Stage != 1)
        {
            _templeTextCo = templeIntroTextRoutine();
            StartCoroutine(_templeTextCo);
        }
        if (DataManager.Stage ==2)
        {
            _templeIntroText1.text = "Sometimes";
            _templeIntroText2.text = "the best way to solve a problem";
            _templeIntroText3.text = "is to do nothing";
        }
        else if (DataManager.Stage == 3)
        {
            _templeIntroText1.text = "Stubborn";
            _templeIntroText2.text = "They never negotiate";
            _templeIntroText3.text = "How do you work around them?";
        }
        else if (DataManager.Stage == 4)
        {
            _templeIntroText1.text = "Desperation";
            _templeIntroText2.text = "They are Vortexes";
            _templeIntroText3.text = "Sucking up all the negative energy";
        }
        else if (DataManager.Stage == 5)
        {
            _templeIntroText1.text = "Ego and drugs";
            _templeIntroText2.text = "They make you feel good";
            _templeIntroText3.text = "But they are also the problem";
        }
        else if (DataManager.Stage == 6)
        {
            _templeIntroText1.text = "Anger";
            _templeIntroText2.text = "They release frustrations";
            _templeIntroText3.text = "Regardless when and where";
        }
        else if (DataManager.Stage == 7)
        {
            _templeIntroText1.text = "Comments";
            _templeIntroText2.text = "They always come to you";
            _templeIntroText3.text = "Do you care? Or should you?";
        }
        else if (DataManager.Stage == 8)
        {
            _templeIntroText1.text = "Depressions";
            _templeIntroText2.text = "Too much desperations";
            _templeIntroText3.text = "Cause depressions";
        }
        else if (DataManager.Stage == 9)
        {
            _templeIntroText1.text = "Stress";
            _templeIntroText2.text = "You are trying so hard to tolerate";
            _templeIntroText3.text = "But you cannot do this forever";
        }
        else if (DataManager.Stage == 10)
        {
            _templeIntroText1.text = "Even when problems become very complicated";
            _templeIntroText2.text = "Believe us, there is always a way out";
            _templeIntroText3.text = "And, it is never too late.";
        }
        else if (DataManager.Stage == 11)
        {
            _templeIntroText1.text = "When stress comes in play";
            _templeIntroText2.text = "They act like a bomb timer";
            _templeIntroText3.text = "You can no longer do nothing";
        }
        else if (DataManager.Stage == 12)
        {
            _templeIntroText1.text = "When you feel angry";
            _templeIntroText2.text = "Do others make you angry?";
            _templeIntroText3.text = "Or you making yourself angry?";
        }
        else if (DataManager.Stage == 13)
        {
            _templeIntroText1.text = "A huge problem";
            _templeIntroText2.text = "Will cause small problems";
            _templeIntroText3.text = "Will cause even smaller problems";
        }
        else if (DataManager.Stage == 14)
        {
            _templeIntroText1.text = "Flooded with comments";
            _templeIntroText2.text = "People comment you because they like commenting";
            _templeIntroText3.text = "Not necessarily because of what you have done";
        }
        else if (DataManager.Stage == 15)
        {
            _templeIntroText1.text = "Try to see things differently";
            _templeIntroText2.text = "Are they Anger? Stress? No. Not anymore";
            _templeIntroText3.text = "They are fireworks. Please Enjoy it! Please enjoy Your life";
        }
        else if (DataManager.Stage == 16)
        {
            _templeIntroText1.text = "You are now the True Master of this temple";
            _templeIntroText2.text = "We are appreciate that you can make it to this far!";
            _templeIntroText3.text = "Hope you like this Garden!";
        }

 
    }
    private IEnumerator templeIntroTextRoutine()
    {
        _templeIntroText1.gameObject.SetActive(false);
        _templeIntroText2.gameObject.SetActive(false);
        _templeIntroText3.gameObject.SetActive(false);
        _templeIntroText1.gameObject.SetActive(true);
        yield return new WaitForSeconds(4.8f);
        _templeIntroText1.gameObject.SetActive(false);
        _templeIntroText2.gameObject.SetActive(true);
        yield return new WaitForSeconds(4.8f);
        _templeIntroText2.gameObject.SetActive(false);
        _templeIntroText3.gameObject.SetActive(true);
        yield return new WaitForSeconds(4.8f);
        _templeIntroText3.gameObject.SetActive(false);
        StopCoroutine(templeTutorialTextRoutine());
    }

    private void clearOffTextGO()
    {
        _templeIntroText1.gameObject.SetActive(false);
        _templeIntroText2.gameObject.SetActive(false);
        _templeIntroText3.gameObject.SetActive(false);
        _templeTutorialText1.gameObject.SetActive(false);
        _templeTutorialText2.gameObject.SetActive(false);
        _templeTutorialText3.gameObject.SetActive(false);
    }

    private void SetGardenCollectText()
    {
        if(DataManager.PlantEnergy <= 0 && GameManager.Instance.State == EnumGameState.Garden)
        {
            _gardenCollect.gameObject.SetActive(true);
        }
        else if (DataManager.PlantEnergy >0 || GameManager.Instance.State != EnumGameState.Garden)
        {
            _gardenCollect.gameObject.SetActive(false);
        }
    }
    private void SetAfterGardenCollectText()
    {
        if (DataManager.PlantEnergy >0 && DataManager.PlantEnergy <=2000 )
        {
            if (GameManager.Instance.State == EnumGameState.Garden)
            {
                _afterGardenCollect.gameObject.SetActive(true);
            }
            else if (GameManager.Instance.State != EnumGameState.Garden)
            {
                _afterGardenCollect.gameObject.SetActive(false);
            }
           
        }
        else if (DataManager.PlantEnergy > 2000)
        {
            _afterGardenCollect.gameObject.SetActive(false);
        }
       

    }

    private void SetAfterFishCollect()
    {
        if(DataManager.FishEnergy <= 1000 && GameManager.Instance.State == EnumGameState.Pond )
        {
            _afterFishCollect.gameObject.SetActive(true);
        } else if (DataManager.FishEnergy >1000 || GameManager.Instance.State != EnumGameState.Pond)
        {
            _afterFishCollect.gameObject.SetActive(false);
        }    
    }

    private void SetClickStatueText()
    {
        if(DataManager.Stage ==1 && GameManager.Instance.State == EnumGameState.InsideTemple)
        {
            _clickStatue.gameObject.SetActive(true);
        } else 
        {
            _clickStatue.gameObject.SetActive(false);
        }
    
    }
    #endregion

}
