﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

    #region Cache

        private GameObject _cacheGO;
        public GameObject CacheGO {

            get {

                if (null == _cacheGO) {
                    _cacheGO = this.gameObject;
                }

                return _cacheGO;
            }

        }

        private Transform _cacheTF;
        public Transform CacheTF {

            get {

                if (null == _cacheTF) {
                    _cacheTF = this.transform;
                }

                return _cacheTF;
            }

        }

    #endregion

    private static object _lock = new object ();

    private static T _instance;
    public static T Instance { get { return GetInstance(); } }

    public static T GetInstance () {

        if (_applicationIsQuitting) {
            return null;
        }

        lock (_lock) {

            if (null == _instance) {
                _instance = FindObjectOfType<T> ();

                if (FindObjectsOfType (typeof(T)).Length > 1) {
					Debug.LogError ("場景中有多於一個的單例, Type: " + typeof(T));
					return _instance;
				}

                if (null == _instance) {
                    GameObject singleton = new GameObject (typeof(T).ToString ());
					_instance = singleton.AddComponent<T> ();
                }

            }

        }

        return _instance;
    }

    private void Awake () {
        OnAwake ();
    }
    protected virtual void OnAwake () {}

    private void Start () {
        OnStart ();
    }
    protected virtual void OnStart () {}

    public void Init () {
        OnInit ();
    }
    protected virtual void OnInit () {}


    protected static bool _applicationIsQuitting = false;
    protected void OnDestroy () {
		_applicationIsQuitting = true;
	}

}
