﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : Singleton<Config> {

    [Header ("WaterWheel")]
    [Tooltip ("水車容量升級花費的初值")]
    [SerializeField] private int _waterWheelCapacityDefaultCost;
    [Tooltip ("水車容量升級花費的增量-倍數")]
    [SerializeField] private float _waterWheelCapacityUpgradeCostPerLevel;
    [Tooltip ("水車容量的初值")]
    [SerializeField] private int _waterWheelCapacityDefaultAbility;
    [Tooltip ("水車容量的增量-固值")]
    [SerializeField] private int _waterWheelCapacityUpgradeAbilityPerLevel;
    [Tooltip ("水車水能升級花費的初值")]
    [SerializeField] private int _waterWheelWaterEnergyDefaultCost;
    [Tooltip ("水車水能升級花費的增量-倍數")]
    [SerializeField] private float _waterWheelWaterEnergyUpgradeCostPerLevel;
    [Tooltip ("水車水能的初值")]
    [SerializeField] private int _waterWheelWaterEnergyDefaultAbility;
    [Tooltip ("水車水能的增量-固值")]
    [SerializeField] private int _waterWheelWaterEnergyUpgradeAbilityPerLevel;

    public static int WaterWheelCapacityDefaultCost { get { return Instance._waterWheelCapacityDefaultCost; } }
    public static float WaterWheelCapacityUpgradeCostPerLevel { get { return Instance._waterWheelCapacityUpgradeCostPerLevel; } }
    public static int WaterWheelCapacityDefaultAbility { get { return Instance._waterWheelCapacityDefaultAbility; } }
    public static int WaterWheelCapacityUpgradeAbilityPerLevel { get { return Instance._waterWheelCapacityUpgradeAbilityPerLevel; }set { Instance._waterWheelCapacityUpgradeAbilityPerLevel = value; } }
    public static int WaterWheelWaterEnergyDefaultCost { get { return Instance._waterWheelWaterEnergyDefaultCost; } }
    public static float WaterWheelWaterEnergyUpgradeCostPerLevel { get { return Instance._waterWheelWaterEnergyUpgradeCostPerLevel; } }
    public static int WaterWheelWaterEnergyDefaultAbility { get { return Instance._waterWheelWaterEnergyDefaultAbility; } }
    public static int WaterWheelWaterEnergyUpgradeAbilityPerLevel { get { return Instance._waterWheelWaterEnergyUpgradeAbilityPerLevel; } set { Instance._waterWheelWaterEnergyUpgradeAbilityPerLevel = value; }}

    [Header ("Plant")]
    [Header ("Bamboo")]
    [Tooltip ("竹的解鎖關卡")]
    [SerializeField] private int _bambooUnlockStage = 1;
    [Tooltip ("竹的等級上限")]
    [SerializeField] private int _bambooLevelLimit = 12;
    [Tooltip ("竹升級的初始費用")]
    [SerializeField] private int _bambooDefaultCost = 1800;
    [Tooltip ("竹升級花費的增量-倍數")]
    [SerializeField] private float _bambooUpgradeCostPerLevel = 2f;

    public static int BambooUnlockStage { get { return Instance._bambooUnlockStage; } }
    public static int BambooLevelLimit { get { return Instance._bambooLevelLimit; } }
    public static int BambooDefaultCost { get { return Instance._bambooDefaultCost; } }
    public static float BambooUpgradeCostPerLevel { get { return Instance._bambooUpgradeCostPerLevel; } }

    [Header ("Reed")]
    [Tooltip ("蘆葦的解鎖關卡")]
    [SerializeField] private int _reedUnlockStage = 9;
    [Tooltip ("蘆葦的等級上限")]
    [SerializeField] private int _reedLevelLimit = 7;
    [Tooltip ("蘆葦升級的初始費用")]
    [SerializeField] private int _reedDefaultCost = 46000;
    [Tooltip ("蘆葦升級花費的增量-倍數")]
    [SerializeField] private float _reedUpgradeCostPerLevel = 3.2f;

    public static int ReedUnlockStage { get { return Instance._reedUnlockStage; } }
    public static int ReedLevelLimit { get { return Instance._reedLevelLimit; } }
    public static int ReedDefaultCost { get { return Instance._reedDefaultCost; } }
    public static float ReedUpgradeCostPerLevel { get { return Instance._reedUpgradeCostPerLevel; } }

    [Header ("Mushroom")]
    [Tooltip ("蘑菇的解鎖關卡")]
    [SerializeField] private int _mushroomUnlockStage = 3;
    [Tooltip ("蘑菇的等級上限")]
    [SerializeField] private int _mushroomLevelLimit = 3;
    [Tooltip ("蘑菇升級的初始費用")]
    [SerializeField] private int _mushroomDefaultCost = 200000;
    [Tooltip ("蘑菇升級花費的增量-倍數")]
    [SerializeField] private float _mushroomUpgradeCostPerLevel = 8f;

    public static int MushroomUnlockStage { get { return Instance._mushroomUnlockStage; } }
    public static int MushroomLevelLimit { get { return Instance._mushroomLevelLimit; } }
    public static int MushroomDefaultCost { get { return Instance._mushroomDefaultCost; } }
    public static float MushroomUpgradeCostPerLevel { get { return Instance._mushroomUpgradeCostPerLevel; } }
    
    [Header ("Tree")]
    [Tooltip ("樹的解鎖關卡")]
    [SerializeField] private int _treeUnlockStage = 6;
    [Tooltip ("樹的等級上限")]
    [SerializeField] private int _treeLevelLimit = 3;
    [Tooltip ("樹升級的費用")]
    [SerializeField] private int[] _treeCost = new int [3] { 750000, 2500000, 12500000 };

    public static int TreeUnlockStage { get { return Instance._treeUnlockStage; } }
    public static int TreeLevelLimit { get { return Instance._treeLevelLimit; } }
    public static int[] TreeCost { get { return Instance._treeCost; } }

    [Header ("Sakura")]
    [Tooltip ("櫻花的解鎖關卡")]
    [SerializeField] private int _sakuraUnlockStage = 12;
    [Tooltip ("櫻花的等級上限")]
    [SerializeField] private int _sakuraLevelLimit = 3;
    [Tooltip ("櫻花升級的費用")]
    [SerializeField] private int[] _sakuraCost = new int [3] { 3900000, 15000000, 25000000 };

    public static int SakuraUnlockStage { get { return Instance._sakuraUnlockStage; } }
    public static int SakuraLevelLimit { get { return Instance._sakuraLevelLimit; } }
    public static int[] SakuraCost { get { return Instance._sakuraCost; } }

    [Header ("Pond")]
    [Header ("Koi")]
    [Tooltip ("錦鯉的解鎖關卡")]
    [SerializeField] private int _koiUnlockStage = 2;
    [Tooltip ("錦鯉的等級上限")]
    [SerializeField] private int _koiLevelLimit = 12;
    [Tooltip ("錦鯉升級的初始費用")]
    [SerializeField] private int _koiDefaultCost = 2500;
    [Tooltip ("錦鯉升級花費的增量-倍數")]
    [SerializeField] private float _koiUpgradeCostPerLevel = 2.1f;

    public static int KoiUnlockStage { get { return Instance._koiUnlockStage; } }
    public static int KoiLevelLimit { get { return Instance._koiLevelLimit; } }
    public static int KoiDefaultCost { get { return Instance._koiDefaultCost; } }
    public static float KoiUpgradeCostPerLevel { get { return Instance._koiUpgradeCostPerLevel; } }

    [Header ("Turtle")]
    [Tooltip ("龜的解鎖關卡")]
    [SerializeField] private int _turtleUnlockStage = 5;
    [Tooltip ("龜的等級上限")]
    [SerializeField] private int _turtleLevelLimit = 5;
    [Tooltip ("龜升級的初始費用")]
    [SerializeField] private int _turtleDefaultCost = 35000;
    [Tooltip ("龜升級花費的增量-倍數")]
    [SerializeField] private float _turtleUpgradeCostPerLevel = 5f;

    public static int TurtleUnlockStage { get { return Instance._turtleUnlockStage; } }
    public static int TurtleLevelLimit { get { return Instance._turtleLevelLimit; } }
    public static int TurtleDefaultCost { get { return Instance._turtleDefaultCost; } }
    public static float TurtleUpgradeCostPerLevel { get { return Instance._turtleUpgradeCostPerLevel; } }

    [Header ("Snails")]
    [Tooltip ("蝸牛的解鎖關卡")]
    [SerializeField] private int _snailsUnlockStage = 7;
    [Tooltip ("蝸牛的等級上限")]
    [SerializeField] private int _snailsLevelLimit = 12;
    [Tooltip ("蝸牛升級的初始費用")]
    [SerializeField] private int _snailsDefaultCost = 180000;
    [Tooltip ("蝸牛升級花費的增量-倍數")]
    [SerializeField] private float _snailsUpgradeCostPerLevel = 1.5f;

    public static int SnailsUnlockStage { get { return Instance._snailsUnlockStage; } }
    public static int SnailsLevelLimit { get { return Instance._snailsLevelLimit; } }
    public static int SnailsDefaultCost { get { return Instance._snailsDefaultCost; } }
    public static float SnailsUpgradeCostPerLevel { get { return Instance._snailsUpgradeCostPerLevel; } }

    [Header ("Frog")]
    [Tooltip ("青蛙的解鎖關卡")]
    [SerializeField] private int _frogUnlockStage = 10;
    [Tooltip ("青蛙的等級上限")]
    [SerializeField] private int _frogLevelLimit = 1;
    [Tooltip ("青蛙升級的初始費用")]
    [SerializeField] private int _frogDefaultCost = 7000000;
    [Tooltip ("青蛙升級的增量-倍數")]
    [SerializeField] private float _frogUpgradeCostPerLevel = 1.5f;

    public static int FrogUnlockStage { get { return Instance._frogUnlockStage; } }
    public static int FrogLevelLimit { get { return Instance._frogLevelLimit; } }
    public static int FrogDefaultCost { get { return Instance._frogDefaultCost; } }
    public static float FrogUpgradeCostPerLevel { get { return Instance._frogUpgradeCostPerLevel; } }

    [Header ("DragonFly")]
    [Tooltip ("蜻蜓的解鎖關卡")]
    [SerializeField] private int _dragonflyUnlockStage = 11;
    [Tooltip ("蜻蜓的等級上限")]
    [SerializeField] private int _dragonflyLevelLimit = 3;
    [Tooltip ("蜻蜓升級的費用")]
    [SerializeField] private int[] _dragonflyCost = new int [3] { 5000000, 15000000, 50000000 };

    public static int DragonFlyUnlockStage { get { return Instance._dragonflyUnlockStage; } }
    public static int DragonFlyLevelLimit { get { return Instance._dragonflyLevelLimit; } }
    public static int[] DragonFlyCost { get { return Instance._dragonflyCost; } }

}
