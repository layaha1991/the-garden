﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimtor : Singleton<SimpleAnimtor>
{

    public void StopAnim(Coroutine anim)
    {

        if (anim != null)
        {
            StopCoroutine(anim);
        }

    }

    #region Plant Grow

        public Coroutine PlayGrowAnim(Transform target, float delay, Action callback)
        {
            return StartCoroutine(GrowAnim(target, delay, callback));
        }

        private IEnumerator GrowAnim(Transform target, float delay, Action callback) {
            yield return new WaitForSeconds(delay);

            float t = 0f;
            float duration = 1.5f;

            Vector3 startScale = Vector3.one;
            startScale.y = 0f;
            Vector3 endScale = Vector3.one;
            target.localScale = startScale;

            while (t <= duration)
            {
                t += Time.deltaTime;

                target.localScale = Vector3.Lerp(startScale, endScale, t / duration);

                yield return null;
            }
            target.localScale = endScale;

            yield return new WaitForSeconds(0.3f);

            if (callback != null)
            {
                callback();
            }

        }

    #endregion

    #region Pond Grow

        public Coroutine PlayPondGrowAnim (Transform target, SpriteRenderer body, float delay, Action callback) {
            return StartCoroutine (PondGrowAnim (target, body, delay, callback));
        }

        private IEnumerator PondGrowAnim (Transform target, SpriteRenderer body, float delay, Action callback) {
            yield return new WaitForSeconds(delay);

            float t = 0f;
            float duration = 1f;

            Vector3 startScale = Vector3.zero;
            Vector3 endScale = Vector3.one;
            target.localScale = startScale;

            Color startColor = body.color;
            Color endColor = startColor;
            startColor.a = 0f;

            while (t <= duration) {
                t += Time.deltaTime;

                float d = t / duration;

                target.localScale = Vector3.Lerp(startScale, endScale, d);
                body.color = Color.Lerp (startColor, endColor, d);

                yield return null;
            }
            target.localScale = endScale;

            yield return new WaitForSeconds(0.3f);

            if (callback != null) {
                callback();
            }

        }

    #endregion

}
