﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameTool {

    #region Get waterwheel info

        public static FacilityInfo GetWaterWheelCapacityInfo (int level) {
            int l = level + 1;

            int defaultCost = Config.WaterWheelCapacityDefaultCost;
            float upgradeCost = Config.WaterWheelCapacityUpgradeCostPerLevel;
            int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
            string nextCostStr = nextCost.ToString ();

            int defaultAbility = Config.WaterWheelCapacityDefaultAbility ;
            int upgradeAbility = Config.WaterWheelCapacityUpgradeAbilityPerLevel;


            int curAbility = defaultAbility + (level* Config.WaterWheelCapacityUpgradeAbilityPerLevel);
            int nextAbility = defaultAbility + (l * Config.WaterWheelCapacityUpgradeAbilityPerLevel);
            string curFunc = curAbility.ToString ();
            string nextFunc = nextAbility.ToString ();

            return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
        }

        public static FacilityInfo GetWaterWheelWaterEnergyInfo (int level) {
            int l = level + 1;

            int defaultCost = Config.WaterWheelWaterEnergyDefaultCost;
            float upgradeCost = Config.WaterWheelWaterEnergyUpgradeCostPerLevel;
            int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
            string nextCostStr = nextCost.ToString ();

            int defaultAbiilty = Config.WaterWheelWaterEnergyDefaultAbility;
            int upgradeAbility = Config.WaterWheelWaterEnergyUpgradeAbilityPerLevel;
            int curAbility = defaultAbiilty + (int)(level * upgradeAbility);
            int nextAbility = defaultAbiilty + (int)(l * upgradeAbility);
    
            string curFunc = curAbility.ToString ();
            string nextFunc = nextAbility.ToString ();
            return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
       
        }

        public static int GetWaterWheelCapacityCost (int level) {
            int l = level;

            int defaultCost = Config.WaterWheelCapacityDefaultCost;
            float upgradeCost = Config.WaterWheelCapacityUpgradeCostPerLevel;
            int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

            return nextCost;
        }

        public static int GetWaterWheelWaterEnergyCost (int level) {
            int l = level;

            int defaultCost = Config.WaterWheelWaterEnergyDefaultCost;
            float upgradeCost = Config.WaterWheelWaterEnergyUpgradeCostPerLevel;
            int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

            return nextCost;
        }

        public static int GetWaterWheelCapacity (int level) {
            int l = level;

            int defaultAbiilty = Config.WaterWheelCapacityDefaultAbility;
            int upgradeAbility = Config.WaterWheelCapacityUpgradeAbilityPerLevel ;
            int curAbility = defaultAbiilty + (int)(l * upgradeAbility);

            return curAbility;
        }

        public static int GetWaterWheelWaterEnergyPerSecond (int level) {
            int l = level;

            int defaultAbiilty = Config.WaterWheelWaterEnergyDefaultAbility;
            int upgradeAbility = Config.WaterWheelWaterEnergyUpgradeAbilityPerLevel;
            int curAbility = defaultAbiilty + (int)(l * upgradeAbility);

            return curAbility;
        }

    #endregion

    #region Get Plant

        #region Bamboo

            public static bool BambooIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.BambooUnlockStage;
            }

            public static FacilityInfo GetBambooInfo (int level) {
                int nextLevel = (int)(20 * Mathf.Pow(1.2f, level+1));

                int defaultCost = Config.BambooDefaultCost;
                float upgradeCost = Config.BambooUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString ();

                int curAbility = (int)(20 * Mathf.Pow(1.2f, level));;
                 int nextAbility = nextLevel;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.BambooLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetBambooCost (int level) {
                int l = level;

                int defaultCost = Config.BambooDefaultCost;
                float upgradeCost = Config.BambooUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region Tree

            public static bool TreeIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.TreeUnlockStage;
            }

            public static FacilityInfo GetTreeInfo (int level) {
            int nextLevel = (int)(150) + 30*(level+1);


                int cost = 0;
                if (level < Config.TreeLevelLimit) {
                    cost = Config.TreeCost [level];
                }
                string costStr = cost.ToString ();

                int curAbility = 150;
                int nextAbility = nextLevel;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.TreeLevelLimit) {
                    nextFunc = "MAX";
                    costStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, costStr);
            }

            public static int GetTreeCost (int level) {
                // Debug.Log("level: " + level);
                int cost = Config.TreeCost [level];

                return cost;
            }

        #endregion

        #region Reed

            public static bool ReedIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.ReedUnlockStage;
            }

            public static FacilityInfo GetReedInfo (int level) {
            int nextLevel = (int)(150 * Mathf.Pow(1.1f, level + 1));


                int defaultCost = Config.ReedDefaultCost;
                float upgradeCost = Config.ReedUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString ();

                int curAbility =(int)(150 * Mathf.Pow(1.1f, level));
;
                int nextAbility = nextLevel;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.ReedLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetReedCost (int level) {
                int l = level;

                int defaultCost = Config.ReedDefaultCost;
                float upgradeCost = Config.ReedUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region Mushroom

            public static bool MushroomIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.MushroomUnlockStage;
            }

            public static FacilityInfo GetMushroomInfo (int level) {
            int l = (int)(100 * Mathf.Pow(1.5f, level + 1));

                int defaultCost = Config.MushroomDefaultCost;
                float upgradeCost = Config.MushroomUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString();
                int curAbility = (int)(100 * Mathf.Pow(1.5f, level));
                int nextAbility = l;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.MushroomLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetMushroomCost (int level) {
                int l = level;

                int defaultCost = Config.MushroomDefaultCost;
                float upgradeCost = Config.MushroomUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region Sakura

            public static bool SakuraIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.SakuraUnlockStage;
            }

            public static FacilityInfo GetSakuraInfo (int level) {
            int nextLevel =(int)(300 * Mathf.Pow(1.3f, level + 1));


                int cost = 0;
                if (level < Config.SakuraLevelLimit) 
        {
                cost = Config.SakuraCost [level];
                }
                string costStr = cost.ToString ();

                int curAbility = (int)(300 * Mathf.Pow(1.3f, level));

                int nextAbility = nextLevel;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.SakuraLevelLimit) 
        {
                    nextFunc = "MAX";
                    costStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, costStr);
            }

            public static int GetSakuraCost (int level) {
                // Debug.Log("level: " + level);
                int cost = Config.SakuraCost [level];

                return cost;
            }

        #endregion

    #endregion

    #region Get Pond

        #region Koi

            public static bool KoiIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.KoiUnlockStage;
            }

            public static FacilityInfo GetKoiInfo (int level) {
                int l = (int)(22 * Mathf.Pow(1.2f, level+1));
;

                int defaultCost = Config.KoiDefaultCost;
                float upgradeCost = Config.KoiUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString ();

                int curAbility = (int)(22 * Mathf.Pow(1.2f, level));;
                int nextAbility = l;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.KoiLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetKoiCost (int level) {
                int l = level;

                int defaultCost = Config.KoiDefaultCost;
                float upgradeCost = Config.KoiUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region Turtle

            public static bool TurtleIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.TurtleUnlockStage;
            }

            public static FacilityInfo GetTurtleInfo (int level) {
        int l = (int)(35 * Mathf.Pow(1.6f, level + 1));;

                int defaultCost = Config.TurtleDefaultCost;
                float upgradeCost = Config.TurtleUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString ();

        int curAbility =(int) (35 * Mathf.Pow(1.6f, level));
                int nextAbility = l;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.TurtleLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetTurtleCost (int level) {
                int l = level;

                int defaultCost = Config.TurtleDefaultCost;
                float upgradeCost = Config.TurtleUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region Snails

            public static bool SnailsIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.SnailsUnlockStage;
            }

            public static FacilityInfo GetSnailsInfo (int level) {
        int l = (int)(60 * Mathf.Pow(1.1f, level + 1));

                int defaultCost = Config.SnailsDefaultCost;
                float upgradeCost = Config.SnailsUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString ();

        int curAbility = (int)(60 * Mathf.Pow(1.1f, level));
                int nextAbility = l;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.SnailsLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetSnailsCost (int level) {
                int l = level;

                int defaultCost = Config.SnailsDefaultCost;
                float upgradeCost = Config.SnailsUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region Frog

            public static bool FrogIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.FrogUnlockStage;
            }

            public static FacilityInfo GetFrogInfo (int level) {
            int l = (int)(170 * Mathf.Pow(1f, level+1));

                int defaultCost = Config.FrogDefaultCost;
                float upgradeCost = Config.FrogUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, level));
                string nextCostStr = nextCost.ToString ();

                 int curAbility = (int)(170 * Mathf.Pow(1f, level));
                int nextAbility = l;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.FrogLevelLimit) {
                    nextFunc = "MAX";
                    nextCostStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, nextCostStr);
            }

            public static int GetFrogCost (int level) {
                int l = level;

                int defaultCost = Config.FrogDefaultCost;
                float upgradeCost = Config.FrogUpgradeCostPerLevel;
                int nextCost = (int)(defaultCost * Mathf.Pow(upgradeCost, l));

                return nextCost;
            }

        #endregion

        #region DragonFly

            public static bool DragonFlyIsUnlock () {
                int stage = LevelManager.Instance.Stage;
                return stage >= Config.DragonFlyUnlockStage;
            }

            public static FacilityInfo GetDragonFlyInfo (int level) {
                int nextLevel = (int)(150 * Mathf.Pow(3f + ((level+1)*4), level+1));

                int cost = 0;
                if (level < Config.DragonFlyLevelLimit) {
                    cost = Config.DragonFlyCost [level];
                }
                string costStr = cost.ToString ();

                int curAbility = (int)(150 * Mathf.Pow(3f + ((level) * 4), level ));
                int nextAbility = nextLevel;
                string curFunc = curAbility.ToString ();
                string nextFunc = nextAbility.ToString ();

                if (level >= Config.DragonFlyLevelLimit) {
                    nextFunc = "MAX";
                    costStr = "MAX";
                }

                return new FacilityInfo (level, curFunc, nextFunc, costStr);
            }

            public static int GetDragonFlyCost (int level) {
                // Debug.Log("level: " + level);
                int cost = Config.DragonFlyCost [level];

                return cost;
            }

        #endregion

    #endregion

    #region Common

        public static bool IsEnoughWaterEnergy (int cost) {
            return EnergyManager.Instance.WaterEnergy >= cost;
        }

    #endregion

}
