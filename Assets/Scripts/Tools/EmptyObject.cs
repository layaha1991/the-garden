﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyObject : MonoBehaviour {

    [SerializeField] private bool _debug = true;
    [SerializeField] private Color _color = Color.white;
    [SerializeField] private float _radius = 1f;

    private void OnDrawGizmosSelected() {

        if (!_debug)
            return;

        Gizmos.color = _color;
        Gizmos.DrawSphere (transform.position, _radius);
    }

}
