﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayManager : Singleton<SprayManager> {


	[SerializeField] private GameObject LeftSpray;
	[SerializeField] private GameObject RightSpray;


	public void stage0(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage0();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage0();
	}

	public void stage1(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage1();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage1();
	}

	public void stage2(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage2();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage2();
	}
	public void stage3(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage3();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage3();
	}
	public void stage4(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage4();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage4();
	}
	public void stage5(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage5();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage5();
	}
	public void stage6(){
		LeftSpray.gameObject.GetComponent<SprayDefine> ().stage6();
		RightSpray.gameObject.GetComponent<SprayDefine> ().stage6();
	}


}
