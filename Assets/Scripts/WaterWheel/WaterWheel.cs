﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void WaterWheelPowerChangedEventHandler (float power, int capacity);

[Serializable]
public struct WaterWheelStatus {

	[Range (0, 1)]
	[SerializeField] private float _minPercent;
	[Range (0, 1)]
	[SerializeField] private float _maxPercent;
	[Range (0, 1)]
	// 增加的能量百分比
	[SerializeField] private float _percent;

	public bool InRange (float percent) {

		if (percent >= _minPercent && percent <= _maxPercent) {
			return true;
		}

		return false;
	}

	public float Percent { get { return _percent; } }

}

public class WaterWheel : Singleton<WaterWheel> {

	[Tooltip ("Power狀態")]
	[SerializeField] private WaterWheelStatus[] _powerStatus;
	[Tooltip ("Energy狀態")]
	[SerializeField] private WaterWheelStatus[] _energyStatus;
	[Tooltip ("每秒減少的Power")]
	[SerializeField] private float _reducePowerPerSecond = 3f;
	[Tooltip ("旋轉速度")]
	[SerializeField] private float _rotateSpeed = 5f;
	[Tooltip ("最小增加的Power")]
	[SerializeField] private int _minAddPowerValue = 2;

	[Header ("UI")]
	[SerializeField] private Slider _powerSlider;
	[SerializeField] private Text _capacityText;
	[SerializeField] private Text _curPowerText;

	// 當前的旋轉速度系數
	[SerializeField]private float _rotateSpeedFactor = 0f;

	// 當前power
	private float _curPower = 0;
	public float CurPower {
		get { return _curPower; }
		private set {
			_curPower = value;

			if (PowerChanged != null) {
                PowerChanged (_curPower, (int)Capacity);
			}

			DataManager.WaterWheelPower = _curPower;
		}   
	}

	// 容量
	public float Capacity { 
		get; 
		private set; 
	}

	// 每秒增加的水量
	public float WaterEnergyPerSecond {
		get;
		private set;
	}

	// 水車容量等級
	public int CapacityLevel {
		get;
		private set;
	}

	// 水車能量等級
	public int EnergyLevel {
		get;
		private set;
	}

	// Power的比例
	public float PowerPercent {

		get {
			return _curPower / Capacity;
		}

	}

	// 能量改變事件
	public event WaterWheelPowerChangedEventHandler PowerChanged;

	protected override void OnInit () {
		OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

		CapacityLevel = DataManager.WaterWheelCapacityLevel;
		EnergyLevel = DataManager.WaterWheelEnergyLevel;

		CurPower = DataManager.WaterWheelPower;
		Capacity = GameTool.GetWaterWheelCapacity (CapacityLevel);
		WaterEnergyPerSecond = GameTool.GetWaterWheelWaterEnergyPerSecond (EnergyLevel);

		_capacityText.text = Capacity.ToString ();

		// Debug.Log("WaterWheel-CapacityLevel: " + CapacityLevel);
		// Debug.Log("WaterWheel-Capacity: " + Capacity);
		// Debug.Log("WaterWheel-EnergyLevel: " + EnergyLevel);
		// Debug.Log("WaterWheel-Energy: " + WaterEnergyPerSecond);
	}

	#region Update

	private void Update () {
		AddWater (Time.deltaTime);
		ReducePower (Time.deltaTime);

		// Debug.Log("Current Water Power: " + _curPower);        
		SetPowerSlider ();

		Rotate (Time.deltaTime);

		//Debug.Log (_rotateSpeedFactor);
		PhasedSpray ();
	}


	private void PhasedSpray(){
		//According to the rotaterSpeedFactor phase the spray

		if ((_rotateSpeedFactor < 0.04) && (_rotateSpeedFactor > 0.01)) {
			SprayManager.Instance.stage1 ();
			//			Debug.Log ("stage1 , the rotateSpeedFactor is" + _rotateSpeedFactor);
		}

		if ((_rotateSpeedFactor < 0.2) && (_rotateSpeedFactor >= 0.04)) {
			//			Debug.Log ("stage2 , the rotateSpeedFactor is" + _rotateSpeedFactor);
			SprayManager.Instance.stage2 ();
		}

		else if ((_rotateSpeedFactor < 0.6) && (_rotateSpeedFactor >= 0.2)) {
			//			Debug.Log ("stage3, the rotateSpeedFactor is" + _rotateSpeedFactor);
			SprayManager.Instance.stage3 ();
		}

		else if ((_rotateSpeedFactor < 0.8) && (_rotateSpeedFactor >= 0.6)) {
			//			Debug.Log ("stage4, the rotateSpeedFactor is" + _rotateSpeedFactor);
			SprayManager.Instance.stage4 ();
		}

		else if ((_rotateSpeedFactor < 0.9) && (_rotateSpeedFactor >= 0.8)) {
			//			Debug.Log ("stage5, the rotateSpeedFactor is" + _rotateSpeedFactor);
			SprayManager.Instance.stage5 ();
		}

		else if ((_rotateSpeedFactor <= 1) && (_rotateSpeedFactor >= 0.9)) {
			//			Debug.Log ("stage6, the rotateSpeedFactor is" + _rotateSpeedFactor);
			SprayManager.Instance.stage6 ();
		}

		else if(_rotateSpeedFactor < 0.01){
			//			Debug.Log("disable the spray, the rotateSpeedFactor is" + _rotateSpeedFactor);
			SprayManager.Instance.stage0();

		}

	}

	private void Rotate (float deltaTime) {
		_rotateSpeedFactor = Mathf.Lerp(_rotateSpeedFactor, PowerPercent, deltaTime);

		CacheTF.Rotate (0f, 0f, _rotateSpeed * _rotateSpeedFactor);

	}

	private void ReducePower (float deltaTime) 
	{
		CurPower = Mathf.Max(CurPower - _reducePowerPerSecond * deltaTime, 0);
	}

	private void SetPowerSlider () {
		_powerSlider.value = PowerPercent;
		_curPowerText.text = "Speed: "+ (Mathf.CeilToInt(_curPower)).ToString ();
	}

	private void AddWater (float deltaTime) {

		if (PowerPercent <= 0f)
			return;

		float times = 1 + GetAddEnergyPercent (PowerPercent);
		// Debug.Log("Add Water Energy Times: " + times);
		float energy = WaterEnergyPerSecond * times * deltaTime;
		// Debug.Log("Add Water Energy: " + energy);
		EnergyManager.Instance.WaterEnergy += energy;
	}

	#endregion

	private void OnMouseDown() {

		if (EventSystem.current.IsPointerOverGameObject()) {
			return;
		}

		switch (GameManager.Instance.State) {
		case EnumGameState.WaterWheel:
			AddPower ();
			break;
		}

	}

	private void AddPower () {
		float percent = GetAddPowerPercent (PowerPercent);
		int power = Mathf.RoundToInt(Capacity * percent);
		power = Mathf.Max (_minAddPowerValue, power);
		CurPower = Mathf.Min(power + CurPower, Capacity);
	}

	private float GetAddPowerPercent (float percent) {
		int length = _powerStatus.Length;
		for (int i = 0; i < length; i++) {
			WaterWheelStatus status = _powerStatus [i];

			if (status.InRange (percent)) {
				return status.Percent;
			}

		}

		return 0f;
	}

	private float GetAddEnergyPercent (float percent) {
		int length = _energyStatus.Length;
		for (int i = 0; i < length; i++) {
			WaterWheelStatus status = _energyStatus [i];

			if (status.InRange (percent)) {
				return status.Percent;
			}

		}

		return 0f;
	}

	#region Upgrade

	public void UpgradeCapacity () {
		CapacityLevel++;
		DataManager.WaterWheelCapacityLevel = CapacityLevel;
        Capacity += Config.WaterWheelCapacityUpgradeAbilityPerLevel;
		_capacityText.text = Capacity.ToString ();
	}

	public void UpgradeEnergy () {
		EnergyLevel++;
		DataManager.WaterWheelEnergyLevel = EnergyLevel;
        WaterEnergyPerSecond += Config.WaterWheelWaterEnergyUpgradeAbilityPerLevel;
	}

	#endregion

	#region Events

	private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
		// Debug.Log("WaterWheel-TimeSpan: " + timeSpan);

		float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.WaterWheelOfflineSecondsLimit);
		// Debug.Log("WaterWheel-TimeSpan-Seconds: " + seconds);
		float power = _curPower;
		// Debug.Log("WaterWheel-CurPower: " + power);
		float powerTotalSeconds = power / _reducePowerPerSecond;
		// Debug.Log("WaterWheel-PowerTotalSeconds: " + powerTotalSeconds);
		// Debug.Log("WaterEnergyPerSecond: " + WaterEnergyPerSecond);
		float energy = WaterEnergyPerSecond * seconds;
		if (seconds > powerTotalSeconds) {
			energy = WaterEnergyPerSecond * powerTotalSeconds;
			power = 0f;
		} else {
			energy = WaterEnergyPerSecond * seconds;
			power -= _reducePowerPerSecond * seconds;
		}
		// Debug.Log("WaterWheel-TimeSpan-AddEnergy: " + energy);
		// Debug.Log("WaterWheel-Remaining-Power: " + power);

		CurPower = power;
		EnergyManager.Instance.WaterEnergy += energy;
	}

	#endregion

}
