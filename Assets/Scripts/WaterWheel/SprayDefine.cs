﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayDefine : MonoBehaviour {

	private ParticleSystem ps;

	void Start(){
		ps = GetComponent<ParticleSystem> ();
		ps.Stop();
		var emission = ps.emission;
		var main = ps.main;

		emission.enabled = true;

	}

	public void stage1(){
//		this.gameObject.SetActive (true);
		var _main = ps.main;
		var _startLifetime = _main.startLifetime;
		var _startSize = _main.startSize;

		//		_main.duration = 5.0f;
		_startLifetime.constantMin = 0f;
		_startLifetime.constantMax = 12f;
		_main.startSpeed = 10.0f;
		_startSize.constantMin = 1.0f;
		_startSize.constantMax = 5.0f;
		_main.maxParticles = 100000;

		var _emission = ps.emission;
		_emission.enabled = true;
		_emission.rateOverTime = 5.0f;
		_emission.SetBursts (new ParticleSystem.Burst[] {new ParticleSystem.Burst(0.0f, 0, 1)});

		var _shape = ps.shape;
		_shape.angle = 5.0f;
		_shape.radius = 0.5f;

		ps.Play ();
//		Debug.Log ("right in stage 1");
	}

	public void stage2(){
		this.gameObject.SetActive (true);
		var _main = ps.main;
		var _startLifetime = _main.startLifetime;
		var _startSize = _main.startSize;

		//		_main.duration = 5.0f;
		_startLifetime.constantMin = 0;
		_startLifetime.constantMax = 12;
		_main.startSpeed = 10.0f;
		_startSize.constantMin = 5.0f;
		_startSize.constantMax = 6.0f;
		_main.maxParticles = 1000000;

		var _emission = ps.emission;
		_emission.enabled = true;
		_emission.rateOverTime = 10.0f;
		_emission.SetBursts (new ParticleSystem.Burst[] {new ParticleSystem.Burst(0.0f, 0, 5)});

		var _shape = ps.shape;
		_shape.angle = 6.0f;
		_shape.radius = 1.0f;

		ps.Play ();

//		Debug.Log ("right in stage 2");
	}


	public void stage3(){

		this.gameObject.SetActive (true);
		var _main = ps.main;
		var _startLifetime = _main.startLifetime;
		var _startSize = _main.startSize;

		//		_main.duration = 5.0f;
		_startLifetime.constantMin = 0;
		_startLifetime.constantMax = 12;
		_main.startSpeed = 10.0f;
		_startSize.constantMin = 5.0f;
		_startSize.constantMax = 12.0f;
		_main.maxParticles = 1000000;

		var _emission = ps.emission;
		_emission.enabled = true;
		_emission.rateOverTime = 30.0f;
		_emission.SetBursts (new ParticleSystem.Burst[] {new ParticleSystem.Burst(0.0f, 0, 20)});

		var _shape = ps.shape;
		_shape.angle = 7.0f;
		_shape.radius = 1.0f;

		ps.Play ();

//		Debug.Log ("right in stage 3");
	}


	public void stage4(){
		this.gameObject.SetActive (true);
		var _main = ps.main;
		var _startLifetime = _main.startLifetime;
		var _startSize = _main.startSize;

		//		_main.duration = 5.0f;
		_startLifetime.constantMin = 0;
		_startLifetime.constantMax = 12;
		_main.startSpeed = 10.0f;
		_startSize.constantMin = 10.0f;
		_startSize.constantMax = 12.0f;
		_main.maxParticles = 1000000;

		var _emission = ps.emission;
		_emission.enabled = true;
		_emission.rateOverTime = 30.0f;
		_emission.SetBursts (new ParticleSystem.Burst[] {new ParticleSystem.Burst(0.0f, 10, 30)});

		var _shape = ps.shape;
		_shape.angle = 8.0f;
		_shape.radius = 3.67f;

		ps.Play ();

//		Debug.Log ("right in stage 4");
	}

	public void stage5(){
		this.gameObject.SetActive (true);
		var _main = ps.main;
		var _startLifetime = _main.startLifetime;
		var _startSize = _main.startSize;

		//		_main.duration = 5.0f;
		_startLifetime.constantMin = 0;
		_startLifetime.constantMax = 12;
		_main.startSpeed = 10.0f;
		_startSize.constantMin = 12.0f;
		_startSize.constantMax = 20.0f;
		_main.maxParticles = 1000000;

		var _emission = ps.emission;
		_emission.enabled = true;
		_emission.rateOverTime = 40.0f;
		_emission.SetBursts (new ParticleSystem.Burst[] {new ParticleSystem.Burst(0.0f, 20, 30)});

		var _shape = ps.shape;
		_shape.angle = 10.0f;
		_shape.radius = 2.0f;

		ps.Play ();

//		Debug.Log ("right in stage 5");
	}


	public void stage6(){

//		this.gameObject.SetActive (true);
		var _main = ps.main;
		var _startLifetime = _main.startLifetime;
		var _startSize = _main.startSize;

		//		_main.duration = 5.0f;
		_startLifetime.constantMin = 0;
		_startLifetime.constantMax = 5;
		_main.startSpeed = 10.0f;
		_startSize.constantMin = 12f;
		_startSize.constantMax = 30f;
		_main.maxParticles = 1000000;

		var _emission = ps.emission;
		_emission.enabled = true;
		_emission.rateOverTime = 50.0f;
		_emission.SetBursts (new ParticleSystem.Burst[] {new ParticleSystem.Burst(0.0f, 30, 50)});

		var _shape = ps.shape;
		_shape.angle = 10.0f;
		_shape.radius = 5.0f;

		ps.Play ();

//		Debug.Log ("right in stage 6");
	}


	public void stage0(){

		ps.Stop ();
//		this.gameObject.SetActive (false);
//		Debug.Log ("right in stage end");

	}
}
