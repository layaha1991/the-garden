﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class SakuraGroup : MonoBehaviour {

	[SerializeField] private GameObject _EnergySoundEffect;
    [SerializeField] private int _id;
    [SerializeField] private ParticleSystem _plantEnergyParticle;
    [SerializeField] private Animator _flySakuraAni;


    public int ID { get { return _id; } }
    // 是否可收獲
    public bool CanCrop { get { return _plantEnergy > 0; } }
    // 能量庫存
    public int PlantEnergy {
        get { return _plantEnergy; }
        private set {
            _plantEnergy = Mathf.Min(value, SakuraManager.Instance.PlantEnergyLimit);

            CheckPlantEnergyParticle ();
            
            DataManager.SetSakuraPlantEnergy (_id - 1, _plantEnergy);
        }

    }

    private int _plantEnergy = 0;       // 能量庫存

    public void Setup () {
        // 獲取是否解鎖
        bool unlock = DataManager.SakuraIsUnlock (_id);

        // 如果已解鎖, 則顯示出來
        this.gameObject.SetActive (unlock);

        PlantEnergy = DataManager.GetSakuraPlantEnergy (_id - 1);

        CheckPlantEnergyParticle ();
    }

    public void CheckPlantEnergyParticle () {
        SetPlantEnergy (CanCrop);
    }

    private void SetPlantEnergy (bool active) {

        if (active) {
            _plantEnergyParticle.Play ();
        } else {
            _plantEnergyParticle.Stop ();
        }

    }

    public void AddEnergy (int energy) {
        PlantEnergy += energy;
    }

    // 收成
    public void Crop () {

        if (!CanCrop)
            return;
        EnergyManager.Instance.plantEnergyCrop[4] = true;
        EnergyManager.Instance.plantEnergyIncreased[4] = PlantEnergy;
        EnergyManager.Instance.PlantEnergy += PlantEnergy;
        PlantEnergy = 0;
        StartCoroutine(playFlySakuraAni());
    

    }
    private IEnumerator playFlySakuraAni()
    {
        _flySakuraAni.gameObject.SetActive(true);
        _flySakuraAni.Play("flyTree");
        yield return new WaitForSeconds(6f);
        _flySakuraAni.gameObject.SetActive(false);
        StopCoroutine(playFlySakuraAni());
    }


    #region Grow

        public void Grow (Action callback = null) {
            this.gameObject.SetActive (true);
            this.transform.localScale = Vector3.zero;

            SimpleAnimtor.Instance.PlayGrowAnim (this.transform, 0.1f, ()=> {
                CheckPlantEnergyParticle ();

                if (callback != null) {
                    callback ();
                }

            });

        }

    #endregion

    private void OnMouseDown() {

        if (EventSystem.current.IsPointerOverGameObject ()) {
            return;
        }

        // Debug.Log(this.name + " on mouse down.");
        Crop ();

        AudioManager.Instance.RandomPlay();
    }

}
