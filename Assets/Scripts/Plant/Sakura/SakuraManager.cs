﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SakuraManager : Singleton<SakuraManager> {

    [SerializeField] private SakuraGroup[] _sakuras;

    [Header ("櫻花收成")]
    [SerializeField] private PlantCropInfo _cropInfo;

    public int SakuraLevel {
        get;
        private set;
    }

    // 當前增加的能量
    private int CurAddEnergy {

        get {

            if (SakuraLevel <= 0)
                return 0;

            return (int)(_cropInfo.CropDefaultEnergy * Mathf.Pow (_cropInfo.CropUpEnergyPerStage, SakuraLevel - 1));
        }

    }
    // 庫存的能量上限
    public int PlantEnergyLimit { get { return CurAddEnergy * 240; } }

    private float _cropTime = 0f;

    protected override void OnInit() {
        OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

        SakuraLevel = DataManager.SakuraLevel;

        int length = _sakuras.Length;
        for (int i = 0; i < length; i++) {
            _sakuras [i].Setup ();
        }

    }

    public void UnlockSakura () {
        SakuraLevel ++;
        DataManager.SakuraLevel = SakuraLevel;

        int index = SakuraLevel - 1;

        SakuraGroup sakura = GetSakura (SakuraLevel);
        if (sakura != null) {
            CameraManager.Instance.SetWatch (CameraDefine.POS_SAKURA [index], CameraDefine.ROT_SAKURA [index]);
            GameManager.Instance.State = EnumGameState.Watch;

            sakura.Grow ( ()=> {
                GameManager.Instance.State = EnumGameState.Garden;
            });

        }

    }

    private SakuraGroup GetSakura (int id) {
        int length = _sakuras.Length;
        for (int i = 0; i < length; i++) {
            SakuraGroup sakura = _sakuras [i];
            if (sakura.ID == id) {
                return sakura;
            }

        }

        return null;
    }

    private void Update () {
        EnergyCounDown (Time.deltaTime);
    }

    private void EnergyCounDown (float deltaTime) {
        
        if (SakuraLevel <= 0) 
            return;

        _cropTime += deltaTime;

        if (_cropTime >= _cropInfo.CropSeconds) {
            _cropTime = 0f;
            AddEnergy ();
        }

    }

    private void AddEnergy () {
        int length = _sakuras.Length;
        for (int i = 0; i < length; i++) {
            _sakuras [i].AddEnergy (CurAddEnergy);   
        }

    }

    #region Events

        private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
            float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.PlantOfflineSecondsLimit);
            // Debug.Log("Seconds: " + seconds);
            int times = (int)(seconds / _cropInfo.CropSeconds);
            // Debug.Log("Times: " + times);
            int energy = CurAddEnergy * times;
            // Debug.Log("Energy: " + energy);

            int length = _sakuras.Length;
            for (int i = 0; i < length; i++) {
                _sakuras [i].AddEnergy (energy);
            }

        }

    #endregion

}
