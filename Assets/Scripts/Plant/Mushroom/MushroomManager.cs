﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomManager : Singleton<MushroomManager> {

    [SerializeField] private MushroomGroup[] _mushrooms;

    [Header ("蘑菇收成")]
    [SerializeField] private PlantCropInfo _cropInfo;

    // 蘑菇等級
    public int MushroomLevel {
        get;
        private set;
    }
    // 蘑菇收成資料
    public PlantCropInfo CropInfo { get { return _cropInfo; } }
    // 庫存的能量上限
    public int PlantEnergyLimit { get { return CurAddEnergy * 240; } }
    // 當前增加的能量
    private int CurAddEnergy {

        get {

            if (MushroomLevel <= 0)
                return 0;

            return (int)(MushroomManager.Instance.CropInfo.CropDefaultEnergy * Mathf.Pow (MushroomManager.Instance.CropInfo.CropUpEnergyPerStage, MushroomLevel - 1));
        }

    }

    private float _cropTime = 0f;

    protected override void OnInit() {
        OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

        MushroomLevel = DataManager.MushroomLevel;

        int length = _mushrooms.Length;
        for (int i = 0; i < length; i++) {
            _mushrooms [i].Setup ();
        }

    }

    public void UnlockMushroom () {
        MushroomLevel ++;
        DataManager.MushroomLevel = MushroomLevel;
    
        int index = MushroomLevel - 1;

        MushroomGroup mushroom = GetMushroom (MushroomLevel);
        if (mushroom != null) {
            CameraManager.Instance.SetWatch (CameraDefine.POS_MUSHROOM [index], CameraDefine.ROT_MUSHROOM [index]);
            GameManager.Instance.State = EnumGameState.Watch;

            mushroom.Grow ( ()=> {
                GameManager.Instance.State = EnumGameState.Garden;
            });

        }

    }

    private MushroomGroup GetMushroom (int id) {
        int length = _mushrooms.Length;
        for (int i = 0; i < length; i++) {
            MushroomGroup mushroomGroup = _mushrooms [i];
            if (mushroomGroup.ID == id) {
                return mushroomGroup;
            }

        }

        return null;
    }

    private void Update () {
        EnergyCounDown (Time.deltaTime);
    }

    private void EnergyCounDown (float deltaTime) {
        
        if (MushroomLevel <= 0) 
            return;

        _cropTime += deltaTime;

        if (_cropTime >= _cropInfo.CropSeconds) {
            _cropTime = 0f;
            AddEnergy ();
        }

    }

    private void AddEnergy () {
        int length = _mushrooms.Length;
        for (int i = 0; i < length; i++) {
            _mushrooms [i].AddEnergy (CurAddEnergy);   
        }

    }

    #region Events

        private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
            float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.PlantOfflineSecondsLimit);
            // Debug.Log("Seconds: " + seconds);
            int times = (int)(seconds / _cropInfo.CropSeconds);
            // Debug.Log("Times: " + times);
            int energy = CurAddEnergy * times;
            // Debug.Log("Energy: " + energy);

            int length = _mushrooms.Length;
            for (int i = 0; i < length; i++) {
                _mushrooms [i].AddEnergy (energy); 
            }
             
        }

    #endregion

}
