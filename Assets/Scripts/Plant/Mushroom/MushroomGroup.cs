﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class MushroomGroup : MonoBehaviour {

	[SerializeField] private GameObject _EnergySoundEffect;
    [SerializeField] private int _id;
    [SerializeField] private ParticleSystem _plantEnergyParticle;
    [SerializeField] private Animator _mushroomFly;


    public int ID { get { return _id; } }
    // 是否可收獲
    public bool CanCrop { get { return _plantEnergy > 0; } }
    // 能量庫存
    public int PlantEnergy {
        get { return _plantEnergy; }
        private set {
            _plantEnergy = Mathf.Min(value, MushroomManager.Instance.PlantEnergyLimit);

            CheckPlantEnergyParticle ();
            
            DataManager.SetMushroomPlantEnergy (_id - 1, _plantEnergy);
        }

    }

    private int _plantEnergy = 0;       // 能量庫存

    public void Setup () {
        // 獲取是否解鎖
        bool unlock = DataManager.MushroomIsUnlock (_id);

        // 如果已解鎖, 則顯示出來
        this.gameObject.SetActive (unlock);

        PlantEnergy = DataManager.GetMushroomPlantEnergy (_id - 1);
        CheckPlantEnergyParticle ();
    }

    public void CheckPlantEnergyParticle () {
        SetPlantEnergy (CanCrop);
    }

    private void SetPlantEnergy (bool active) {

        if (active) {
            _plantEnergyParticle.Play ();
        } else {
            _plantEnergyParticle.Stop ();
        }

    }

    public void AddEnergy (int energy) {
        // Debug.Log(this.name + ", Level: " + MushroomLevel);
        // Debug.Log(this.name + ", CurAddEnergy: " + CurAddEnergy);

        PlantEnergy += energy;
    }

    // 收成
    public void Crop () 
    {

        if (!CanCrop)
            return;
        EnergyManager.Instance.plantEnergyCrop[1] = true;
        EnergyManager.Instance.plantEnergyIncreased[1] = PlantEnergy;
        EnergyManager.Instance.PlantEnergy += PlantEnergy;
        PlantEnergy = 0;
        StartCoroutine(playMushroomFlyAni());
      
    }



private IEnumerator playMushroomFlyAni()
{
        _mushroomFly.gameObject.SetActive(true);
        _mushroomFly.Play("flyMushroom");
        yield return new WaitForSeconds(6.5f);
        _mushroomFly.gameObject.SetActive(false);
        StopCoroutine(playMushroomFlyAni());

}



    #region Grow

        public void Grow (Action callback = null) {
            this.gameObject.SetActive (true);
            this.transform.localScale = Vector3.zero;

            SimpleAnimtor.Instance.PlayGrowAnim (this.transform, 0.1f, ()=> {
                CheckPlantEnergyParticle ();

                if (callback != null) {
                    callback ();
                }

            });

        }

    #endregion
    
    private void OnMouseDown() {

        if (EventSystem.current.IsPointerOverGameObject ()) {
            return;
        }

        // Debug.Log(this.name + " on mouse down.");
        Crop ();

        AudioManager.Instance.RandomPlay();
    }

}
