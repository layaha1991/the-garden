﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FlowerGroup : MonoBehaviour {

    private bool _isGrew = false;
    
    public bool IsGrew { get { return _isGrew; } }

    public void Setup (bool isGrew) {
        _isGrew = isGrew;
        this.gameObject.SetActive (isGrew);
    }

    public void Grow (Action callback) {
        _isGrew = true;
        this.gameObject.SetActive (true);
        this.transform.localScale = Vector3.zero;

        SimpleAnimtor.Instance.PlayGrowAnim (this.transform, 0.2f, callback);
    }

}
