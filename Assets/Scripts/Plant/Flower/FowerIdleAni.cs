﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FowerIdleAni : MonoBehaviour {

    private float t;
    private float CD;
    private Quaternion destination;

    private void Start()
    {
        CD = Random.Range(1,3);
        destination = Quaternion.Euler(0, 0, (Random.Range (-25,25)));
    }
    void Update () 
    {
        randomMovement();

	}

    private void randomMovement()
    {
        t += Time.deltaTime;
        if (t > CD) 
        {
       
            transform.rotation = Quaternion.Slerp (transform.rotation, destination, 1f *Time.deltaTime); 
            // reset the random number
           

            if(transform.rotation == destination)
            {
                destination = Quaternion.Euler(0, 0, (Random.Range(-20, 20)));
                CD = Random.Range(1,3);
                t = 0f;
            }
           

        }

    }
}
