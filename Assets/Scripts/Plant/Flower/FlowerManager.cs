﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerManager : Singleton<FlowerManager>
{

    [SerializeField] private FlowerGroup[] _flowers;                // 花堆
    [SerializeField] private int[] _growStageStatus;    // 花堆生長等級
    private float t; // count (finish the temple animation first


    protected override void OnInit()
    {
        int stage = DataManager.Stage;

        SetupFlower(stage);

        LevelManager.Instance.StageChanged += OnStageChanged;
    }

    private void SetupFlower(int stage)
    {
        // Debug.Log("Stage: " + stage);
        int length = _growStageStatus.Length;
        for (int i = 0; i < length; i++)
        {
            int growStage = _growStageStatus[i];
            // Debug.Log("GrowStage: " + growStage);
            if (growStage <= stage)
            {
                _flowers[i].Setup(true);
            }
            else
            {
                _flowers[i].Setup(false);
            }

        }

    }

    private void GrowFlower(int stage)
    {
        int length = _growStageStatus.Length;
        for (int i = 0; i < length; i++)
        {
            int growStage = _growStageStatus[i];
            FlowerGroup flower = _flowers[i];

            if (growStage <= stage && !flower.IsGrew)
            {
                Vector3 pos = CameraDefine.POS_FLOWER[i];
                Quaternion rot = CameraDefine.ROT_FLOWER[i];
                CameraManager.Instance.SetWatch(pos, rot);
                //GameManager.Instance.State = EnumGameState.Watch;
                _flowers[i].Grow(() =>
                {
                    //GameManager.Instance.State = EnumGameState.Main;
                });


            }

        }
    }

        #region Events

        private void OnStageChanged(int stage)
        {
            StartCoroutine(growFlowerRoutine(stage));
        }
        private IEnumerator growFlowerRoutine(int stage)

        {
            GrowFlower(stage);
            yield return null;
            StopCoroutine(growFlowerRoutine(stage));
            
        }

        #endregion

    }

