﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReedIdleAni : MonoBehaviour 
{
    
    private float t;
    private float CD;
    private Quaternion destination;
    public float speed;
    public float moveRange;

    private void Start()
    {
        CD = Random.Range(1,3);
        destination = Quaternion.Euler(0, 0, Random.Range(-moveRange , moveRange));
    }
    void Update()
    {
        randomMovement();

    }

    private void randomMovement()
    {
        t += Time.deltaTime;
        if (t > CD)
        {

            transform.rotation = Quaternion.Slerp(transform.rotation, destination, speed* Time.deltaTime);
            // reset the random number


            if (t > CD+1)
            {
                destination = Quaternion.Euler(0, 0, (Random.Range(-(moveRange), moveRange)));
                CD = Random.Range(1,3);
                t = 0f;
            }


        }

    }
}

