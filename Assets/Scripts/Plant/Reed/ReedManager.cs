﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ReedManager : Singleton<ReedManager> {
    
    [SerializeField] private ReedGroup[] _reedGroups;

    [Header ("蘆葦收成")]
    [SerializeField] private PlantCropInfo _cropInfo;

    private float _cropTime = 0f;

    public int ReedLevel {
        get;
        private set;
    }

    // 庫存的能量上限
    public int PlantEnergyLimit { get { return CurAddEnergy * 240; } }
    // 當前增加的能量
    private int CurAddEnergy {

        get {

            if (ReedLevel <= 0)
                return 0;

            return (int)(_cropInfo.CropDefaultEnergy * Mathf.Pow (_cropInfo.CropUpEnergyPerStage, ReedLevel - 1));
        }

    }

    protected override void OnInit() {
        OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

        ReedLevel = DataManager.ReedLevel;

        int length = _reedGroups.Length;
        for (int i = 0 ; i < length; i++) {
            ReedGroup reedGroup = _reedGroups [i];
            reedGroup.Setup ();
        }

    }

    private void Update () {
        EnergyCounDown (Time.deltaTime);
    }

    private void EnergyCounDown (float deltaTime) {
        
        if (ReedLevel <= 0) 
            return;

        _cropTime += deltaTime;

        if (_cropTime >= _cropInfo.CropSeconds) {
            _cropTime = 0f;
            AddEnergy ();
        }

    }

    private void AddEnergy () {
        int length = _reedGroups.Length;
        for (int i = 0; i < length; i++) {
            _reedGroups [i].AddEnergy (CurAddEnergy);
        }

    }

    public void UnlockReed () {
        ReedLevel++;
        DataManager.ReedLevel = ReedLevel;

        int index;
        Reed reed = GetReed (ReedLevel, out index);

        CameraManager.Instance.SetWatch (CameraDefine.POS_REED [index], CameraDefine.ROT_REED [index]);
        GameManager.Instance.State = EnumGameState.Watch;

        reed.Grow (()=> {
            GameManager.Instance.State = EnumGameState.Garden;
        });
    }

    private Reed GetReed (int id, out int index) {
        index = -1;

        int length = _reedGroups.Length;
        for (int i = 0; i < length; i++) {
            index = i;
            ReedGroup group = _reedGroups [i];
            Reed reed = group.GetReed (id);
            if (reed != null) {
                return reed;
            }

        }

        return null;
    }

    #region Events

        private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
            float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.PlantOfflineSecondsLimit);
            // Debug.Log("Seconds: " + seconds);
            int times = (int)(seconds / _cropInfo.CropSeconds);
            // Debug.Log("Times: " + times);
            int energy = CurAddEnergy * times;

            int length = _reedGroups.Length;
            for (int i = 0; i < length; i++) {
                _reedGroups [i].AddEnergy (energy);
            }
            
        }

    #endregion

}
