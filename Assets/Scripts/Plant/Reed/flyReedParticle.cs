﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyReedParticle: MonoBehaviour {

    [SerializeField] private Transform collectPowerPoint;

	void Update () {
        transform.position = Vector3.Slerp (transform.position, collectPowerPoint.transform.position, 5f * Time.deltaTime);
	}
}
