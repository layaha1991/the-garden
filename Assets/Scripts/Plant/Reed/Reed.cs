﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Reed : MonoBehaviour
{
	[SerializeField] private GameObject _EnergySoundEffect;
    [SerializeField] private int _id;
    [SerializeField] private ParticleSystem _plantEnergyParticle;
    [SerializeField] private ReedGroup _group;
    [SerializeField] private Animator flyReedPE;

    public int ID { get { return _id; } }

    public void Setup()
    {
        bool unlock = DataManager.ReedIsUnlock(_id);
        this.gameObject.SetActive(unlock);
        CheckPlantEnergyParticle();
    }

    public void CheckPlantEnergyParticle()
    {
        SetPlantEnergy(_group.CanCrop);
    }

    private void SetPlantEnergy(bool active)
    {

        if (active)
        {
            _plantEnergyParticle.Play();
            //Instantiate(_flyReedParticle, this.transform.localPosition, Quaternion.identity);

        }
        else
        {
            _plantEnergyParticle.Stop();
        }

    }

    public void Grow(Action callback = null)
    {
        this.gameObject.SetActive(true);
        this.transform.localScale = Vector3.zero;

        SimpleAnimtor.Instance.PlayGrowAnim(this.transform, 0f, () =>
        {
            CheckPlantEnergyParticle();

            if (callback != null)
            {
                callback();
            }

        });

    }

    private void OnMouseDown()
    {

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        // Debug.Log(this.name + " on mouse down.");
        _group.Crop();

        AudioManager.Instance.RandomPlay();
    }
}
