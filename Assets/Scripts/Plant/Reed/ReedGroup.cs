﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReedGroup : MonoBehaviour {

    [SerializeField] private Reed[] _reeds;
    [SerializeField] private int _id;
    [SerializeField] private Animator _flyPE;

    public int ID { get { return _id; } }

    // 能量庫存
    private int _plantEnergy;
    public int PlantEnergy {
        get { return _plantEnergy; }
        private set {
            _plantEnergy = Mathf.Min (value, ReedManager.Instance.PlantEnergyLimit);
            
            int length = _reeds.Length;
            for (int i = 0; i < length; i++) {
                _reeds [i].CheckPlantEnergyParticle ();
            }

            DataManager.SetReedPlantEnergy (_id, _plantEnergy);
        }

    }

    // 是否可以收獲
    public bool CanCrop { get { return _plantEnergy > 0; } }

    public void Setup () {
        int length = _reeds.Length;
        for (int i = 0 ; i < length; i++) {
            Reed reed = _reeds [i];
            reed.Setup ();
        }

        PlantEnergy = DataManager.GetReedPlantEnergy (_id);
    }

    public Reed GetReed (int id) {
        int length = _reeds.Length;
        for (int i = 0; i < length; i ++) {
            Reed reed = _reeds [i];

            if (reed.ID == id) {
                return reed;
            }

        }

        return null;
    }

    public void AddEnergy (int energy) {
        // Debug.Log(this.name + ", ReedLevel: " + ReedLevel);
        // Debug.Log(this.name + ", CurAddEnergy: " + CurAddEnergy);

        PlantEnergy += energy;
    }

    // 收成
    public void Crop () {

        if (!CanCrop)
            return;
        EnergyManager.Instance.plantEnergyCrop[2] = true;
        EnergyManager.Instance.plantEnergyIncreased[2] = PlantEnergy;
        EnergyManager.Instance.PlantEnergy += PlantEnergy;
        PlantEnergy = 0;
        StartCoroutine(playReedFlyAni());
      

    }

    private IEnumerator playReedFlyAni()
    {
        _flyPE.gameObject.SetActive(true);
        _flyPE.Play("flyReed");
        yield return new WaitForSeconds(6f);
        _flyPE.gameObject.SetActive(false);
        StopCoroutine(playReedFlyAni());
    }

}
