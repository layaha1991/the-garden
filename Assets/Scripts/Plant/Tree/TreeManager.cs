﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TreeManager : Singleton<TreeManager> {

    [SerializeField] private TreeGroup[] _trees;

    [Header ("樹收成")]
    [SerializeField] private float _cropSeconds = 60f;
    [SerializeField] private int[] _cropEnergy = new int[3] { 20, 90, 180 };

    public int TreeLevel {
        get;
        private set;
    }

    // 當前增加的能量
    private int CurAddEnergy {

        get {

            if (TreeLevel <= 0)
                return 0;

            return _cropEnergy [TreeLevel - 1];
        }

    }
    // 庫存的能量上限
    public int PlantEnergyLimit { get { return CurAddEnergy * 240; } }

    private float _cropTime = 0f;

    protected override void OnInit() {
        OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

        TreeLevel = DataManager.TreeLevel;

        int length = _trees.Length;
        for (int i = 0; i < length; i++) {
            _trees [i].Setup ();
        }

    }

    public void UnlockTree () {
        TreeLevel ++;
        DataManager.TreeLevel = TreeLevel;

        int index = TreeLevel - 1;

        TreeGroup tree = GetTree (TreeLevel);
        if (tree != null) {
            CameraManager.Instance.SetWatch (CameraDefine.POS_TREE [0], CameraDefine.ROT_TREE [0]);
            GameManager.Instance.State = EnumGameState.Watch;

            tree.Grow ( ()=> {
                GameManager.Instance.State = EnumGameState.Garden;
            });

        }

    }

    private TreeGroup GetTree (int id) {
        int length = _trees.Length;
        for (int i = 0; i < length; i++) {
            TreeGroup tree = _trees [i];
            if (tree.ID == id) {
                return tree;
            }

        }

        return null;
    }

    private void Update () {
        EnergyCounDown (Time.deltaTime);
    }

    private void EnergyCounDown (float deltaTime) {
        
        if (TreeLevel <= 0) 
            return;

        _cropTime += deltaTime;

        if (_cropTime >= _cropSeconds) {
            _cropTime = 0f;
            AddEnergy ();
        }

    }

    private void AddEnergy () {
        int length = _trees.Length;
        for (int i = 0; i < length; i++) {
            _trees [i].AddEnergy (CurAddEnergy);   
        }

    }

    #region Events

        private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
            float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.PlantOfflineSecondsLimit);
            // Debug.Log("Seconds: " + seconds);
            int times = (int)(seconds / _cropSeconds);
            // Debug.Log("Times: " + times);
            int energy = CurAddEnergy * times;
            // Debug.Log("Energy: " + energy);

            int length = _trees.Length;
            for (int i = 0; i < length; i++) {
                _trees [i].AddEnergy (energy);
            }

        }

    #endregion

}
