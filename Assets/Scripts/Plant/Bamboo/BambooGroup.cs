﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class BambooGroup : MonoBehaviour {

	[SerializeField] private GameObject _EnergySoundEffect;
    [SerializeField] private int _id;
    [SerializeField] private ParticleSystem _plantEnergyParticle;
    private int randomNum;


    public int ID { get { return _id; } }

    public void Setup () {
        // 獲取是否解鎖
        bool unlock = DataManager.BambooIsUnlock (_id);

        // 如果已解鎖, 則顯示出來
        this.gameObject.SetActive (unlock);

        CheckPlantEnergyParticle ();
    }

    #region Grow

        public void Grow (Action callback = null) {
            this.gameObject.SetActive (true);
            this.transform.localScale = Vector3.zero;

            SimpleAnimtor.Instance.PlayGrowAnim (this.transform, 0.1f, ()=> {
                CheckPlantEnergyParticle ();

                if (callback != null) {
                    callback ();
                }

            });

        }

    #endregion

    public void CheckPlantEnergyParticle () {
        SetPlantEnergy (BambooManager.Instance.CanCrop);
    }

    private void SetPlantEnergy (bool active) {

        if (active) {
            _plantEnergyParticle.Play ();
        } else {
            _plantEnergyParticle.Stop ();
        }

    }
    
    private void OnMouseDown() 
    {

        if (EventSystem.current.IsPointerOverGameObject ()) 
        {
            return;
        }

        // Debug.Log(this.name + " on mouse down.");
        BambooManager.Instance.Crop ();

        AudioManager.Instance.RandomPlay();
		//_EnergySoundEffect.gameObject.GetComponent<energySoundEffect> ().randomPlay();
    }

}
