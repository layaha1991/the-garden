﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BambooManager : Singleton<BambooManager> {


    [SerializeField] private BambooGroup[] _bamboos;    // 竹林
    [SerializeField] private Animator _flyBambooParticle;

    [Header ("竹林收成")]
    [SerializeField] private PlantCropInfo _cropInfo;

    private float _cropTime = 0f;       // 竹林收成經過時間
    private int _plantEnergy = 0;       // 能量庫存

    private bool _cropped;

    public int BambooLevel {
        get;
        private set;
    }
    // 能量庫存
    public int PlantEnergy {
        get { return _plantEnergy; }
        private set {
            _plantEnergy = Mathf.Min(value, PlantEnergyLimit);

            int length = _bamboos.Length;
            for (int i = 0; i < length; i++) {
                _bamboos [i].CheckPlantEnergyParticle ();       
            }
            
            DataManager.BambooPlantEnergy = _plantEnergy;
        }

    }
    // 保存的能量上限
    private int PlantEnergyLimit { get { return CurAddEnergy * 240; } }
    // 當前增加的能量
    private int CurAddEnergy { 
        get { 

            if (BambooLevel <= 0)
                return 0;    

            return (int)(_cropInfo.CropDefaultEnergy * Mathf.Pow (_cropInfo.CropUpEnergyPerStage, BambooLevel - 1)); 
        } 
    }
    // 是否可收獲
    public bool CanCrop { get { return _plantEnergy > 0; } }

    protected override void OnInit () {
        OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

        BambooLevel = DataManager.BambooLevel;
        PlantEnergy = DataManager.BambooPlantEnergy;

        int length = _bamboos.Length;
        for (int i = 0 ; i < length; i++) {
            BambooGroup bambooGroup = _bamboos [i];
            bambooGroup.Setup ();
        } 

    }

    public void UnlockBamboo () {
        BambooLevel++;
        DataManager.BambooLevel = BambooLevel;

        // 顯示對應的竹
        BambooGroup bambooGroup = GetBamboo (BambooLevel);
        if (bambooGroup != null) {
            CameraManager.Instance.SetWatch (CameraDefine.POS_BAMBOO, CameraDefine.ROT_BAMBOO);
            GameManager.Instance.State = EnumGameState.Watch;

            bambooGroup.Grow ( () => {
                GameManager.Instance.State = EnumGameState.Garden;
            });

        }

    }

    private BambooGroup GetBamboo (int id) {
        int length = _bamboos.Length;
        for (int i = 0 ; i < length; i++) {
            BambooGroup bambooGroup = _bamboos [i];

            if (bambooGroup.ID == id) {
                return bambooGroup;
            }

        }

        return null;
    }

    private void Update () {
        EnergyCountDown (Time.deltaTime);
    }

    private void EnergyCountDown (float deltaTime) {
        
        if (BambooLevel <= 0) 
            return;

        _cropTime += deltaTime;

        if (_cropTime >= _cropInfo.CropSeconds) {
            _cropTime = 0f;
            AddEnergy ();
        }

    }

    private void AddEnergy () {
        PlantEnergy += CurAddEnergy;
    }

    /// <Summary>
    /// 收成
    /// </Summary>
    public void Crop () {

        if (!CanCrop)
            return;
        EnergyManager.Instance.plantEnergyCrop[0] = true;
        EnergyManager.Instance.plantEnergyIncreased[0] = PlantEnergy;
        EnergyManager.Instance.PlantEnergy += PlantEnergy; 
        PlantEnergy = 0;
        StartCoroutine(playBambooFlyAni());


    
    }

    private IEnumerator playBambooFlyAni()
    {
        _flyBambooParticle.gameObject.SetActive(true);
        _flyBambooParticle.Play("flyBamboo");
        yield return new WaitForSeconds(6f);
        _flyBambooParticle.gameObject.SetActive(false);

    }

    #region Events

        private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
            // Debug.Log("Bamboo-TimeSpan: " + timeSpan);

            // 不能超过240分鐘
            float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.PlantOfflineSecondsLimit);
            // Debug.Log("Bamboo-TimeSpan-Seconds: " + seconds);
            int times = (int)(seconds / _cropInfo.CropSeconds);
            // Debug.Log("Bamboo-Times: " + times);
            // Debug.Log("Bamboo-CurAddEnergy: " + CurAddEnergy);
            int energy = CurAddEnergy * times;
            // Debug.Log("Bamboo-TimeSpan-AddEnergy: " + energy);

            PlantEnergy += energy;
            // EnergyManager.Instance.PlantEnergy += energy;
        }

    #endregion
}
