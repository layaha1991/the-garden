﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotusAnimation : MonoBehaviour
{


    private float t;
    private Vector3 destination;
    public float speed;
    private Vector3 originalPos;

    private float _moveRange;
    public float MoveRange
    {
        get 
        {
            return Random.Range ( - (_moveRange) , (_moveRange));   
        }
        set
        {
            _moveRange = value;

        }
    }

    private void Start()
    {
        destination = new Vector3 (MoveRange, 0 ,MoveRange);
        MoveRange = 100f;
        originalPos = transform.position;

    }
    void Update()
    {
        randomMovement();
    }

    private void randomMovement()
    {
        t += Time.deltaTime;
        if (t > 0 && t <15)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, destination, speed * Time.deltaTime);
        }

        if (t >= 15 && t < 30)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, originalPos, speed * Time.deltaTime);
        }
                
        if (t >= 30)
        {
            t = 0; 
            MoveRange = 100f; 
            destination = new Vector3(MoveRange, 0, MoveRange);

        }

    }
}
