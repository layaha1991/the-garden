﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

    private void Awake () {
        DataManager.Load ();

        Pond.Instance.Init ();
        EnergyManager.Instance.Init ();
        GameManager.Instance.Init ();
        WaterWheel.Instance.Init ();
        BambooManager.Instance.Init ();
        MushroomManager.Instance.Init ();
        ReedManager.Instance.Init ();
        TreeManager.Instance.Init ();
        SakuraManager.Instance.Init ();
        FlowerManager.Instance.Init ();
        LevelManager.Instance.Init();
        DistractionManager.Instance.Init();
    }

    private void Start () 
    {
        CameraManager.Instance.ArrivedEvents += ArrivedHandler;
        GameManager.Instance.State = EnumGameState.Main;
    }

    private void ArrivedHandler ()
    {

    }

}
