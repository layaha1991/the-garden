﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PondPoint : MonoBehaviour {

    [SerializeField] private PondPoint _prePoint;
    [SerializeField] private PondPoint _nextPoint;

    public Vector3 Position {
        get { return transform.position; }
    }

    public Vector3 PrePointPosition {
        get { return _prePoint.Position; }
    }

    public Vector3 NextPointPosition {
        get { return _nextPoint.Position; }
    }

    public PondPoint NextPoint {
        get { return _nextPoint; }
    }

    public PondPoint PrePoint {
        get { return _prePoint; }
    }

    #region Debug

        [Header ("Debug")]
        [SerializeField] private float _debug_radius = 5f;

        public void OnDrawGizmosSelected () {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere (Position, _debug_radius);
        }

    #endregion

}
