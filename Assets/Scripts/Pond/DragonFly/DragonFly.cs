﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DragonFly : MonoBehaviour {

    [SerializeField] private float _moveSpeed = 1f;
    [SerializeField] private float _rotateAngle = 5f;
    [SerializeField] private SpriteRenderer _body;
	[SerializeField] private GameObject _DragonFlyEnergyParticle;

    private PondPointGroup _path;
    private Vector3 _destination;

    private bool _inMoving = false;

    private void Start () {
        _destination = this.transform.position;
    }

    public void SetPath (PondPointGroup path) {
        _path = path;
    }   

    private void NextDestination () {
        _destination = _path.GetNextPoint ();

        Vector3 selfPos = transform.position;
        Vector3 dir = _destination;
        dir.x -= selfPos.x;
        dir.z = selfPos.z - _destination.z;
        float angle = Mathf.Atan2 (dir.z, dir.x) * Mathf.Rad2Deg - 90f;

        StartCoroutine (RotateToAngle (angle));
    }

    private IEnumerator RotateToAngle (float angle) {
        float t = 0f;
        float duration = 0.25f;
        Quaternion startRot = transform.localRotation;
        Quaternion endRot = Quaternion.Euler (0f, 0f, angle);

        while (t < duration) {
            t += Time.deltaTime;

            transform.localRotation = Quaternion.Slerp (startRot, endRot, t / duration);

            yield return null;
        }
        transform.localRotation = endRot;

        _inMoving = true;
    }

    private void Update () {

        if (!_inMoving)
            return;

        Move (Time.deltaTime);
    }

    private void Move (float deltaTime) {

        if (_path == null)
            return;

        transform.Translate (transform.forward * deltaTime * _moveSpeed, Space.Self);

        Vector2 pos = new Vector2 (transform.position.x, transform.position.z);
        Vector2 pos2 = new Vector2 (_destination.x, _destination.z);
        if (Vector3.Distance (pos, pos2) <= 20f) {
            _inMoving = false;
        }

    }

    private void Rotate () {
        transform.Rotate (0f, 0f, _rotateAngle * Time.deltaTime);
    }

    private void OnCollisionStay (Collision other) {
        // Rotate ();
    }

    public void Grow (Action callback = null) {
        this.transform.localScale = Vector3.zero;

        SimpleAnimtor.Instance.PlayPondGrowAnim (this.transform, _body, 0f, ()=>{

            if (callback != null) {
                callback ();
            }

        });

    }

	public void playDragonFlyEnergyParticle(){
		StartCoroutine (_playDragonFlyEnergyParticle());
	}

	private IEnumerator _playDragonFlyEnergyParticle(){
		_DragonFlyEnergyParticle.gameObject.SetActive (true);
		yield return new WaitForSeconds (6f);
		_DragonFlyEnergyParticle.gameObject.SetActive (false);
	}

    private void OnMouseDown () {

        if (GameManager.Instance.State != EnumGameState.Pond) {
            GameManager.Instance.State = EnumGameState.Pond;
            return;
        }

        if (_inMoving)
            return;

        NextDestination ();
    }

}
