﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonFlyManager : IPondManager {

    #region DragonFly list

        private List<DragonFly> _dragonflies = new List<DragonFly> ();

    #endregion

    public EnumPondType GetPondType () {
        return EnumPondType.DragonFly;
    }

    public int Level {
        get;
        private set;
    }

    public void Init () {
        Level = DataManager.DragonFlyLevel;

        CreateDragonFlies ();
    }
    
    #region Create

        private void CreateDragonFlies () {

            for (int i = 0; i < Level; i++) {
                DragonFly dragonfly = CreateDragonfly ();
                PondPointGroup path = Pond.Instance.GetPondPoints ();
                dragonfly.SetPath (path);
            }

        }

        private DragonFly CreateDragonfly () {
            PondSpawn spawn = Pond.Instance.GetRandomSpawn ();
            GameObject dragonfly = spawn.Create (GetPondType ());
            _dragonflies.Add (dragonfly.GetComponent<DragonFly> ());
            return dragonfly.GetComponent<DragonFly> ();
        }

    #endregion

    public void Unlock () {
        Level++;
        DataManager.DragonFlyLevel = Level;

        DragonFly dragonfly = CreateDragonfly ();

        Vector3 pos = dragonfly.transform.position;
        pos.y += 400f;
        CameraManager.Instance.SetWatch (pos);
        GameManager.Instance.State = EnumGameState.Watch;

        dragonfly.Grow (()=>{
            GameManager.Instance.State = EnumGameState.Pond;
            PondPointGroup path = Pond.Instance.GetPondPoints ();
            dragonfly.SetPath (path);
        });

    }

    #region Crop

        public void Crop () {
        
            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info);
            // Debug.Log("DragonFly crop energy: " + cropEnergy);
            //EnergyManager.Instance.fishEnergyCrop[4] = true;
            EnergyManager.Instance.fishEnergyIncreased[4] = cropEnergy;
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (DragonFly _dragonfly in _dragonflies) {
				_dragonfly.GetComponent<DragonFly> ().playDragonFlyEnergyParticle ();
			}
        }

        public void Crop (int times) {

            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info) * times;

            // Debug.Log("DragonFly crop energy(Offline): " + cropEnergy);
            EnergyManager.Instance.FishEnergy += cropEnergy;
			
			foreach (DragonFly _dragonfly in _dragonflies) {
				_dragonfly.GetComponent<DragonFly> ().playDragonFlyEnergyParticle ();
			}
        }

    #endregion

    #region Common

        private int CalcCropEnergy (PondCropInfo info) {
            int energy = info.CropEnergys [Level - 1];

            return energy;
        }

    #endregion

}
