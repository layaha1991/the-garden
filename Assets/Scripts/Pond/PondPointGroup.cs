﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PondPointGroup : MonoBehaviour {

    [SerializeField] private PondPoint[] _points;

    private int _index = 0;

    private void Awake () {
        // random index
        _index = Random.Range (0, _points.Length);
    }

    public Vector3 GetNextPoint () {
        _index++;
        if (_index >= _points.Length) {
            _index = 0;
        }

        return _points [_index].Position;
    }

    #region Debug

        [Header ("Debug")]
        [SerializeField] private float _debug_radius = 5f;

        public void OnDrawGizmosSelected () {
            Gizmos.color = Color.blue;

            if (_points == null)
                return;

            int length = _points.Length;
            for (int i = 0; i < length; i++) {
                PondPoint point = _points [i];
                Vector3 pos = point.Position;
                Gizmos.DrawSphere (pos, _debug_radius);

                if (point.NextPoint != null) {
                    Gizmos.DrawLine (pos, point.NextPointPosition);
                }

                if (point.PrePoint != null) {
                    Gizmos.DrawLine (pos, point.PrePointPosition);
                }

            }

        }

    #endregion

}
