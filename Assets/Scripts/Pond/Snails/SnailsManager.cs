﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailsManager : IPondManager {

    #region Snails list

        private List<Snails> _snails = new List<Snails> ();

    #endregion

    public EnumPondType GetPondType () {
        return EnumPondType.Snails;
    }

    public int Level {
        get;
        private set;
    }

    public void Init () {
        Level = DataManager.SnailsLevel;

        CreateSnails ();
    }
    
    #region Create

        private void CreateSnails () {

            for (int i = 0; i < Level; i++) {
                CreateSnail ();
            }

        }

        private Snails CreateSnail () {
            PondSpawn spawn = Pond.Instance.GetRandomSpawn ();
            GameObject snail = spawn.Create (GetPondType ());
            _snails.Add (snail.GetComponent<Snails> ());
            return snail.GetComponent<Snails> ();
        }

    #endregion

    public void Unlock () {
        Level++;
        DataManager.SnailsLevel = Level;

        Snails snail = CreateSnail ();

        Vector3 pos = snail.transform.position;
        pos.y += 400f;
        CameraManager.Instance.SetWatch (pos);
        GameManager.Instance.State = EnumGameState.Watch;

        snail.Grow (()=>{
            GameManager.Instance.State = EnumGameState.Pond;
        });

    }

    #region Crop

        public void Crop () {

            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info);
            // Debug.Log("Snails crop energy: " + cropEnergy);
            //EnergyManager.Instance.fishEnergyCrop[2] = true;
            EnergyManager.Instance.fishEnergyIncreased[2] = cropEnergy;
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (Snails _snail in _snails) {
				_snail.GetComponent<Snails> ().playSnailsEnergyParticle();
			}
			
        }

        public void Crop (int times) {

            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info) * times;

            // Debug.Log("Snails crop energy(Offline): " + cropEnergy);
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (Snails _snail in _snails) {
				_snail.GetComponent<Snails> ().playSnailsEnergyParticle();
			}
        }

    #endregion

    #region Common

        private int CalcCropEnergy (PondCropInfo info) {
            int defaultEnergy = info.CropDefaultEnergy;
            float upEnergy = info.CropUpEnergyPerStage;
            int totalEnergy = (int)(defaultEnergy * Mathf.Pow(upEnergy, Level - 1));

            return totalEnergy;
        }

    #endregion

}
