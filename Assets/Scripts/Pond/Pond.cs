﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

// 水塘
public class Pond : Singleton<Pond> {

    #region Spawn points

        [Header ("生成點")]
        [SerializeField] private PondSpawn[] _spawns;

        public PondSpawn GetRandomSpawn () {
            return _spawns [UnityEngine.Random.Range (0, _spawns.Length)];
        }

    #endregion

    #region Point

        [Header ("蜻蜓的路徑")]
        [SerializeField] private Transform _pondPointParent;

        private List<PondPointGroup> _pondPoints;

        private int _index = 0;
        public PondPointGroup GetPondPoints () {

            if (_index >= (_pondPoints.Count - 1))
                _index = 0;

            return _pondPoints [_index++];
        }

    #endregion

    #region Propertyies

        [Header ("收成屬性")]
        [Tooltip ("收成屬性")]
        [SerializeField]
        private float _cropSeconds;
        [SerializeField]
        private PondCropInfo[] _cropInfos;

        public PondCropInfo GetCropInfo (EnumPondType type) {
            int length = _cropInfos.Length;
            for (int i = 0; i < length; i++) {
                PondCropInfo info = _cropInfos [i];

                if (info.Type == type) {
                    return info;
                }

            }

            return null;
        }

    #endregion

    #region Manager

        private List<IPondManager> _managers = new List<IPondManager> ();

        public T GetPondManager<T> (EnumPondType type) where T : IPondManager {

            int count = _managers.Count;
            for (int i = 0; i < count; i++) {
                IPondManager manager = _managers [i];
                if (manager.GetPondType () == type) {
                    return (T)manager;
                }

            }

            return default (T);
        }

        public IPondManager GetPondManager (EnumPondType type) {

            int count = _managers.Count;
            for (int i = 0; i < count; i++) {
                IPondManager manager = _managers [i];
                if (manager.GetPondType () == type) {
                    return manager;
                }

            }

            return null;
        }

        public int GetPondLevel (EnumPondType type) {
            IPondManager pondManager = GetPondManager (type);

            return pondManager.Level;
        }

    #endregion

    #region Initialization

        protected override void OnInit () {
            // Debug.Log("Pond init.");

            OfflineSystem.TimeSpanChanged += OnOfflineSystemTimeSpanChanged;

            SetPoints ();

            // koi
            KoiManager koiManager = new KoiManager ();
            koiManager.Init ();

            // turtle
            TurtleManager turtleManager = new TurtleManager ();
            turtleManager.Init ();

            // snails
            SnailsManager snailsManager = new SnailsManager ();
            snailsManager.Init ();

            // frog
            FrogManager frogManager = new FrogManager ();
            frogManager.Init ();            

            // dragonfly
            DragonFlyManager dragonflyManager = new DragonFlyManager ();
            dragonflyManager.Init ();

            _managers.Add (koiManager);
            _managers.Add (turtleManager);
            _managers.Add (snailsManager);
            _managers.Add (frogManager);
            _managers.Add (dragonflyManager);
        }

        private void SetPoints () {
            _pondPoints = new List<PondPointGroup> ();

            int chidlCount = _pondPointParent.childCount;
            for (int i = 0; i < chidlCount; i++) {
                Transform parent = _pondPointParent.GetChild (i);
                PondPointGroup group = parent.GetComponent<PondPointGroup> ();

                if (group != null) {
                    _pondPoints.Add (group);
                }

            }
            
        }

    #endregion

    #region Update
        float _countdown = 0f;

        private void Update () {
            CountDown (Time.deltaTime);
        }

        private void CountDown (float deltaTime) {
            bool b = true;
            int count = _managers.Count;
            for (int i = 0; i < count; i++) {

                if (_managers [i].Level > 0) {
                    b = false;
                    break;
                }

            }

            if (b) return;

            _countdown += deltaTime;

            if (_countdown >= _cropSeconds) {
                Crop ();
                _countdown = 0f;
            }

        }



    #endregion

    #region Crop

        private void Crop () {
            int count = _managers.Count;

            for (int i = 0; i < count; i++) {
                _managers [i].Crop ();
            }

        }

    #endregion

    #region Unlock

        public void UnlockKoi () {
            GetPondManager<KoiManager> (EnumPondType.Koi).Unlock ();
        }

        public void UnlockTurtle () {
            GetPondManager<TurtleManager> (EnumPondType.Turtle).Unlock ();
        }

        public void UnlockSnails () {
            GetPondManager<SnailsManager> (EnumPondType.Snails).Unlock ();
        }

        public void UnlockFrog () {
            GetPondManager<FrogManager> (EnumPondType.Frog).Unlock ();
        }

        public void UnlockDragonFly () {
            GetPondManager<DragonFlyManager> (EnumPondType.DragonFly).Unlock ();
        }

    #endregion

    #region Input

       private void OnMouseDown() {

            if (EventSystem.current.IsPointerOverGameObject())  {
                return;
            }

            if (GameManager.Instance.State == EnumGameState.Pond) {
                Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast (ray, out hit, Mathf.Infinity, LayerMask.GetMask ("Interaction"))) {
                    GetPondManager<KoiManager> (EnumPondType.Koi).FeedTheFish (hit.point);
                }
                
                return;
            }

        }

    #endregion

    #region offline

        private void OnOfflineSystemTimeSpanChanged (TimeSpan timeSpan) {
            // Debug.Log("Bamboo-TimeSpan: " + timeSpan);

            // 不能超过240分鐘
            float seconds = Mathf.Min((float)timeSpan.TotalSeconds, OfflineSystem.Instance.PondOfflineSecondsLimit);
            // Debug.Log("Bamboo-TimeSpan-Seconds: " + seconds);
            int times = (int)(seconds / _cropSeconds);
            // Debug.Log("Bamboo-Times: " + times);
            
            int count = _managers.Count;
            for (int i = 0; i < count; i++) {
                _managers [i].Crop (times);
            }
            
        } 

    #endregion

}
