﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogManager : IPondManager {

    #region Frog list

        private List<Frog> _frogs = new List<Frog> ();

    #endregion

    public EnumPondType GetPondType () {
        return EnumPondType.Frog;
    }

    public int Level {
        get;
        private set;
    }

    public void Init () {
        Level = DataManager.FrogLevel;

        CreateFrogs ();
    }

    #region Create

        private void CreateFrogs () {

            for (int i = 0; i < Level; i++) {
                CreateFrog ();
            }

        }

        private Frog CreateFrog () {
            PondSpawn spawn = Pond.Instance.GetRandomSpawn ();
            GameObject frog = spawn.Create (GetPondType ());
            _frogs.Add (frog.GetComponent<Frog> ());
            return frog.GetComponent<Frog> ();
        }

    #endregion

    public void Unlock () {
        Level++;
        DataManager.FrogLevel = Level;

        Frog frog = CreateFrog ();

        Vector3 pos = frog.transform.position;
        pos.y += 400f;
        CameraManager.Instance.SetWatch (pos);
        GameManager.Instance.State = EnumGameState.Watch;

        frog.Grow (()=>{
            GameManager.Instance.State = EnumGameState.Pond;
        });

    }

    #region Crop

        public void Crop () {

            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info);
            // Debug.Log("Frog crop energy: " + cropEnergy);
            //EnergyManager.Instance.fishEnergyCrop[3] = true;
            EnergyManager.Instance.fishEnergyIncreased[3] = cropEnergy;
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (Frog _frog in _frogs) {
				_frog.GetComponent<Frog> ().playFrogEnergyParticle();
			}
				
        }

        public void Crop (int times) {

            if (Level <= 0)
                return;
                
            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info) * times;

            // Debug.Log("Frog crop energy(Offline): " + cropEnergy);
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (Frog _frog in _frogs) {
				_frog.GetComponent<Frog> ().playFrogEnergyParticle();
			}

        }

    #endregion

    #region Common

        private int CalcCropEnergy (PondCropInfo info) {
            int defaultEnergy = info.CropDefaultEnergy;
            float upEnergy = info.CropUpEnergyPerStage;
            int totalEnergy = (int)(defaultEnergy * Mathf.Pow(upEnergy, Level - 1));

            return totalEnergy;
        }

    #endregion

}
