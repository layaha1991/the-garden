﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Frog : MonoBehaviour {

    [SerializeField] private float _moveSpeed = 1f;
    [SerializeField] private float _rotateAngle = 5f;
    [SerializeField] private SpriteRenderer _body;
	[SerializeField] private GameObject _FrogEnergyParticle;

    private void Update () {
        Move (Time.deltaTime);
    }

    private void Move (float deltaTime) {
        transform.Translate (transform.forward * deltaTime * _moveSpeed, Space.Self);
    }

    private void Rotate () {
        transform.Rotate (0f, 0f, _rotateAngle * Time.deltaTime);
    }

    private void OnCollisionStay (Collision other) {
        Rotate ();
    }

    public void Grow (Action callback = null) {
        this.transform.localScale = Vector3.zero;

        SimpleAnimtor.Instance.PlayPondGrowAnim (this.transform, _body, 0f, ()=>{

            if (callback != null) {
                callback ();
            }

        });

    }

	public void playFrogEnergyParticle(){
		StartCoroutine (_playFrogEnergyParticle ());
	}

	private IEnumerator _playFrogEnergyParticle(){
		_FrogEnergyParticle.gameObject.SetActive (true);
		yield return new WaitForSeconds (8f);
		_FrogEnergyParticle.gameObject.SetActive (false);
	}
    
    private void OnMouseDown () {

        if (GameManager.Instance.State != EnumGameState.Pond) {
            GameManager.Instance.State = EnumGameState.Pond;
            return;
        }

    }

}
