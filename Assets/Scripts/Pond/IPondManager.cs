﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPondManager {

    int Level {
        get;
    }

    EnumPondType GetPondType ();

    void Init ();

    void Crop ();

    void Crop (int times);

    void Unlock ();

}
