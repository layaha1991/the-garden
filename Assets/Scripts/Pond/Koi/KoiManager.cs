﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KoiManager : IPondManager {

    #region Koi list

        private List<Koi> _kois = new List<Koi> ();

    #endregion

    public int Level {
        get;
        private set;
    }

    public EnumPondType GetPondType () {
        return EnumPondType.Koi;
    }

    public void Init () {
        Level = DataManager.KoiLevel;
       // Level = 12 ;
        CreateKois ();
    }

    public void FeedTheFish (Vector3 pos) {
        int count = _kois.Count;
        for (int i = 0; i < count; i++) {
            Koi koi = _kois [i];
            Vector3 selfPos = koi.transform.position;
            Vector3 dir = pos;
            dir.x -= selfPos.x;
            dir.z = selfPos.z - pos.z;
            float angle = Mathf.Atan2 (dir.z, dir.x) * Mathf.Rad2Deg - 90f;
            
            koi.StartRotateToFeedPoint (angle);
        }
        
    }

    #region Create

        private void CreateKois () {

            for (int i = 0; i < Level; i++) {
                CreateKoi ();
            }

        }

        private Koi CreateKoi () {
            PondSpawn spawn = Pond.Instance.GetRandomSpawn ();
            GameObject koi = spawn.Create (GetPondType ());
            _kois.Add (koi.GetComponent<Koi> ());
            return koi.GetComponent<Koi> ();
        }

    #endregion

    public void Unlock () {
        Level++;
        DataManager.KoiLevel = Level;

        // 生成魚
        Koi koi = CreateKoi ();

        Vector3 pos = koi.transform.position;
        pos.y += 400f;
        CameraManager.Instance.SetWatch (pos);
        GameManager.Instance.State = EnumGameState.Watch;

        koi.Grow (()=>{
            GameManager.Instance.State = EnumGameState.Pond;
        });

    }

    #region Crop

        public void Crop () {

            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info);
        // Debug.Log("Koi crop energy: " + cropEnergy);
            EnergyManager.Instance.fishEnergyCrop[0] = true;
            EnergyManager.Instance.fishEnergyIncreased[0] = cropEnergy;
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (Koi _koi in _kois) {
				_koi.GetComponent<Koi> ().playKoiEnergyParticle ();
			}

        }

        public void Crop (int times) {

            if (Level <= 0)
                return;
                
            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info) * times;
        //		Debug.Log("Koi crop energy(Offline): " + cropEnergy);

            EnergyManager.Instance.FishEnergy += cropEnergy;


		foreach (Koi _koi in _kois) {
			_koi.GetComponent<Koi> ().playKoiEnergyParticle ();
		}


        }

    #endregion

    #region Common

        private int CalcCropEnergy (PondCropInfo info) {
            int defaultEnergy = info.CropDefaultEnergy;
            float upEnergy = info.CropUpEnergyPerStage;
            int totalEnergy = (int)(defaultEnergy * Mathf.Pow(upEnergy, Level - 1));

            return totalEnergy;
        }

    #endregion

}
