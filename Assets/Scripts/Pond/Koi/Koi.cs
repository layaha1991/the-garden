﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Koi : MonoBehaviour {

    [SerializeField] private float _moveSpeed = 20f;
    [SerializeField] private float _rotateAngle = 5f;
    [SerializeField] private SpriteRenderer _body;
	[SerializeField] private GameObject _KoiEnergyParticle;
	[SerializeField] private GameObject _interactiveMusic;
	[SerializeField] private GameObject _RipplingEnergyParticle;

	void Start(){
		_interactiveMusic = GameObject.FindGameObjectWithTag ("zenSound");
	}

    private void Update () {
        Move (Time.deltaTime);
    }

    private void Move (float deltaTime) {
        transform.Translate (transform.forward * deltaTime * _moveSpeed, Space.Self);
    }

    private void Rotate () {
        transform.Rotate (0f, 0f, _rotateAngle * Time.deltaTime);
    }

    private void OnCollisionStay (Collision other) {
        Rotate ();
    }

    public void Grow (Action callback = null) {
        this.transform.localScale = Vector3.zero;

        SimpleAnimtor.Instance.PlayPondGrowAnim (this.transform, _body, 0f, ()=>{

            if (callback != null) {
                callback ();
            }

        });

    }

    public void StartRotateToFeedPoint (float angle) {
        StartCoroutine (RotateToFeedPoint (angle));
    }

    private IEnumerator RotateToFeedPoint (float angle) {
        float t = 0f;
        float duration = 0.25f;
        Quaternion startRot = transform.localRotation;
        Quaternion endRot = Quaternion.Euler (0f, 0f, angle);

        while (t < duration) {
            t += Time.deltaTime;

            transform.localRotation = Quaternion.Slerp (startRot, endRot, t / duration);

            yield return null;
        }
        transform.localRotation = endRot;
    }

	public void playKoiEnergyParticle(){
		StartCoroutine (_playKoiEnergyParticle ());
	}

	private IEnumerator _playKoiEnergyParticle(){
		_KoiEnergyParticle.gameObject.SetActive (true);
		yield return new WaitForSeconds (10f);
		_KoiEnergyParticle.gameObject.SetActive (false);
	}

    private void OnMouseDown () {

//        if (GameManager.Instance.State != EnumGameState.Pond) {
//            GameManager.Instance.State = EnumGameState.Pond;
//            return;
//        }
		StartCoroutine(Speedup());
		StartCoroutine (Rippling ());
        AudioManager.Instance.RandomPlay();

    }

	IEnumerator Speedup(){
		_moveSpeed = 60f;
		yield return new WaitForSeconds (5f);
		_moveSpeed = 20f;
	}

	IEnumerator Rippling(){
		_RipplingEnergyParticle.gameObject.SetActive (true);
		yield return new WaitForSeconds (5f);
		_RipplingEnergyParticle.gameObject.SetActive (false);
	}
}
