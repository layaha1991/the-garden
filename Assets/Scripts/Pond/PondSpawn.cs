﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PondSpawn : MonoBehaviour {

    [Header ("基础屬性")]
    [Tooltip ("生成範圍")]
    [SerializeField] private float _radius = 5f;

    public GameObject Create (EnumPondType type) {
        // Debug.Log("Type: " + type);

        Vector3 selfPos = transform.position;
        selfPos.y = selfPos.z;
        Vector3 pos = (Vector2)selfPos + Random.insideUnitCircle * _radius;
        float temp = pos.z;
        pos.z = pos.y;
        pos.y = temp;
        Quaternion rotation = Quaternion.Euler (-90f, 0f, Random.Range (0f, 360f));

        GameObject prefab = Resources.Load<GameObject> (PathDefine.GetPondPath (type));
        GameObject pondLiving = GameObject.Instantiate (prefab, pos, rotation, Pond.Instance.transform);

        // pondLiving.transform.position = pos;
        // pondLiving.transform.SetParent (Pond.Instance.transform);
        return pondLiving;
    }

    private void OnDrawGizmosSelected () {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere (transform.position, _radius);
    }

}
