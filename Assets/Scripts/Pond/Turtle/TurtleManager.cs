﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurtleManager : IPondManager {

    #region Turtle list

        private List<Turtle> _turtles = new List<Turtle> ();

    #endregion

    public EnumPondType GetPondType () {
        return EnumPondType.Turtle;
    }

    public int Level {
        get;
        private set;
    }

    public void Init () {
        Level = DataManager.TurtleLevel;
        // Level = 5;
        CreateTurtles ();
    }

    #region Create

        private void CreateTurtles () {

            for (int i = 0; i < Level; i++) {
                CreateTurtle ();
            }

        }

        private Turtle CreateTurtle () {
            PondSpawn spawn = Pond.Instance.GetRandomSpawn ();
            GameObject turtle = spawn.Create (GetPondType ());
            _turtles.Add (turtle.GetComponent<Turtle> ());
            return turtle.GetComponent<Turtle> ();
        }

    #endregion

    public void Unlock () {
        Level++;
        DataManager.TurtleLevel = Level;

        Turtle turtle = CreateTurtle ();

        Vector3 pos = turtle.transform.position;
        pos.y += 400f;
        CameraManager.Instance.SetWatch (pos);
        GameManager.Instance.State = EnumGameState.Watch;

        turtle.Grow (()=>{
            GameManager.Instance.State = EnumGameState.Pond;
        });

    }

    #region Crop

        public void Crop () {

            if (Level <= 0)
                return;

            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info);
            // Debug.Log("Turtle crop energy: " + cropEnergy);
            //EnergyManager.Instance.fishEnergyCrop[1] = true;
            EnergyManager.Instance.fishEnergyIncreased[1] = cropEnergy;
            EnergyManager.Instance.FishEnergy += cropEnergy;

			foreach (Turtle _turtle in _turtles) {
				_turtle.GetComponent<Turtle> ().playTurtleEnergyParticle ();
			}
        }

        public void Crop (int times) {

            if (Level <= 0)
                return;
                
            PondCropInfo info = Pond.Instance.GetCropInfo (GetPondType ());
            int cropEnergy = CalcCropEnergy (info) * times;

            // Debug.Log("Turtle crop energy(Offline): " + cropEnergy);
            EnergyManager.Instance.FishEnergy += cropEnergy;
			
			foreach (Turtle _turtle in _turtles) {
				_turtle.GetComponent<Turtle> ().playTurtleEnergyParticle ();
			}
        }

    #endregion

    #region Common

        private int CalcCropEnergy (PondCropInfo info) {
            int defaultEnergy = info.CropDefaultEnergy;
            float upEnergy = info.CropUpEnergyPerStage;
            int totalEnergy = (int)(defaultEnergy * Mathf.Pow(upEnergy, Level - 1));

            return totalEnergy;
        }

    #endregion

}
