﻿using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class energySoundEffect: MonoBehaviour {


    [SerializeField] private AudioClip[] _energySoundEffect;

	private AudioSource audioSource;
	private AudioClip shootClip;

    public void randomPlay(){
		audioSource = gameObject.GetComponent<AudioSource> ();
		int index = Random.Range (0, _energySoundEffect.Length);
		shootClip = _energySoundEffect [index];
		audioSource.clip = shootClip;
		audioSource.Play ();
	}
}
