﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacilityInfo {

    public readonly int Level;
    public readonly string CurFunc;
    public readonly string NextFunc;
    public readonly string NextCost;

    public FacilityInfo () {}

    public FacilityInfo (int level, string curFunc, string nextFunc, string nextCost) {
        Level = level;
        CurFunc = curFunc;
        NextFunc = nextFunc;
        NextCost = nextCost;
    }

}
