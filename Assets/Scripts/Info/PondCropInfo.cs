﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PondCropInfo {

    [SerializeField] private EnumPondType _type;
    [SerializeField] private int _cropDefaultEnergy;
    [SerializeField] private float _cropUpEnergyPerStage;
    [SerializeField] private int[] _cropEnergys;

    public int CropDefaultEnergy { get { return _cropDefaultEnergy; } }
    public float CropUpEnergyPerStage { get { return _cropUpEnergyPerStage; } }
    public EnumPondType Type { get { return _type; } }
    public int[] CropEnergys { get { return _cropEnergys; } }

}
