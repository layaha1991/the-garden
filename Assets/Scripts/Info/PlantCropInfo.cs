﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlantCropInfo {
    [SerializeField] private float _cropSeconds;
    [SerializeField] private int _cropDefaultEnergy;
    [SerializeField] private float _cropUpEnergyPerStage;

    public float CropSeconds { get { return _cropSeconds; } }
    public int CropDefaultEnergy { get { return _cropDefaultEnergy; } }
    public float CropUpEnergyPerStage { get { return _cropUpEnergyPerStage; } }

}
