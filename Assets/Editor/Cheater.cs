﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Cheater {

    [MenuItem("Cheater/AddWaterEnergy")]
    private static void AddWaterEnergy () {
        float waterEnergy = PlayerPrefs.GetFloat (DataManager.KEY_WATER_ENERGY);
        waterEnergy += 1000000f;
        PlayerPrefs.SetFloat (DataManager.KEY_WATER_ENERGY, waterEnergy);

        Debug.Log("當前水能: " + waterEnergy);
    }

    [MenuItem("Cheater/AddPlantEnergy")]
    private static void AddPlantEnergy () {
        int plantEnergy = PlayerPrefs.GetInt (DataManager.KEY_PLANT_ENERGY);
        plantEnergy += 10000;
        PlayerPrefs.SetInt (DataManager.KEY_PLANT_ENERGY, plantEnergy);

        Debug.Log("當前植能: " + plantEnergy);
    }

    [MenuItem("Cheater/AddFishEnergy")]
    private static void AddFishEnergy () {
        int fishEnergy = PlayerPrefs.GetInt (DataManager.KEY_FISH_ENERGY);
        fishEnergy += 10000;
        PlayerPrefs.SetInt (DataManager.KEY_FISH_ENERGY, fishEnergy);

        Debug.Log("當前魚能: " + fishEnergy);
    }

}
