﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameEditor {

    [MenuItem("Game/Tools/Camera/ToMain")]
    private static void ToMain () {
        Camera.main.transform.position = CameraDefine.POS_MAIN;
        Camera.main.transform.rotation = CameraDefine.ROT_MAIN;
    }

    [MenuItem("Game/Tools/Camera/ToWaterWheel")]
    private static void ToWaterWheel () {
        Camera.main.transform.position = CameraDefine.POS_WATERWHEEL;
        Camera.main.transform.rotation = CameraDefine.ROT_WATERWHEEL;
    }

    [MenuItem("Game/Tools/Camera/ToPond")]
    private static void ToPond () {
        Camera.main.transform.position = CameraDefine.POS_POND;
        Camera.main.transform.rotation = CameraDefine.ROT_POND;
    }

    [MenuItem ("Game/Tools/Camera/ToBamboo")]
    private static void ToBamboo () {
        Camera.main.transform.position = CameraDefine.POS_BAMBOO;
        Camera.main.transform.rotation = CameraDefine.ROT_BAMBOO;
    }

    [MenuItem ("Game/Tools/Camera/ToFlower_1")]
    private static void ToFlower_1 () {
        Camera.main.transform.position = CameraDefine.POS_FLOWER [0];
        Camera.main.transform.rotation = CameraDefine.ROT_FLOWER [0];
    }

    [MenuItem ("Game/Tools/Camera/ToFlower_2")]
    private static void ToFlower_2 () {
        Camera.main.transform.position = CameraDefine.POS_FLOWER [1];
        Camera.main.transform.rotation = CameraDefine.ROT_FLOWER [1];
    }

    [MenuItem ("Game/Tools/Camera/ToFlower_3")]
    private static void ToFlower_3 () {
        Camera.main.transform.position = CameraDefine.POS_FLOWER [2];
        Camera.main.transform.rotation = CameraDefine.ROT_FLOWER [2];
    }

    [MenuItem ("Game/Tools/Camera/ToReed_1")]
    private static void ToReed_1 () {
        Camera.main.transform.position = CameraDefine.POS_REED [0];
        Camera.main.transform.rotation = CameraDefine.ROT_REED [0];
    }

    [MenuItem ("Game/Tools/Camera/ToReed_2")]
    private static void ToReed_2 () {
        Camera.main.transform.position = CameraDefine.POS_REED [1];
        Camera.main.transform.rotation = CameraDefine.ROT_REED [1];
    }

    [MenuItem ("Game/Tools/Camera/ToReed_3")]
    private static void ToReed_3 () {
        Camera.main.transform.position = CameraDefine.POS_REED [2];
        Camera.main.transform.rotation = CameraDefine.ROT_REED [2];
    }

    [MenuItem ("Game/Tools/Camera/ToMushroom_1")]
    private static void ToMushroom_1 () {
        Camera.main.transform.position = CameraDefine.POS_MUSHROOM [0];
        Camera.main.transform.rotation = CameraDefine.ROT_MUSHROOM [0];
    }

    [MenuItem ("Game/Tools/Camera/ToMushroom_2")]
    private static void ToMushroom_2 () {
        Camera.main.transform.position = CameraDefine.POS_MUSHROOM [1];
        Camera.main.transform.rotation = CameraDefine.ROT_MUSHROOM [1];
    }

    [MenuItem ("Game/Tools/Camera/ToMushroom_3")]
    private static void ToMushroom_3 () {
        Camera.main.transform.position = CameraDefine.POS_MUSHROOM [2];
        Camera.main.transform.rotation = CameraDefine.ROT_MUSHROOM [2];
    }

    [MenuItem ("Game/Tools/Camera/ToTree_1")]
    private static void ToTree_1 () {
        Camera.main.transform.position = CameraDefine.POS_TREE [0];
        Camera.main.transform.rotation = CameraDefine.ROT_TREE [0];
    }

    [MenuItem ("Game/Tools/Camera/ToTree_2")]
    private static void ToTree_2 () {
        Camera.main.transform.position = CameraDefine.POS_TREE [1];
        Camera.main.transform.rotation = CameraDefine.ROT_TREE [1];
    }

    [MenuItem ("Game/Tools/Camera/ToTree_3")]
    private static void ToTree_3 () {
        Camera.main.transform.position = CameraDefine.POS_TREE [2];
        Camera.main.transform.rotation = CameraDefine.ROT_TREE [2];
    }

    [MenuItem ("Game/Tools/Camera/ToSakura_1")]
    private static void ToSakura_1 () {
        Camera.main.transform.position = CameraDefine.POS_SAKURA [0];
        Camera.main.transform.rotation = CameraDefine.ROT_SAKURA [0];
    }

    [MenuItem ("Game/Tools/Camera/ToSakura_2")]
    private static void ToSakura_2 () {
        Camera.main.transform.position = CameraDefine.POS_SAKURA [1];
        Camera.main.transform.rotation = CameraDefine.ROT_SAKURA [1];
    }

    [MenuItem ("Game/Tools/Camera/ToSakura_3")]
    private static void ToSakura_3 () {
        Camera.main.transform.position = CameraDefine.POS_SAKURA [2];
        Camera.main.transform.rotation = CameraDefine.ROT_SAKURA [2];
    }

    [MenuItem ("Game/Tools/ClearPlayerPrefs %&Q")]
    private static void ClearPlayerPrefs () {
        Debug.Log("Clear PlayerPrefs Succeed.");

        PlayerPrefs.DeleteAll ();
    }



}
